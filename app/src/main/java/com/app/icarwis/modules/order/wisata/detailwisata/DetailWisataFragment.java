package com.app.icarwis.modules.order.wisata.detailwisata;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.icarwis.R;
import com.app.icarwis.base.BaseMvpFragment;
import com.app.icarwis.models.wisata.Wisata;
import com.app.icarwis.modules.common.FragmentPhotoViewSingle;
import com.app.icarwis.modules.common.FragmentPhotoViewSingleBuilder;
import com.app.icarwis.utility.CommonUtilities;
import com.app.icarwis.utility.Str;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.OnClick;

@FragmentWithArgs
public class DetailWisataFragment extends BaseMvpFragment<XDetailWisataView, DetailWisataPresenter>
        implements XDetailWisataView {

    @Arg Wisata wisata;
    @BindView(R.id.tvTitle) TextView tvTitle;
    @BindView(R.id.tvAddress) TextView tvAddress;
    @BindView(R.id.layMap) LinearLayout layMap;
    @BindView(R.id.wvDesc) WebView wvDesc;
    @BindView(R.id.ivImage) ImageView ivImage;
    @BindView(R.id.tvPrice) TextView tvPrice;

    public DetailWisataFragment() {

    }

    @Override public DetailWisataPresenter createPresenter() {
        return new DetailWisataPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_detail_wisata;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        WebSettings ws = wvDesc.getSettings();
        ws.setDefaultFontSize(16);
        ws.setBuiltInZoomControls(false);
        ws.setDisplayZoomControls(false);
        ws.setJavaScriptEnabled(false);

        if (TextUtils.isEmpty(wisata.getMediaValue())){
            ivImage.setImageResource(R.color.md_grey_200);
        }else{
            Picasso.with(getContext()).load(wisata.getMediaValue()).resize(400, 0).into(ivImage);
        }
        tvTitle.setText(Str.getText(wisata.getCwName(), ""));
        tvAddress.setText(Str.getText(wisata.getCwAddress(), ""));
        tvPrice.setText(CommonUtilities.toRupiahNumberFormat(wisata.getAmount() + wisata.getCwService()));
        wvDesc.loadData(Str.getText(wisata.getCwDesc(), ""), "text/html", "UTF-8");
    }

    @OnClick({R.id.ivImage, R.id.layMap}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivImage:
                if (!TextUtils.isEmpty(wisata.getMediaValue())){
                    FragmentPhotoViewSingle fragmentPhotoView = new FragmentPhotoViewSingleBuilder(wisata.getMediaValue()).build();
                    fragmentPhotoView.setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogFragmentTheme);
                    fragmentPhotoView.show(getFragmentManager(), "photo");
                }
                break;
            case R.id.layMap:
                String url = "https://www.google.com/maps/search/?api=1&query=" + wisata.getCwLat() + "," + wisata.getCwLong();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                break;
        }
    }
}