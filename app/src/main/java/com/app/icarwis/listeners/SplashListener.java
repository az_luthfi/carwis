package com.app.icarwis.listeners;


import com.app.icarwis.models.customer.Customer;

/**
 * Created by Luthfi on 02/08/2017.
 */

public interface SplashListener {
    void OnSuccessAuth(Customer customer);
}
