package com.app.icarwis.adapter.wisata;

import com.app.icarwis.base.BaseRecyclerViewAdapter;
import com.app.icarwis.holder.wisata.ItemWisataCartHolder;
import com.app.icarwis.models.wisata.Wisata;

/**
 * Created by luthfiaziz on 01/03/18.
 */
public class WisataCartAdapter extends BaseRecyclerViewAdapter<Wisata, ItemWisataCartHolder> {

    private final onActionListener listener;

    public WisataCartAdapter(onActionListener listener) {
        super(ItemWisataCartHolder.class);
        this.listener = listener;
    }

    @Override protected void populateViewHolder(ItemWisataCartHolder viewHolder, Wisata item, int position) {
        viewHolder.bindView(item, listener);
    }

    public interface onActionListener {
        void onClickUp(int position);

        void onClickDown(int position);

        void onClickDelete(int position);

        void onClickDetail(int position);

        int getItemsCount();
    }
}