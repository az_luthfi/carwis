package com.app.icarwis.apis;

import com.app.icarwis.models.berita.RequestContent;
import com.app.icarwis.models.car.RequestCar;
import com.app.icarwis.models.customer.RequestCustomer;
import com.app.icarwis.models.deposit.RequestDeposit;
import com.app.icarwis.models.pesan.RequestMessage;
import com.app.icarwis.models.transaction.RequestTransaction;
import com.app.icarwis.models.wisata.RequestWisata;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

/**
 * Created by j3p0n on 3/21/2017.
 */

public interface ApiService {

    @FormUrlEncoded
    @POST("APIpesan.php")
    Observable<RequestMessage> apiMessageRequest(@FieldMap Map<String, String> params);

    @Multipart
    @POST("APIpesan.php")
    Observable<RequestMessage> sendMessage(
            @PartMap Map<String, RequestBody> params,
            @Part MultipartBody.Part image
    );

    @FormUrlEncoded
    @POST("APIuser.php")
    Observable<RequestCustomer> apiCustomer(@FieldMap HashMap<String, String> params);

    @Multipart
    @POST("APIuser.php")
    Observable<RequestCustomer> apiRegisterMember(
            @PartMap Map<String, RequestBody> params,
            @Part MultipartBody.Part image
    );

    @FormUrlEncoded
    @POST("APIcontent.php")
    Observable<RequestContent> apiContentRequest(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST("APIuser.php")
    Observable<RequestDeposit> apiDepositRequest(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST("APIwisata.php")
    Observable<RequestWisata> apiWisata(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST("APIcar.php")
    Observable<RequestCar> apiCar(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST("APItransaction.php")
    Observable<RequestTransaction> apiTransaction(@FieldMap HashMap<String, String> params);


}
