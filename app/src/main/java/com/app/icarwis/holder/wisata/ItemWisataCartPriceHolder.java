package com.app.icarwis.holder.wisata;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.icarwis.R;
import com.app.icarwis.models.wisata.Wisata;
import com.app.icarwis.utility.CommonUtilities;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemWisataCartPriceHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvText) TextView tvText;
    @BindView(R.id.tvAmount) TextView tvAmount;

    public ItemWisataCartPriceHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_wisata_cart_price, parent, false));
    }

    public ItemWisataCartPriceHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void bindView(Wisata model) {
        tvText.setText(new StringBuilder("- ").append(model.getCwName()));
        tvAmount.setText(CommonUtilities.toRupiahNumberFormat(model.getAmount() + model.getCwService()));
    }
}
