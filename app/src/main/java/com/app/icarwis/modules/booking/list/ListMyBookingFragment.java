package com.app.icarwis.modules.booking.list;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.icarwis.R;
import com.app.icarwis.adapter.transaction.BookingAdapter;
import com.app.icarwis.base.BaseLceRefreshFragment;
import com.app.icarwis.holder.ItemLoadingHolder;
import com.app.icarwis.models.DataNotification;
import com.app.icarwis.models.eventbus.EventRefreshBookingList;
import com.app.icarwis.models.transaction.RequestTransaction;
import com.app.icarwis.modules.booking.detail.DetailMyBookingFragment;
import com.app.icarwis.modules.booking.detail.DetailMyBookingFragmentBuilder;
import com.app.icarwis.utility.listview.EndlessRecyclerOnScrollListener;
import com.app.icarwis.utility.listview.ItemClickSupport;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;

import static com.app.icarwis.utility.Config.PAGINATION_LIMIT;

@FragmentWithArgs
public class ListMyBookingFragment extends BaseLceRefreshFragment<SwipeRefreshLayout, RequestTransaction, XListMyBookingView, ListMyBookingPresenter>
        implements XListMyBookingView, ItemLoadingHolder.ItemViewLoadingListener {

    @Arg String status;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.emptyView) TextView emptyView;
    @BindView(R.id.contentView) SwipeRefreshLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.container) FrameLayout container;

    private BookingAdapter adapter;
    private EndlessRecyclerOnScrollListener scrollListener;

    public ListMyBookingFragment() {

    }

    @Override public ListMyBookingPresenter createPresenter() {
        return new ListMyBookingPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_with_recyclerview_refresh;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new BookingAdapter(this);
        container.setBackgroundColor(getResources().getColor(R.color.md_grey_200));
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                DetailMyBookingFragment fragment = new DetailMyBookingFragmentBuilder(adapter.getItem(position).getCtBookingCode()).build();
                String tagName = DetailMyBookingFragment.class.getSimpleName() + adapter.getItem(position).getCtBookingCode();
                mListener.gotoPage(fragment, false, tagName);
            }
        });

        scrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override public void onLoadMore(int page, int totalItemsCount) {
                if (!adapter.isLoading()) {
                    presenter.loadData(false, page, status);
                }
            }
        };
        loadData(false);
    }

    @Override public void setData(RequestTransaction data) {
        if (adapter.getItemCount() > 0) {
            adapter.clear();
        }
        if (data.getStatus()) {
            emptyView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            adapter.setItems(data.getTransactions());
            if (data.getTransactions().size() < PAGINATION_LIMIT) {
                recyclerView.removeOnScrollListener(scrollListener);
            }
        } else {
            if (adapter.getItemCount() == 0) {
                emptyView.setVisibility(View.VISIBLE);
                emptyView.setText(data.getText());
                recyclerView.setVisibility(View.GONE);
            } else {
                recyclerView.removeOnScrollListener(scrollListener);
            }
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        scrollListener.resetState();
        recyclerView.addOnScrollListener(scrollListener);
        presenter.loadData(pullToRefresh, 1, status);
    }

    @Override public void onRetryNextPage() {
        adapter.setErrorNextPage(null);
        presenter.loadData(false, scrollListener.getCurrentPage(), status);
    }

    @Override public void showLoadingNextPage(boolean show) {
        adapter.setLoadingNextPage(show);
    }

    @Override public void setNextData(RequestTransaction data) {
        if (data.getStatus()) {
            adapter.setItems(data.getTransactions());
            if (data.getTransactions().size() < PAGINATION_LIMIT) {
                recyclerView.removeOnScrollListener(scrollListener);
            }
        } else {
            recyclerView.removeOnScrollListener(scrollListener);
        }
    }

    @Override public void showErrorNextPage(String string) {
        showToast(string);
        adapter.setErrorNextPage("Tap to retry");
        scrollListener.backToPreviousPage();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventRefresh(EventRefreshBookingList event) {
        if (getContext() != null){
            loadData(false);
        }
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventDataNotification(DataNotification dataNotification) {
        String s = dataNotification.getMethod().toLowerCase();
        if (s.equals("booking") && getContext() != null) {
            loadData(false);
        }
    }

    @Override public void onAttach(Context context) {
        super.onAttach(context);
        EventBus.getDefault().register(this);
    }

    @Override public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }
}