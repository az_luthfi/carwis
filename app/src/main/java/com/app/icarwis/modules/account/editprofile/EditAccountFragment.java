package com.app.icarwis.modules.account.editprofile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.icarwis.R;
import com.app.icarwis.adapter.SimpleAdapter;
import com.app.icarwis.base.BaseMvpLceFragment;
import com.app.icarwis.listeners.OnChangeToolbar;
import com.app.icarwis.models.customer.Customer;
import com.app.icarwis.models.customer.RequestCustomer;
import com.app.icarwis.models.datalist.SimpleList;
import com.app.icarwis.utility.CommonUtilities;
import com.app.icarwis.utility.DialogListOption;
import com.app.icarwis.utility.ProgressRequestBody;
import com.app.icarwis.utility.Str;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;
import com.yalantis.ucrop.util.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.app.icarwis.utility.Config.APP_PREFIX;
import static com.app.icarwis.utility.Config.CUSTOMER_IMAGE_CROP_HEIGHT;
import static com.app.icarwis.utility.Config.CUSTOMER_IMAGE_CROP_WIDTH;
import static com.app.icarwis.utility.Config.ID_PREFIX;

public class EditAccountFragment extends BaseMvpLceFragment<RelativeLayout, RequestCustomer, XEditAccountView, EditAccountPresenter>
        implements XEditAccountView, ProgressRequestBody.UploadCallbacks, OnChangeToolbar {
    private static final int CHOICE_IMAGE_FROM_GALLERY = 8874;
    private static final String CAMERA_FILE_NAME = "cameraFileName";
    @BindView(R.id.ivCustomerImage) CircleImageView ivCustomerImage;
    @BindView(R.id.tvCustomerId) TextView tvCustomerId;
    @BindView(R.id.etName) MaterialEditText etName;
    @BindView(R.id.etEmail) MaterialEditText etEmail;
    @BindView(R.id.etPhone) MaterialEditText etPhone;
    @BindView(R.id.etAddress) MaterialEditText etAddress;
    @BindView(R.id.etProvince) MaterialEditText etProvince;
    @BindView(R.id.tvUpdate) TextView tvUpdate;
    @BindView(R.id.contentView) RelativeLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.fabEditPhoto) FloatingActionButton fabEditPhoto;
    @BindView(R.id.radioWni) RadioButton radioWni;
    @BindView(R.id.radioWna) RadioButton radioWna;
    @BindView(R.id.radioGender) RadioGroup radioGender;

    private SimpleAdapter adapter;
    private File fileImage;
    private String cameraFileName;
    private Customer customer;
    private ProgressDialog progressBar;

    public EditAccountFragment() {

    }

    @Override public EditAccountPresenter createPresenter() {
        return new EditAccountPresenter(getContext(), getActivity());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_edit_account;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new SimpleAdapter();
        loadData(false);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return getContext().getString(R.string.error_network);
    }

    @Override public void setData(RequestCustomer data) {
        if (data.getStatus()) {
            customer = data.getCustomer();
            if (!TextUtils.isEmpty(data.getCustomer().getCustomerImage())) {
                Picasso.with(getContext()).load(data.getCustomer().getCustomerImage()).placeholder(R.drawable.person_default).into(ivCustomerImage);
            }
            if (customer.getCustomerKewarganegaraan().equals("wni")){
                radioWni.setChecked(true);
            }else if (customer.getCustomerKewarganegaraan().equals("wna")){
                radioWna.setChecked(true);
            }
            tvCustomerId.setText(APP_PREFIX + data.getCustomer().getCustomerId() + ID_PREFIX);
            etName.setText(data.getCustomer().getCustomerName());
            etEmail.setText(data.getCustomer().getCustomerEmail());
            etPhone.setText(Str.getText(data.getCustomer().getCustomerMsisdn(), ""));
            etAddress.setText(Str.getText(data.getCustomer().getCustomerAddress(), ""));
            etProvince.setText(Str.getText(data.getCustomer().getCustomerProvince(), ""));

        }

    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadData(pullToRefresh);
    }

    @OnClick({R.id.etProvince, R.id.tvUpdate, R.id.fabEditPhoto}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.etProvince:
                presenter.loadProvince();
                break;
            case R.id.tvUpdate:
                validationInput();
                break;
            case R.id.fabEditPhoto:
                presenter.requestPermissionGallery();
                break;
        }
    }

    private void validationInput() {
        if (TextUtils.isEmpty(etName.getText())) {
            etName.requestFocus();
            etName.setError("Nama lengkap tidak boleh kosong");
            return;
        }
        if (TextUtils.isEmpty(etPhone.getText())) {
            etPhone.requestFocus();
            etPhone.setError("No HP tidak boleh kosong");
            return;
        }
        if (etPhone.getText().length() < 10) {
            etPhone.requestFocus();
            etPhone.setError("No HP tidak sesuai");
            return;
        }
        if (TextUtils.isEmpty(etAddress.getText())) {
            etAddress.requestFocus();
            etAddress.setError("Alamat tidak boleh kosong");
            return;
        }
        if (TextUtils.isEmpty(etProvince.getText())) {
            showToast("Provinsi harus dipilih");
            return;
        }
        String kewarganegaraan = null;
        if (radioWni.isChecked()) {
            kewarganegaraan = "wni";
        }
        if (radioWna.isChecked()) {
            kewarganegaraan = "wna";
        }
        if (TextUtils.isEmpty(kewarganegaraan)) {
            showToast("Anda belum memilih kewarganegaraan");
            return;
        }
        HashMap<String, RequestBody> params = new HashMap<>();
        params.put("name", RequestBody.create(MediaType.parse("multipart/form-data"), etName.getText().toString()));
        params.put("msisdn", RequestBody.create(MediaType.parse("multipart/form-data"), etPhone.getText().toString()));
        params.put("address", RequestBody.create(MediaType.parse("multipart/form-data"), etAddress.getText().toString()));
        params.put("province", RequestBody.create(MediaType.parse("multipart/form-data"), etProvince.getText().toString()));
        params.put("photo", RequestBody.create(MediaType.parse("multipart/form-data"), customer.getCustomerImageName()));
        params.put("kewarganegaraan", RequestBody.create(MediaType.parse("multipart/form-data"), kewarganegaraan));
        presenter.requestUpdateProfile(params, getRequestImage());
    }

    @Override public void showDialog(ArrayList<? extends SimpleList> data, String title) {
        if (adapter.getItemCount() > 0) {
            adapter.clear();
        }
        adapter.addAll(data);
        new DialogListOption(getContext(), title, adapter) {
            @Override public void onItemClicked(int position) {
                if (position != -1) {
                    etProvince.setText(adapter.getItem(position).getName());
                }
            }

            @Override public void onFilter(CharSequence s) {
                adapter.filter(s);
            }
        };
    }

    @Override public void imageFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "Pilih Gambar"), CHOICE_IMAGE_FROM_GALLERY);
    }

    @Override public void showLoadingUpdate(boolean show) {
        if (show) {
            progressBar = new ProgressDialog(getActivity());
            progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressBar.setMessage("Proses Upload.. ");
            progressBar.setTitle("");
            progressBar.setMax(100);
            progressBar.setCancelable(false);
            progressBar.setProgress(0);
            progressBar.show();
        } else {
            if (progressBar != null) {
                progressBar.cancel();
            }
        }
    }

    @Override public void gotoBack() {
        mListener.back();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CHOICE_IMAGE_FROM_GALLERY) {
                final Uri selectedUri = data.getData();
                if (selectedUri != null) {
                    startCrop(selectedUri);
                } else {
                    showToast("Tidak dapat mengambil gambar");
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                onSelectImage(UCrop.getOutput(data));
            }
        }
        if (resultCode == UCrop.RESULT_ERROR) {
            handleCropError(data);
        }

    }

    private void startCrop(Uri uri) {
        UCrop.of(uri, getTarget())
                .withAspectRatio(1, 1)
                .withMaxResultSize(CUSTOMER_IMAGE_CROP_WIDTH, CUSTOMER_IMAGE_CROP_HEIGHT)
                .start(getContext(), EditAccountFragment.this);
    }

    private Uri getTarget() {
        cameraFileName = CommonUtilities.getTargetImage();
        return Uri.fromFile(new File(cameraFileName));
    }

    private void onSelectImage(Uri selectedImage) {
        if (selectedImage != null) {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContext().getContentResolver().query(
                    selectedImage, filePathColumn, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                fileImage = new File(cursor.getString(columnIndex));
                cursor.close();
            } else {
                String filePath = FileUtils.getPath(getContext(), selectedImage);
                fileImage = new File(filePath);
            }
            new Handler().post(new Runnable() {
                public void run() {
                    Picasso.with(getContext()).load(fileImage).into(ivCustomerImage);
                }
            });

        } else {
            Toast.makeText(getContext(), R.string.error_pick_image, Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Toast.makeText(getContext(), cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getContext(), "Gagal memotong gambar", Toast.LENGTH_SHORT).show();
        }
    }

    public MultipartBody.Part getRequestImage() {
        if (fileImage != null) {
            // create RequestBody instance from file
            ProgressRequestBody requestFile = null;
            try {
                requestFile = new ProgressRequestBody(fileImage, MediaType.parse("multipart/form-data"), this);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            // MultipartBody.Part is used to send also the actual file name
            if (requestFile != null) {
                return MultipartBody.Part.createFormData("img", fileImage.getName(), requestFile);
            } else {
                return null;
            }
        }
        return null;
    }

    @Override public void onProgressUpdate(long uploaded, long lenght, int percentage) {

    }

    @Override public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString(CAMERA_FILE_NAME, cameraFileName);
    }

    @Override public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            cameraFileName = savedInstanceState.getString(CAMERA_FILE_NAME);
        }
    }

    @Nullable @Override public String getTitle() {
        return "Ubah Profil";
    }

    @Override public boolean isSearch() {
        return false;
    }

    @Override public boolean isCart() {
        return false;
    }

}