package com.app.icarwis.modules.help.list;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XMessageListPresenter extends MvpPresenter<XMessageListView> {

    void loadListPesan(boolean pullToRefresh);

    void loadDataNextPage(int page);
}