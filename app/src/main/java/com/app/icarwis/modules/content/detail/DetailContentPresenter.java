package com.app.icarwis.modules.content.detail;

import android.content.Context;

import com.app.icarwis.base.BasePresenter;


public class DetailContentPresenter extends BasePresenter<XDetailContentView>
        implements XDetailContentPresenter {

    public DetailContentPresenter(Context context) {
        super(context);
    }

}