package ${packageName};

import android.content.Context;

import ${applicationPackage}.base.BaseMvpPresenter;

public class ${className}Presenter extends BaseMvpPresenter<X${className}View>
        implements X${className}Presenter{

    public ${className}Presenter(Context context)
    {
        super(context);
    }

}