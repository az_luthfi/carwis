package com.app.icarwis.modules.account.editprofile;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public interface XEditAccountPresenter extends MvpPresenter<XEditAccountView> {

    void loadData(boolean pullToRefresh);

    void loadProvince();

    void requestPermissionGallery();

    void requestUpdateProfile(HashMap<String, RequestBody> params, MultipartBody.Part requestImage);
}