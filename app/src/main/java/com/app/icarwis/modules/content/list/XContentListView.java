package com.app.icarwis.modules.content.list;


import com.app.icarwis.models.berita.RequestContent;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XContentListView extends MvpLceView<RequestContent> {

    void resetAdapter();

    void showLoadingNextPage(boolean show);

    void showErrorNextPage(String msg);

    void setNextData(RequestContent data);
}
