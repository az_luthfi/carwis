package com.app.icarwis.modules.order.wisata.car;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import java.util.HashMap;

public interface XCarListPresenter extends MvpPresenter<XCarListView> {

    void loadData(boolean pullToRefresh, HashMap<String, String> params);
}