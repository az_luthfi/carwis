package com.app.icarwis.modules.order.paketwisata.list;

import android.content.Context;

import com.app.icarwis.R;
import com.app.icarwis.app.App;
import com.app.icarwis.base.BaseRxLcePresenter;
import com.app.icarwis.models.wisata.RequestWisata;
import com.app.icarwis.utility.Config;

import java.util.HashMap;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class PaketWisataPresenter extends BaseRxLcePresenter<XPaketWisataView, RequestWisata>
        implements XPaketWisataPresenter {

    public PaketWisataPresenter(Context context) {
        super(context);
    }

    @Override public void loadData(boolean pullToRefresh, int page) {
        HashMap<String, String> params = new HashMap<>();
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(Config.CUSTOMER_ID)));
        params.put("page", String.valueOf(page));
        params.put("act", "paket_wisata");

        Observable<RequestWisata> observable = App.getService().apiWisata(params);
        if (page == 1) {
            subscribe(observable, pullToRefresh);
        } else {
            if (isViewAttached()) {
                getView().showLoadingNextPage(true);
            }
            Observer<RequestWisata> observer1 = new Observer<RequestWisata>() {
                @Override public void onSubscribe(Disposable d) {

                }

                @Override public void onNext(RequestWisata data) {
                    if (isViewAttached()) {
                        getView().showLoadingNextPage(false);
                        getView().setNextData(data);
                    }
                }

                @Override public void onError(Throwable e) {
                    if (isViewAttached()) {
                        getView().showErrorNextPage(context.getString(R.string.error_network));
                    }
                }

                @Override public void onComplete() {

                }
            };

            observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(observer1);
        }
    }
}