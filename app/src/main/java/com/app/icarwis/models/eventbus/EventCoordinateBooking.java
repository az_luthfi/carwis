package com.app.icarwis.models.eventbus;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Luthfi Aziz on 13/11/2017.
 */

public class EventCoordinateBooking {
    public static final String TYPE_JEMPUT = "type_jemput";
    public static final String TYPE_PULANG = "type_pulang";

    private LatLng latLng;
    private String address;
    private String type;

    public EventCoordinateBooking(LatLng latLng, String address, String type) {
        this.latLng = latLng;
        this.address = address;
        this.type = type;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
