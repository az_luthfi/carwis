package com.app.icarwis.modules.help.add;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.icarwis.R;
import com.app.icarwis.base.BaseMvpFragment;
import com.app.icarwis.listeners.OnChangeToolbar;
import com.app.icarwis.utility.CommonUtilities;
import com.app.icarwis.utility.Logs;
import com.app.icarwis.utility.ProgressRequestBody;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;
import com.yalantis.ucrop.util.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;

import butterknife.BindView;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.MultipartBody;

import static com.app.icarwis.utility.Config.MESSAGE_IMAGE_CROP_HEIGHT;
import static com.app.icarwis.utility.Config.MESSAGE_IMAGE_CROP_WIDTH;


public class AddMessageFragment extends BaseMvpFragment<XAddMessageView, AddMessagePresenter>
        implements XAddMessageView, ProgressRequestBody.UploadCallbacks, OnChangeToolbar {

    private static final int CHOICE_IMAGE_FROM_GALLERY = 912;

    @BindView(R.id.etDevisi) MaterialEditText etDevisi;
    @BindView(R.id.etSubject) MaterialEditText etSubject;
    @BindView(R.id.etMessage) MaterialEditText etMessage;
    @BindView(R.id.ivAttachmentImage) ImageView ivAttachmentImage;
    @BindView(R.id.ivAddImage) ImageView ivAddImage;
    @BindView(R.id.tvAttachmentName) TextView tvAttachmentName;
    @BindView(R.id.layImageGallery) RelativeLayout layImageGallery;
    @BindView(R.id.btnSend) Button btnSend;

    private String cameraFileName;
    private File fileImage;
    private ProgressDialog progressBar;
    private static final String TAG = AddMessageFragment.class.getSimpleName();

    public AddMessageFragment() {

    }

    @Override public AddMessagePresenter createPresenter() {
        return new AddMessagePresenter(getContext(), getActivity());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_pesan_add;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @OnClick({R.id.etDevisi, R.id.layImageGallery, R.id.btnSend}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.etDevisi:
                PopupMenu popupMenu = new PopupMenu(getContext(), etDevisi);
                popupMenu.getMenuInflater().inflate(R.menu.divisi_popup_menu, popupMenu.getMenu());

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.option_transaksi:
                                etDevisi.setText("Transaksi");
                                return true;
//                            case R.id.option_deposit:
//                                etDevisi.setText("Deposit");
//                                return true;
                            case R.id.option_kritik:
                                etDevisi.setText("Kritik dan Saran");
                                return true;
                            default:
                                return false;
                        }
                    }
                });
                popupMenu.show();
                break;
            case R.id.layImageGallery:
                presenter.requestPermissionGallery();
                break;
            case R.id.btnSend:
                btnSend.setEnabled(false);
                CommonUtilities.hideSoftKeyboard(getActivity());
                if (isValidateForm()) {
                    presenter.sendMessage(getRequestImage(), etDevisi.getText().toString(), etSubject.getText().toString(), etMessage.getText().toString());
                } else {
                    btnSend.setEnabled(true);
                }
                break;
        }
    }

    private boolean isValidateForm() {
        if (TextUtils.isEmpty(etDevisi.getText())) {
            etDevisi.requestFocus();
            etDevisi.setError("Silahkan pilih devisi");
            return false;
        }
        if (TextUtils.isEmpty(etSubject.getText())) {
            etSubject.requestFocus();
            etSubject.setError("Silahkan isi sibjek");
            return false;
        }
        if (etMessage.length() < 2) {
            etMessage.requestFocus();
            etMessage.setError("Silahkan isi pesan");
            return false;
        }
        return true;
    }

    @Override public void showProgressDialog(boolean show) {
        if (show) {
            progressBar = new ProgressDialog(getActivity());
            progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressBar.setMessage("Proses Upload.. ");
            progressBar.setTitle("");
            progressBar.setMax(100);
            progressBar.setCancelable(false);
            progressBar.setProgress(0);
            progressBar.show();
        } else {
            if (progressBar != null && progressBar.isShowing()) {
                progressBar.cancel();
            }
        }
    }

    @Override public void imageFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "Pilih Gambar"), CHOICE_IMAGE_FROM_GALLERY);
    }

    @Override public void showDialogSuccess(String text) {
        btnSend.setEnabled(true);
        showConfirmDialog(SweetAlertDialog.SUCCESS_TYPE, "Upload berhasil", text, "OK", null, "success", true);
        etDevisi.setText("");
        etSubject.setText("");
        etMessage.setText("");
        ivAttachmentImage.setVisibility(View.GONE);
        fileImage = null;
    }

    @Override public void showDialogError(String string) {
        btnSend.setEnabled(true);
        showConfirmDialog(SweetAlertDialog.WARNING_TYPE, "Peringatan", string, "Retry", "Tutup", "retry", true);
    }

    @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logs.d(TAG, "requestCode : " + requestCode);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CHOICE_IMAGE_FROM_GALLERY) {
                Uri selectedUri = data.getData();
                if (selectedUri != null) {
                    startCrop(selectedUri);
                } else {
                    showToast("Tidak dapat mengambil gambar");
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                onSelectImage(UCrop.getOutput(data));
            }
        }
        if (resultCode == UCrop.RESULT_ERROR) {
            handleCropError(data);
        }

    }

    private void startCrop(Uri uri) {
        UCrop.of(uri, Uri.fromFile(new File(CommonUtilities.getTargetImage())))
                .withMaxResultSize(MESSAGE_IMAGE_CROP_WIDTH, MESSAGE_IMAGE_CROP_HEIGHT)
                .start(getContext(), AddMessageFragment.this);
    }

    private void onSelectImage(Uri selectedImage) {
        if (selectedImage != null) {
            String filePath = FileUtils.getPath(getContext(), selectedImage);
            fileImage = new File(filePath);
            Logs.d(TAG, "imge size : " + CommonUtilities.size((double) fileImage.length()));
            ivAttachmentImage.setVisibility(View.VISIBLE);
            tvAttachmentName.setText(fileImage.getPath());
            Picasso.with(getContext()).load(fileImage).into(ivAttachmentImage);
        } else {
            Toast.makeText(getContext(), R.string.error_pick_image, Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Toast.makeText(getContext(), cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getContext(), "Gagal memotong gambar", Toast.LENGTH_SHORT).show();
        }
    }

    public MultipartBody.Part getRequestImage() {
        if (fileImage != null) {
            // create RequestBody instance from file
            ProgressRequestBody requestFile = null;
            try {
                requestFile = new ProgressRequestBody(fileImage, MediaType.parse("multipart/form-data"), this);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            // MultipartBody.Part is used to send also the actual file name
            if (requestFile != null) {
                return MultipartBody.Part.createFormData("img", fileImage.getName(), requestFile);
            } else {
                return null;
            }
        }
        return null;
    }

    @Override public void onProgressUpdate(long uploaded, long lenght, int percentage) {
        if (progressBar.isShowing()) {
            progressBar.setProgress(percentage);
        }
    }

    @Override public void onAlertConfirm(String action) {
        switch (action) {
            case "retry":
                presenter.sendMessage(getRequestImage(), etDevisi.getText().toString(), etSubject.getText().toString(), etMessage.getText().toString());
                break;
            case "success":
                mListener.back();
                break;
        }
    }

    @Nullable @Override public String getTitle() {
        return "Tambah Pesan";
    }

    @Override public boolean isSearch() {
        return false;
    }

    @Override public boolean isCart() {
        return false;
    }
}