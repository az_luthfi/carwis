package ${packageName};

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface X${className}Presenter extends MvpPresenter<X${className}View> {

}