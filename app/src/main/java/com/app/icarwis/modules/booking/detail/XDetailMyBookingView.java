package com.app.icarwis.modules.booking.detail;

import com.app.icarwis.models.transaction.RequestTransaction;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XDetailMyBookingView extends MvpLceView<RequestTransaction> {

    void shoProgressDialog(boolean show);
}