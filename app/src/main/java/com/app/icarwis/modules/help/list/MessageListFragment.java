package com.app.icarwis.modules.help.list;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.icarwis.R;
import com.app.icarwis.adapter.message.MessageAdapter;
import com.app.icarwis.base.BaseLceRefreshFragment;
import com.app.icarwis.holder.ItemLoadingHolder;
import com.app.icarwis.listeners.OnChangeToolbar;
import com.app.icarwis.models.pesan.Message;
import com.app.icarwis.models.pesan.RequestMessage;
import com.app.icarwis.modules.help.add.AddMessageFragment;
import com.app.icarwis.modules.help.conversation.ConversationFragmentBuilder;
import com.app.icarwis.utility.listview.EndlessRecyclerOnScrollListener;
import com.app.icarwis.utility.listview.ItemClickSupport;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class MessageListFragment extends BaseLceRefreshFragment<SwipeRefreshLayout, RequestMessage, XMessageListView, MessageListPresenter>
        implements XMessageListView, ItemLoadingHolder.ItemViewLoadingListener, OnChangeToolbar {

    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.emptyView) TextView emptyView;
    @BindView(R.id.contentView) SwipeRefreshLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.addMessage) FloatingActionButton addMessage;

    private int maxCountItems;
    private MessageAdapter adapter;
    private EndlessRecyclerOnScrollListener scrollListener;

    public MessageListFragment() {

    }

    @Override public MessageListPresenter createPresenter() {
        return new MessageListPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_pesan_list;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new MessageAdapter(this);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                mListener.gotoPage(new ConversationFragmentBuilder(adapter.getItem(position).getIdPesan(), adapter.getItem(position).getStatus()).build(), false, null);
            }
        });

        scrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (!adapter.isLoading()) {
                    if (adapter.getItemCount() < maxCountItems) {
                        presenter.loadDataNextPage(page);
                    }
                }
            }
        };

        recyclerView.addOnScrollListener(scrollListener);

        loadData(false);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        if (pullToRefresh) {
            return getContext().getString(R.string.error_network_light);
        }
        return getContext().getString(R.string.error_network);
    }

    @Override public void setData(RequestMessage data) {
        if (data.getStatus()) {
            if (adapter != null && adapter.getItemCount() > 0){
                adapter.clear();
            }
            recyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            maxCountItems = data.getCount();
            adapter.setItems(data.getMessages());
        } else {
            recyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
            emptyView.setText(data.getText());
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadListPesan(pullToRefresh);
    }

    @Override public void showLoadingNextPage(boolean show) {
        adapter.setLoadingNextPage(show);
    }

    @Override public void showErrorNextPage(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
        adapter.setErrorNextPage("Tap to retry");
        scrollListener.backToPreviousPage();
    }

    @Override public void setNextData(RequestMessage data) {
        if (data.getStatus()) {
            adapter.setItems(data.getMessages());
        }
    }

    @Override public void addNewItem(Message message) {
        if (adapter.getItemCount() == 0){
            RequestMessage requestMessage = new RequestMessage();
            requestMessage.setStatus(true);
            requestMessage.setCount(1);
            ArrayList<Message> messages = new ArrayList<>();
            messages.add(message);
            requestMessage.setMessages(messages);
            setData(requestMessage);
        }else{
            adapter.addNewItem(message);
        }
    }

    @Override public void onRetryNextPage() {
        adapter.setErrorNextPage(null);
        presenter.loadDataNextPage(scrollListener.getCurrentPage());
    }

    @OnClick(R.id.addMessage) public void onViewClicked() {
        mListener.gotoPage(new AddMessageFragment(), false, null);
    }

    @Nullable @Override public String getTitle() {
        return "Customer Service";
    }

    @Override public boolean isSearch() {
        return false;
    }

    @Override public boolean isCart() {
        return false;
    }
}