package com.app.icarwis.modules.content.page;


import com.app.icarwis.models.berita.RequestContent;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XStaticPageView extends MvpLceView<RequestContent> {

}