package com.app.icarwis.modules.order.carrental.carparent;

import com.app.icarwis.models.car.RequestCar;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XCarParentView extends MvpLceView<RequestCar> {

}