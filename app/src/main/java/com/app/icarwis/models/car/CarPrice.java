package com.app.icarwis.models.car;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Luthfi Aziz on 20/11/2017.
 */

public class CarPrice {
    @SerializedName("ccp_id")
    @Expose
    private String ccpId;
    @SerializedName("ccr_id")
    @Expose
    private String ccrId;
    @SerializedName("ccp_hour")
    @Expose
    private String ccpHour;
    @SerializedName("ccp_amount")
    @Expose
    private String ccpAmount;
    @SerializedName("ccp_amount_disc")
    @Expose
    private String ccpAmountDisc;

    public String getCcpId() {
        return ccpId;
    }

    public void setCcpId(String ccpId) {
        this.ccpId = ccpId;
    }

    public String getCcrId() {
        return ccrId;
    }

    public void setCcrId(String ccrId) {
        this.ccrId = ccrId;
    }

    public String getCcpHour() {
        return ccpHour;
    }

    public void setCcpHour(String ccpHour) {
        this.ccpHour = ccpHour;
    }

    public String getCcpAmount() {
        return ccpAmount;
    }

    public void setCcpAmount(String ccpAmount) {
        this.ccpAmount = ccpAmount;
    }

    public String getCcpAmountDisc() {
        return ccpAmountDisc;
    }

    public void setCcpAmountDisc(String ccpAmountDisc) {
        this.ccpAmountDisc = ccpAmountDisc;
    }
}
