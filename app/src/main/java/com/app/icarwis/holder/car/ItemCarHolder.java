package com.app.icarwis.holder.car;

import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.icarwis.R;
import com.app.icarwis.models.car.Car;
import com.app.icarwis.utility.CommonUtilities;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemCarHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.ivImage) ImageView ivImage;
    @BindView(R.id.tvName) TextView tvName;
    @BindView(R.id.tvDiscount) TextView tvDiscount;
    @BindView(R.id.tvPrice) TextView tvPrice;
    @BindView(R.id.layPrice) LinearLayout layPrice;
    @BindView(R.id.ivWifi) ImageView ivWifi;
    @BindView(R.id.ivGas) ImageView ivGas;
    @BindView(R.id.ivDriver) ImageView ivDriver;
    @BindView(R.id.layFacility) LinearLayout layFacility;

    public ItemCarHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_car, parent, false));
    }

    public ItemCarHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
        tvDiscount.setPaintFlags(tvDiscount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    public void bindView(Car model) {
        if (!TextUtils.isEmpty(model.getMediaValue())) {
            Picasso.with(itemView.getContext()).load(model.getMediaValue()).placeholder(R.color.md_grey_500).resize(300, 0).into(ivImage);
        } else {
            ivImage.setImageResource(R.color.md_grey_500);
        }
        tvName.setText(model.getCcrName());
        if (model.getCcpAmountDisc() != null && model.getCcpAmount() < model.getCcpAmountDisc()) {
            tvDiscount.setVisibility(View.VISIBLE);
            tvDiscount.setText(CommonUtilities.toRupiahNumberFormat(model.getCcpAmountDisc()));
            tvPrice.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.md_red_500));
        } else {
            tvDiscount.setVisibility(View.GONE);
            tvPrice.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.colorAccent));
        }
        tvPrice.setText(CommonUtilities.toRupiahNumberFormat(model.getCcpAmount()));
        if (model.getCcrIsDriver().equals("1")) {
            ivDriver.setVisibility(View.VISIBLE);
        } else {
            ivDriver.setVisibility(View.GONE);
        }
        if (model.getCcrIsGasoline().equals("1")) {
            ivGas.setVisibility(View.VISIBLE);
        } else {
            ivGas.setVisibility(View.GONE);
        }
        ivWifi.setVisibility(View.GONE);
    }
}
