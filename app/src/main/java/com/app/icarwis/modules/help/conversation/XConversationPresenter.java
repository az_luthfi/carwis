package com.app.icarwis.modules.help.conversation;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import okhttp3.MultipartBody;

public interface XConversationPresenter extends MvpPresenter<XConversationView> {

    void loadMessage(String messageId);

    void loadDataNextPage(int page, String messageId);

    void requestPermissionGallery();

    void requestSend(String messageId, String text, MultipartBody.Part requestImage, String lastId);

    void loadDataLast(String lastId, String messageId);
}