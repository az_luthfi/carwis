package com.app.icarwis.modules.account.balance;

import android.content.Context;

import com.app.icarwis.base.BasePresenter;

public class MyBalancePresenter extends BasePresenter<XMyBalanceView>
        implements XMyBalancePresenter {

    public MyBalancePresenter(Context context) {
        super(context);
    }

}