package com.app.icarwis.modules.order.wisata.car;

import android.content.Context;

import com.app.icarwis.app.App;
import com.app.icarwis.base.BaseRxLcePresenter;
import com.app.icarwis.models.car.RequestCar;

import java.util.HashMap;

public class CarListPresenter extends BaseRxLcePresenter<XCarListView, RequestCar>
        implements XCarListPresenter {

    public CarListPresenter(Context context) {
        super(context);
    }

    @Override public void loadData(boolean pullToRefresh, HashMap<String, String> params) {
        params.put("act", "list");

        subscribe(App.getService().apiCar(params), pullToRefresh);
    }
}