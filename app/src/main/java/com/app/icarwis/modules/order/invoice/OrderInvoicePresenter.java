package com.app.icarwis.modules.order.invoice;

import android.content.Context;

import com.app.icarwis.R;
import com.app.icarwis.app.App;
import com.app.icarwis.base.BaseRxLcePresenter;
import com.app.icarwis.models.transaction.RequestTransaction;
import com.app.icarwis.utility.Config;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class OrderInvoicePresenter extends BaseRxLcePresenter<XOrderInvoiceView, RequestTransaction>
        implements XOrderInvoicePresenter {

    public OrderInvoicePresenter(Context context) {
        super(context);
    }

    @Override public void loadPaymentMethod(boolean pullToRefresh, HashMap<String, String> params) {
        params.put("act", "payment_method");

        subscribe(App.getService().apiTransaction(params), pullToRefresh);
    }

    @Override public void requestBooking(HashMap<String, String> params) {
        if (isViewAttached()) {
            getView().showProgressDialog(true);
        }
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(Config.CUSTOMER_ID)));
        params.put("act", "add");
        Observer<RequestTransaction> observer = new Observer<RequestTransaction>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestTransaction data) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().onNextBooking(data);
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().showError(context.getString(R.string.error_network_light));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiTransaction(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }


}