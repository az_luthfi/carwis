package com.app.icarwis.modules.order.carrental.list;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import java.util.HashMap;

public interface XCarListPresenter extends MvpPresenter<XCarListView> {

    void loadData(boolean pullToRefresh, String hour, int page, HashMap<String, String> params);
}