package com.app.icarwis.modules.splash.login;

import com.app.icarwis.models.customer.Customer;
import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface XLoginView extends MvpView {

    void showProgressDialog(boolean show);


    void showError(String text);

    void onSuccessAuth(Customer customer);

    void onSuccessResetPassword(String text);
}