package com.app.icarwis.utility;

import android.content.Context;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by j3p0n on 12/31/2016.
 */

public class Validation {

    public static boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public static boolean isEmpty(EditText editText, String errorMsg) {
        if (TextUtils.isEmpty(editText.getText())) {
            setError(editText, errorMsg);
            return true;
        }
        return false;
    }

    public static boolean isEmpty(Context c, TextView textView, String errorMsg) {
        if (TextUtils.isEmpty(textView.getText())) {
            Toast.makeText(c, errorMsg, Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    public static void setError(EditText editText, String errorMsg) {
        editText.setError(errorMsg);
        editText.requestFocus();
    }
}
