package com.app.icarwis.modules.order.carrental.carparent;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.icarwis.R;
import com.app.icarwis.base.BaseMvpLceFragment;
import com.app.icarwis.listeners.OnChangeToolbar;
import com.app.icarwis.models.car.Car;
import com.app.icarwis.models.car.CarPrice;
import com.app.icarwis.models.car.RequestCar;
import com.app.icarwis.modules.order.carrental.list.CarListFragmentBuilder;
import com.app.icarwis.modules.order.pemesan.PemesanFragment;
import com.app.icarwis.modules.order.pemesan.PemesanFragmentBuilder;
import com.app.icarwis.utility.SmartFragmentStatePagerAdapter;
import com.google.gson.Gson;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;

@FragmentWithArgs
public class CarParentFragment extends BaseMvpLceFragment<FrameLayout, RequestCar, XCarParentView, CarParentPresenter>
        implements XCarParentView, OnChangeToolbar {

    @Arg HashMap<String, String> params;
    @BindView(R.id.tabLayout) TabLayout tabLayout;
    @BindView(R.id.viewPager) ViewPager viewPager;
    @BindView(R.id.contentView) FrameLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.dataView) LinearLayout dataView;
    @BindView(R.id.emptyView) TextView emptyView;

    private ArrayList<CarPrice> carPrices = new ArrayList<>();
    private CarPagerAdapter pagerAdapter;

    public CarParentFragment() {

    }

    @Override public CarParentPresenter createPresenter() {
        return new CarParentPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_car_parent;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData(false);
    }

    @Override public void setData(RequestCar data) {
        if (data.getStatus()) {
            carPrices = data.getCarPrices();
            if (carPrices.size() > 5) {
                tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
            }
            dataView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            pagerAdapter = new CarPagerAdapter(getChildFragmentManager());
            viewPager.setAdapter(pagerAdapter);
            tabLayout.setupWithViewPager(viewPager);
        } else {
            dataView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
            emptyView.setText(data.getText());
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadData();
    }

    @Override public String getTitle() {
        return "Pilih Mobil";
    }

    @Override public boolean isSearch() {
        return false;
    }

    @Override public boolean isCart() {
        return false;
    }


    public void onCarSelected(Car car) {
        mListener.gotoPage(new PemesanFragmentBuilder(params, PemesanFragment.TYPE_CAR_RENTAL).jsonCar(new Gson().toJson(car)).build(), false, null);
    }

    public class CarPagerAdapter extends SmartFragmentStatePagerAdapter {

        public CarPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override public Fragment getItem(int position) {
            return new CarListFragmentBuilder(carPrices.get(position).getCcpHour(), params).build();
        }

        @Override public int getCount() {
            return carPrices.size();
        }

        @Override public CharSequence getPageTitle(int position) {
            return new StringBuilder(carPrices.get(position).getCcpHour()).append(" Jam");
        }
    }
}