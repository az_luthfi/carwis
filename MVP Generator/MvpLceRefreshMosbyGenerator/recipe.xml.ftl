<?xml version="1.0"?>
<recipe>
    <dependency mavenUrl="com.android.support:support-v4:${buildApi}.+"/>

    <#if includeLayout>
        <instantiate from="res/layout/fragment_blank.xml.ftl"
                       to="${escapeXmlAttribute(resOut)}/layout/fragment_${classToResource(className)}.xml" />

        <open file="${escapeXmlAttribute(resOut)}/layout/fragment_${classToResource(className)}.xml" />
    </#if>

    <open file="${escapeXmlAttribute(srcOut)}/${className}Fragment.java" />

    <open file="${escapeXmlAttribute(srcOut)}/${className}Presenter.java" />

    <open file="${escapeXmlAttribute(srcOut)}/X${className}View.java" />

    <open file="${escapeXmlAttribute(srcOut)}/X${className}Presenter.java" />

    <instantiate from="src/app_package/MvpFragment.java.ftl"
                   to="${escapeXmlAttribute(srcOut)}/${className}Fragment.java" />

    <instantiate from="src/app_package/MvpPresenter.java.ftl"
                    to="${escapeXmlAttribute(srcOut)}/${className}Presenter.java" />

    <instantiate from="src/app_package/XMvpView.java.ftl"
                        to="${escapeXmlAttribute(srcOut)}/X${className}View.java" />

    <instantiate from="src/app_package/XMvpPresenter.java.ftl"
                            to="${escapeXmlAttribute(srcOut)}/X${className}Presenter.java" />
</recipe>