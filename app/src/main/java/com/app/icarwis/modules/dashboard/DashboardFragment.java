package com.app.icarwis.modules.dashboard;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.MenuItem;
import android.view.View;

import com.app.icarwis.R;
import com.app.icarwis.base.BaseFragment;
import com.app.icarwis.models.eventbus.EventDashboard;
import com.app.icarwis.modules.account.profile.MyProfileFragment;
import com.app.icarwis.modules.booking.MyBookingFragment;
import com.app.icarwis.modules.help.list.MessageListFragment;
import com.app.icarwis.modules.home.HomeFragment;
import com.app.icarwis.utility.BottomNavigationViewHelper;
import com.app.icarwis.utility.NonSwipeableViewPager;
import com.app.icarwis.utility.SmartFragmentStatePagerAdapter;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
@FragmentWithArgs
public class DashboardFragment extends BaseFragment implements BottomNavigationView.OnNavigationItemSelectedListener {
    @Arg int position;

    @BindView(R.id.viewPager) NonSwipeableViewPager viewPager;
    @BindView(R.id.bottom_navigation) BottomNavigationView bottomNavigation;

    private DashboardPagerAdapter pagerAdapter;

    public DashboardFragment() {
        // Required empty public constructor
    }


    @Override protected int getLayoutRes() {
        return R.layout.fragment_dashboard;
    }

    @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pagerAdapter = new DashboardPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        viewPager.setCurrentItem(position);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigation);

        bottomNavigation.setOnNavigationItemSelectedListener(this);
    }

    @Override public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                viewPager.setCurrentItem(0);
                break;
            case R.id.action_booking:
                viewPager.setCurrentItem(1);
                break;
            case R.id.action_help:
                viewPager.setCurrentItem(2);
                break;
            case R.id.action_profile:
                viewPager.setCurrentItem(3);
                break;
        }
        return true;
    }

    public class DashboardPagerAdapter extends SmartFragmentStatePagerAdapter {

        public DashboardPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override public Fragment getItem(int position) {
            switch (position) {
                case 1:
                    return new MyBookingFragment();
                case 2:
                    return new MessageListFragment();
                case 3:
                    return new MyProfileFragment();
                default:
                    return new HomeFragment();
            }

        }

        @Override public int getCount() {
            return 4;
        }
    }

    public void setCurrentItem(int position) {
        if (viewPager.getCurrentItem() == position){
            return;
        }
        viewPager.setCurrentItem(position);
        switch (position) {
            case 0:
                bottomNavigation.setSelectedItemId(R.id.action_home);
                break;
            case 1:
                bottomNavigation.setSelectedItemId(R.id.action_booking);
                break;
            case 2:
                bottomNavigation.setSelectedItemId(R.id.action_help);
                break;
            case 3:
                bottomNavigation.setSelectedItemId(R.id.action_profile);
                break;
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventDahsboard(EventDashboard event) {
        if (getContext() != null){
            setCurrentItem(event.getPosition());
        }
    }

    @Override public void onAttach(Context context) {
        super.onAttach(context);
        EventBus.getDefault().register(this);
    }

    @Override public void onDetach() {
        EventBus.getDefault().unregister(this);
        super.onDetach();
    }
}
