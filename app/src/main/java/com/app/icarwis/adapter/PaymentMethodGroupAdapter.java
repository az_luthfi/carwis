package com.app.icarwis.adapter;

import com.app.icarwis.base.BaseRecyclerViewAdapter;
import com.app.icarwis.holder.ItemPaymentMethodGroupHolder;
import com.app.icarwis.models.transaction.PaymentGroup;

/**
 * Created by Luthfi Aziz on 08/12/2017.
 */
public class PaymentMethodGroupAdapter extends BaseRecyclerViewAdapter<PaymentGroup, ItemPaymentMethodGroupHolder> {

    private final Listener listener;
    public PaymentMethodGroupAdapter(Listener listener) {
        super(ItemPaymentMethodGroupHolder.class);
        this.listener = listener;
    }

    @Override protected void populateViewHolder(ItemPaymentMethodGroupHolder viewHolder, PaymentGroup model, int position) {
        viewHolder.bindView(model, listener);
    }

    public interface Listener{
        void onSelected(int positionGroup, int positionPayment);
    }
}