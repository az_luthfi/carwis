package com.app.icarwis.modules.home;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.icarwis.R;
import com.app.icarwis.base.BaseMvpLceFragment;
import com.app.icarwis.holder.SliderItem;
import com.app.icarwis.models.berita.Content;
import com.app.icarwis.models.berita.RequestContent;
import com.app.icarwis.modules.content.detail.DetailContentFragmentBuilder;
import com.app.icarwis.modules.order.form.FormBookingFragment;
import com.app.icarwis.modules.order.form.FormBookingFragmentBuilder;
import com.app.icarwis.modules.order.paketwisata.list.PaketWisataFragment;
import com.app.icarwis.utility.CommonUtilities;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class HomeFragment extends BaseMvpLceFragment<RelativeLayout, RequestContent, XHomeView, HomePresenter>
        implements XHomeView, BaseSliderView.OnSliderClickListener {

    @BindView(R.id.slider) SliderLayout slider;
    @BindView(R.id.indicator) PagerIndicator indicator;
    @BindView(R.id.layRentCarWisata) FrameLayout layRentCarWisata;
    @BindView(R.id.layRentCar) FrameLayout layRentCar;
    @BindView(R.id.layPaketWisata) FrameLayout layPaketWisata;
    @BindView(R.id.contentView) NestedScrollView contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;

    private ArrayList<SliderItem> sliderItems = new ArrayList<>();
    private ArrayList<Content> contents = new ArrayList<>();

    public HomeFragment() {

    }

    @Override public HomePresenter createPresenter() {
        return new HomePresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_home;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData(false);
    }

    @Override public void setData(RequestContent data) {
        if (data.getStatus()) {
            contents = new ArrayList<>(data.getContents());
            if (sliderItems.size() == 0) {
                sliderItems = new ArrayList<>();
                SliderItem sliderItem;
                for (int i = 0; i < contents.size(); i++) {
                    sliderItem = new SliderItem(getContext());
                    sliderItem.image(contents.get(i).getContentImage()).setScaleType(BaseSliderView.ScaleType.CenterCrop);
                    sliderItem.description(CommonUtilities.toHtml(contents.get(i).getContentName()).toString());
                    //add your extra information
                    sliderItem.bundle(new Bundle());
                    sliderItem.getBundle().putInt("position", i);
                    sliderItem.setOnSliderClickListener(this);
                    sliderItems.add(sliderItem);
                }
            }
            if (slider != null) {
                slider.removeAllSliders();
                slider.stopAutoCycle();
                for (int i = 0; i < sliderItems.size(); i++) {
                    slider.addSlider(sliderItems.get(i));
                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                                slider.startAnimation(AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_in));
                            slider.setVisibility(View.VISIBLE);
                            slider.startAutoCycle();
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    }
                }, 1000);

            }
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
       presenter.loadData(pullToRefresh);
    }

    @OnClick({R.id.layRentCarWisata, R.id.layRentCar, R.id.layPaketWisata}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layRentCarWisata:
                mListener.gotoPage(new FormBookingFragmentBuilder(FormBookingFragment.RENTCAR_WISATA).build(), false, null);
                break;
            case R.id.layRentCar:
                mListener.gotoPage(new FormBookingFragmentBuilder(FormBookingFragment.RENTCAR_MOBIL).build(), false, null);
                break;
            case R.id.layPaketWisata:
                mListener.gotoPage(new PaketWisataFragment(), false, null);
                break;
        }
    }

    @Override public void onSliderClick(BaseSliderView slider) {
        String jsonContent = new Gson().toJson(contents.get(slider.getBundle().getInt("position")));
        mListener.gotoPage(new DetailContentFragmentBuilder(jsonContent).build(), false, null);
    }
}