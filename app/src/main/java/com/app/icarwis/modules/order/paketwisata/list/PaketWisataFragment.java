package com.app.icarwis.modules.order.paketwisata.list;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.icarwis.R;
import com.app.icarwis.adapter.wisata.PaketWisataAdapter;
import com.app.icarwis.base.BaseMvpLceFragment;
import com.app.icarwis.holder.ItemLoadingHolder;
import com.app.icarwis.listeners.OnChangeToolbar;
import com.app.icarwis.models.wisata.RequestWisata;
import com.app.icarwis.modules.order.paketwisata.detail.PaketWisataDetailFragmentBuilder;
import com.app.icarwis.utility.listview.EndlessRecyclerOnScrollListener;
import com.app.icarwis.utility.listview.ItemClickSupport;
import com.google.gson.Gson;

import butterknife.BindView;

import static com.app.icarwis.utility.Config.PAGINATION_LIMIT;

public class PaketWisataFragment extends BaseMvpLceFragment<RelativeLayout, RequestWisata, XPaketWisataView, PaketWisataPresenter>
        implements XPaketWisataView, ItemLoadingHolder.ItemViewLoadingListener, OnChangeToolbar {

    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.emptyView) TextView emptyView;
    @BindView(R.id.contentView) RelativeLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.container) FrameLayout container;

    private PaketWisataAdapter adapter;
    private EndlessRecyclerOnScrollListener scrollListener;

    public PaketWisataFragment() {

    }

    @Override public PaketWisataPresenter createPresenter() {
        return new PaketWisataPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_with_recyclerview;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new PaketWisataAdapter(this);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        scrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override public void onLoadMore(int page, int totalItemsCount) {
                if (!adapter.isLoading()) {
                    presenter.loadData(false, page);
                }
            }
        };

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                mListener.gotoPage(new PaketWisataDetailFragmentBuilder(new Gson().toJson(adapter.getItem(position))).build(), false, null);
            }
        });
        loadData(false);
    }

    @Override public void setData(RequestWisata data) {
        if (adapter.getItemCount() > 0) {
            adapter.clear();
        }
        if (data.getStatus()) {
            emptyView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            adapter.setItems(data.getPaketWisatas());
            if (data.getWisatas().size() < PAGINATION_LIMIT) {
                recyclerView.removeOnScrollListener(scrollListener);
            }
        } else {
            if (adapter.getItemCount() == 0) {
                emptyView.setVisibility(View.VISIBLE);
                emptyView.setText(data.getText());
                recyclerView.setVisibility(View.GONE);
            } else {
                recyclerView.removeOnScrollListener(scrollListener);
            }
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadData(false, 1);
    }

    @Override public void showLoadingNextPage(boolean show) {
        adapter.setLoadingNextPage(show);
    }

    @Override public void setNextData(RequestWisata data) {
        if (data.getStatus()) {
            adapter.setItems(data.getPaketWisatas());
            if (data.getWisatas().size() < PAGINATION_LIMIT) {
                recyclerView.removeOnScrollListener(scrollListener);
            }
        } else {
            recyclerView.removeOnScrollListener(scrollListener);
        }
    }

    @Override public void showErrorNextPage(String string) {
        showToast(string);
        adapter.setErrorNextPage("Tap to retry");
        scrollListener.backToPreviousPage();
    }

    @Override public void onRetryNextPage() {
        adapter.setErrorNextPage(null);
        presenter.loadData(false, scrollListener.getCurrentPage());
    }

    @Override public String getTitle() {
        return "Paket Wisata";
    }

    @Override public boolean isSearch() {
        return false;
    }

    @Override public boolean isCart() {
        return false;
    }
}