package com.app.icarwis.modules.order.wisata.car;

import com.app.icarwis.models.car.RequestCar;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XCarListView extends MvpLceView<RequestCar> {

}