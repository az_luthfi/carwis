package com.app.icarwis.modules.order.invoice;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.icarwis.R;
import com.app.icarwis.adapter.PaymentMethodGroupAdapter;
import com.app.icarwis.adapter.wisata.WisataCartPriceAdapter;
import com.app.icarwis.base.BaseMvpLceFragment;
import com.app.icarwis.models.car.Car;
import com.app.icarwis.models.eventbus.EventRefreshBookingList;
import com.app.icarwis.models.transaction.PaymentGroup;
import com.app.icarwis.models.transaction.RequestTransaction;
import com.app.icarwis.models.wisata.PaketWisata;
import com.app.icarwis.models.wisata.RequestWisata;
import com.app.icarwis.modules.booking.detail.DetailMyBookingFragment;
import com.app.icarwis.modules.booking.detail.DetailMyBookingFragmentBuilder;
import com.app.icarwis.utility.CommonUtilities;
import com.app.icarwis.utility.Str;
import com.app.icarwis.utility.listview.DividerItemDecoration;
import com.google.gson.Gson;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

@FragmentWithArgs
public class OrderInvoiceFragment extends BaseMvpLceFragment<RelativeLayout, RequestTransaction, XOrderInvoiceView, OrderInvoicePresenter>
        implements XOrderInvoiceView, PaymentMethodGroupAdapter.Listener {
    private static final String TAG = OrderInvoiceFragment.class.getSimpleName();
    @Arg HashMap<String, String> params;
    @Arg int typePesan; // PemesananFragment
    @Arg(required = false) String jsonCar;
    @Arg(required = false) String jsonPaketWisata;
    @Arg(required = false) String jsonRequestWisata;

    @BindView(R.id.recyclerView2) RecyclerView recyclerView2;
    @BindView(R.id.layWisata) LinearLayout layWisata;
    @BindView(R.id.tvCarName) TextView tvCarName;
    @BindView(R.id.tvCarAmount) TextView tvCarAmount;
    @BindView(R.id.layRentCar) LinearLayout layRentCar;
    @BindView(R.id.tvPaketWisataName) TextView tvPaketWisataName;
    @BindView(R.id.tvAmountPaketWisata) TextView tvAmountPaketWisata;
    @BindView(R.id.layPaketWisata) LinearLayout layPaketWisata;
    @BindView(R.id.etVoucher) EditText etVoucher;
    @BindView(R.id.btnVoucher) Button btnVoucher;
    @BindView(R.id.bntSubmit) TextView bntSubmit;
    @BindView(R.id.tvTotalAmount) TextView tvTotalAmount;
    @BindView(R.id.etNote) EditText etNote;
    @BindView(R.id.rvPaymentMethod) RecyclerView rvPaymentMethod;
    @BindView(R.id.contentView) RelativeLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;

    private Car car;
    private PaketWisata paketWisata;
    private RequestWisata requestWisata;
    private WisataCartPriceAdapter priceAdapter;
    private Double total = 0.0;
    private PaymentMethodGroupAdapter adapter;
    private String bookingCode;

    public OrderInvoiceFragment() {

    }

    @Override public OrderInvoicePresenter createPresenter() {
        return new OrderInvoicePresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_order_invoice;
    }

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (jsonRequestWisata != null) {
            requestWisata = new Gson().fromJson(jsonRequestWisata, RequestWisata.class);
        }
        if (jsonCar != null) {
            car = new Gson().fromJson(jsonCar, Car.class);
        }
        if (jsonPaketWisata != null) {
            paketWisata = new Gson().fromJson(jsonPaketWisata, PaketWisata.class);
        }
        params.put("typeBooking", String.valueOf(typePesan));
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new PaymentMethodGroupAdapter(this);
        rvPaymentMethod.setHasFixedSize(true);
        rvPaymentMethod.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rvPaymentMethod.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.LIST_VERTICAL));
        rvPaymentMethod.setAdapter(adapter);
        rvPaymentMethod.setNestedScrollingEnabled(false);
        ViewCompat.setNestedScrollingEnabled(rvPaymentMethod, false);

        if (requestWisata != null) {
            layWisata.setVisibility(View.VISIBLE);
            priceAdapter = new WisataCartPriceAdapter();
            recyclerView2.setHasFixedSize(true);
            recyclerView2.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
            recyclerView2.setNestedScrollingEnabled(false);
            ViewCompat.setNestedScrollingEnabled(recyclerView2, false);
            recyclerView2.setAdapter(priceAdapter);
            priceAdapter.setItems(requestWisata.getWisatas());
            total += requestWisata.getSubtotal();
        }
        if (car != null) {
            layRentCar.setVisibility(View.VISIBLE);
            tvCarName.setText(new StringBuilder("- ").append(car.getCcrName()));
            tvCarAmount.setText(CommonUtilities.toRupiahNumberFormat(car.getCcpAmount()));
            total += car.getCcpAmount();
        }
        if (paketWisata != null) {
            layPaketWisata.setVisibility(View.VISIBLE);
            tvPaketWisataName.setText(new StringBuilder("- ").append(paketWisata.getCpwName()));
            tvAmountPaketWisata.setText(CommonUtilities.toRupiahNumberFormat(paketWisata.getCpwAmount()));
            total += paketWisata.getCpwAmount();
        }
        tvTotalAmount.setText(CommonUtilities.toRupiahNumberFormat(total));

        loadData(false);
    }


    @OnClick({R.id.btnVoucher, R.id.bntSubmit}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnVoucher:
                break;
            case R.id.bntSubmit:
                bntSubmit.setEnabled(false);
                PaymentGroup paymentGroupSelected = null;
                for (PaymentGroup paymentGroup : adapter.getList()) {
                    if (paymentGroup.isSelected()) {
                        paymentGroupSelected = paymentGroup;
                        break;
                    }
                }
                if (paymentGroupSelected == null) {
                    showToast("Pilih metode pembayaran");
                    bntSubmit.setEnabled(true);
                    return;
                }
                params.put("payment_method_id", String.valueOf(paymentGroupSelected.getPaymentMethodId()));
                params.put("invoiceNote", Str.getText(etNote.getText().toString(), ""));
                if (car != null) {
                    params.put("ccp_id", car.getCcpId());
                    params.put("ccr_id", car.getCcrId());
                }
                if (paketWisata != null) {
                    params.put("cpw_id", paketWisata.getCpwId());
                }
                presenter.requestBooking(params);
                break;
        }
    }

    @Override public void showProgressDialog(boolean show) {
        if (show) {
            showLoading("Loading...", false);
        } else {
            hideLoading();
        }
    }

    @Override public void onNextBooking(RequestTransaction data) {
        if (data.getStatus()) {
            EventBus.getDefault().post(new EventRefreshBookingList());
            bookingCode = data.getBookingCode();
            showConfirmDialog(SweetAlertDialog.SUCCESS_TYPE, "Pemesanan Berhasil", data.getText(), "OK", null, "success_booking", false);
        } else {
            showErrorDialog(data.getText());
            bntSubmit.setEnabled(true);
        }
    }

    @Override public void showError(String string) {
        showErrorDialog(string);
        bntSubmit.setEnabled(true);
    }

    @Override public void onAlertConfirm(String action) {
        switch (action) {
            case "success_booking":
                DetailMyBookingFragment fragment = new DetailMyBookingFragmentBuilder(bookingCode).build();
                String tagName = DetailMyBookingFragment.class.getSimpleName() + bookingCode;
                mListener.gotoPage(fragment, false, tagName);
                break;
            case "payement_false":
                mListener.back();
                break;
        }
    }

    @Override public void setData(RequestTransaction data) {
        if (adapter.getItemCount() > 0) {
            adapter.clear();
        }
        if (data.getStatus()) {
            adapter.setItems(data.getPaymentGroups());
        } else {
            showConfirmDialog(SweetAlertDialog.WARNING_TYPE, "Peringatan", data.getText(), "Tutup", null, "payement_false", false);
        }

    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadPaymentMethod(pullToRefresh, params);
    }

    @Override public void onSelected(int positionGroup, int positionPayment) {
        for (int i = 0; i < adapter.getItemCount(); i++) {
            if (adapter.getItem(i).isSelected()) {
                adapter.getItem(i).setSelected(false);
                adapter.getItem(i).setPositionPayment(-1);
            }
        }
        adapter.getItem(positionGroup).setSelected(true);
        adapter.getItem(positionGroup).setPositionPayment(positionPayment);
        adapter.notifyDataSetChanged();
    }
}