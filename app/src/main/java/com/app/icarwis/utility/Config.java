package com.app.icarwis.utility;

import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by j3p0n on 12/3/2016.
 */
public class Config {

    public static final String BASE_URL = "http://icarindo.com/apicarwis/v2dev/";
    static final String APP_NAME = ".-iCarwis-.";
    public static final String APP_NAME_BASE = "iCarwis";
    static final boolean DEBUG_MODE = true;
    static final String LOG_TAG = ".-iCarwis-.";
    public static final String GOOGLE_SIGN_IN_API = ".-iCarwis-.";
    static final String AUTH_KEY = "49b3aa4b7e9732b75e4a4908994822fb";
//    public static final String APP_FILE_PROVIDER = "com.app.carwis.fileprovider";

    public static final String APP_PREFIX = "ICW";
    public static final String ID_PREFIX = "879";

    //Konstanta
    public static final String CUSTOMER_ID = "customer_id";
    public static final String CUSTOMER_EMAIL = "customer_email";
    public static final String CUSTOMER_NAME = "customer_name";
    public static final String CUSTOMER_REG_ID = "customer_reg_id";
    public static final String CUSTOMER_SALDO = "customer_saldo";
    public static final String CUSTOMER_AVATAR = "customer_image";
    public static final String CUSTOMER_PIN = "customer_pin";
    public static final String CUSTOMER_PHONE = "customer_phone";
    public static final String AVATAR_UPLOADED = "avatar_uploaded";
    public static final String ECOMMERCE_MODE = "ecommerce_mode";
    public static final String ECOMMERCE_STATUS = "ecommerce_status";
    public static final String PREFS_WISATA_CART = "prefs_wisata_cart";
    public static final int NOTIFICATION_ID = 575855;
    public static final int NOTIFICATION_MESSAGE_ID = 575856;

    public static final int MIN_LENGHT_PASSWORD = 2;
    public static final int PAGINATION_LIMIT = 12;


    //Product type
    public static final String PT_PLN_TOKEN = "Pembelian Token PLN";
    public static final String PT_PLN_PASCA = "Pembayaran PLN Pasca Bayar";
    public static final String PDAM = "Pembayaran PDAM";
    public static final String HP_PASCA = "Pembayaran HP Pasca Bayar";
    public static final String TELKOM = "Pembayaran Telkom / Speedy";
    public static final String FINANCE = "Pembayaran Finance";
    public static final String TV_CABLE = "Pembayaran TV Cable";
    public static final String PULSA = "Pembelian Pulsa";
    public static final String VOUCHER = "Pembelian Voucher";
    public static final String BPJS = "Pembayaran BPJS Kesehatan";
    public static final String PAKET_DATA_INTERNET = "Pembelian Paket Data Internet";

    // Json data notification firebase
    public static final String JSON_DATA_NOTIFICATION = "json_data_notification";


    public static final int MESSAGE_IMAGE_CROP_WIDTH = 640; // 1:1
    public static final int MESSAGE_IMAGE_CROP_HEIGHT = 640; // 1:1

    public static final int CUSTOMER_IMAGE_CROP_WIDTH = 640; // 1:1
    public static final int CUSTOMER_IMAGE_CROP_HEIGHT = 640; // 1:1

    public static HttpLoggingInterceptor.Level getLogLevel() {
        if (DEBUG_MODE) {
            return HttpLoggingInterceptor.Level.BODY;
        }else{
            return HttpLoggingInterceptor.Level.NONE;
        }
    }
}
