package com.app.icarwis.modules.order.wisata.detailwisata;

import android.content.Context;

import com.app.icarwis.base.BasePresenter;

public class DetailWisataPresenter extends BasePresenter<XDetailWisataView>
        implements XDetailWisataPresenter {

    public DetailWisataPresenter(Context context) {
        super(context);
    }

}