package com.app.icarwis.modules.help.add;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import okhttp3.MultipartBody;

public interface XAddMessagePresenter extends MvpPresenter<XAddMessageView> {

    void requestPermissionGallery();

    void sendMessage(MultipartBody.Part requestImage, String devisi, String subject, String pesan);
}