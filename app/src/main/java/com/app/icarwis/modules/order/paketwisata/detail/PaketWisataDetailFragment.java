package com.app.icarwis.modules.order.paketwisata.detail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import com.app.icarwis.R;
import com.app.icarwis.base.BaseMvpFragment;
import com.app.icarwis.listeners.OnChangeToolbar;
import com.app.icarwis.models.wisata.PaketWisata;
import com.app.icarwis.modules.order.form.FormBookingFragment;
import com.app.icarwis.modules.order.form.FormBookingFragmentBuilder;
import com.app.icarwis.utility.CommonUtilities;
import com.google.gson.Gson;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import butterknife.BindView;
import butterknife.OnClick;

@FragmentWithArgs
public class PaketWisataDetailFragment extends BaseMvpFragment<XPaketWisataDetailView, PaketWisataDetailPresenter>
        implements XPaketWisataDetailView, OnChangeToolbar {
    @Arg String jsonPaketWisata;
    @BindView(R.id.tvTitle) TextView tvTitle;
    @BindView(R.id.wvDesc) WebView wvDesc;
    @BindView(R.id.bntSubmit) TextView bntSubmit;
    @BindView(R.id.tvAmount) TextView tvAmount;

    private PaketWisata paketWisata;

    public PaketWisataDetailFragment() {

    }

    @Override public PaketWisataDetailPresenter createPresenter() {
        return new PaketWisataDetailPresenter(getContext());
    }

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        paketWisata = new Gson().fromJson(jsonPaketWisata, PaketWisata.class);
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_paket_wisata_detail;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        WebSettings ws = wvDesc.getSettings();
        ws.setDefaultFontSize(16);
        ws.setBuiltInZoomControls(false);
        ws.setDisplayZoomControls(false);
        ViewCompat.setNestedScrollingEnabled(wvDesc, false);

        tvTitle.setText(paketWisata.getCpwName());
        wvDesc.loadData(paketWisata.getCpwDesc() != null ? paketWisata.getCpwDesc() : "", "text/html", "UTF-8");
        tvAmount.setText(CommonUtilities.toRupiahNumberFormat(paketWisata.getCpwAmount()));

    }

    @OnClick(R.id.bntSubmit) public void onViewClicked() {
        mListener.gotoPage(new FormBookingFragmentBuilder(FormBookingFragment.PAKET_WISATA).jsonPaketWisata(jsonPaketWisata).build(), false, null);
    }

    @Override public String getTitle() {
        return "Detail Paket Wisata";
    }

    @Override public boolean isSearch() {
        return false;
    }

    @Override public boolean isCart() {
        return false;
    }
}