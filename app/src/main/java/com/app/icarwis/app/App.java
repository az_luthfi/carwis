package com.app.icarwis.app;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.app.icarwis.apis.ApiService;


/**
 * Created by j3p0n on 12/3/2016.
 */
public class App extends Application {

    private static ApiService apiService;

    @Override
    public void onCreate() {
        super.onCreate();
        RestClient restClient = new RestClient();
        apiService = restClient.getApiService();

    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static ApiService getService() {
        return apiService;
    }
}
