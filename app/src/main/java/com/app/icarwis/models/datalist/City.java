package com.app.icarwis.models.datalist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by luthfiaziz on 19/02/18.
 */

public class City implements SimpleList{
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("city_name")
    @Expose
    private String cityName;

    public String getCityId() {
        return cityId;
    }

    public String getCityName() {
        return cityName;
    }

    @Override public String getName() {
        return cityName;
    }

    @Override public String getId() {
        return cityId;
    }
}
