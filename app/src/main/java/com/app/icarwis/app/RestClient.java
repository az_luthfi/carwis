package com.app.icarwis.app;


import android.os.Build;

import com.app.icarwis.apis.ApiService;
import com.app.icarwis.utility.Config;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.TlsVersion;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.app.icarwis.utility.Config.BASE_URL;

/**
 * Created by j3p0n on 12/30/2016.
 */

public class RestClient {
    private ApiService apiService;

    RestClient() {
        Retrofit.Builder retrofit = new Retrofit.Builder()
                .client(getHttpClient(false))
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create());


        apiService = retrofit.baseUrl(BASE_URL).build().create(ApiService.class);
    }

    public ApiService getApiService() {
        return apiService;
    }

    private OkHttpClient getHttpClient(boolean isHttps) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(Config.getLogLevel());

        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(logging)
                .addNetworkInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request().newBuilder().addHeader("Connection", "close").build();
                        return chain.proceed(request);
                    }
                });
        if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            if (isHttps) {
                builder.connectionSpecs(createConnectionSpecs());
            }
        }
        return builder.build();
    }
    private static List<ConnectionSpec> createConnectionSpecs(){
        ConnectionSpec spec = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                .tlsVersions(TlsVersion.TLS_1_2)
                .cipherSuites(
                        CipherSuite.TLS_DHE_RSA_WITH_AES_256_GCM_SHA384,
                        CipherSuite.TLS_DHE_RSA_WITH_AES_256_GCM_SHA384)
                .build();
        return Collections.singletonList(spec);
    }
}
