package com.app.icarwis.modules.home;

import com.app.icarwis.models.berita.RequestContent;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XHomeView extends MvpLceView<RequestContent> {

}