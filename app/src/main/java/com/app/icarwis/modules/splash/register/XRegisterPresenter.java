package com.app.icarwis.modules.splash.register;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import java.util.HashMap;

public interface XRegisterPresenter extends MvpPresenter<XRegisterView> {

    void requestPermissionsAccounts();

    void loadProvince();

    void requestRegister(HashMap<String, String> params);

    void loadTermAndCon();

    void loadBank();
}