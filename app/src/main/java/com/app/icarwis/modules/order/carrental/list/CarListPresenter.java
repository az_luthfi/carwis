package com.app.icarwis.modules.order.carrental.list;

import android.content.Context;

import com.app.icarwis.R;
import com.app.icarwis.app.App;
import com.app.icarwis.base.BaseRxLcePresenter;
import com.app.icarwis.models.car.RequestCar;

import java.util.HashMap;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CarListPresenter extends BaseRxLcePresenter<XCarListView, RequestCar>
        implements XCarListPresenter {

    public CarListPresenter(Context context) {
        super(context);
    }

    @Override public void loadData(boolean pullToRefresh, String hour, int page, HashMap<String, String> params) {
        params.put("act", "list-by-hour");
        params.put("hour", hour);
        params.put("page", String.valueOf(page));

        Observable<RequestCar> observable = App.getService().apiCar(params);
        if (page == 1) {
            subscribe(observable, pullToRefresh);
        } else {
            if (isViewAttached()) {
                getView().showLoadingNextPage(true);
            }
            Observer<RequestCar> observer1 = new Observer<RequestCar>() {
                @Override public void onSubscribe(Disposable d) {

                }

                @Override public void onNext(RequestCar data) {
                    if (isViewAttached()) {
                        getView().showLoadingNextPage(false);
                        getView().setNextData(data);
                    }
                }

                @Override public void onError(Throwable e) {
                    if (isViewAttached()) {
                        getView().showErrorNextPage(context.getString(R.string.error_network));
                    }
                }

                @Override public void onComplete() {

                }
            };

            observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(observer1);
        }
    }
}