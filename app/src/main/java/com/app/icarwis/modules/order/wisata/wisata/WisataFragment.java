package com.app.icarwis.modules.order.wisata.wisata;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.icarwis.R;
import com.app.icarwis.adapter.SimpleAdapter;
import com.app.icarwis.adapter.wisata.WisataAdapter;
import com.app.icarwis.base.BaseMvpLceFragment;
import com.app.icarwis.holder.ItemLoadingHolder;
import com.app.icarwis.listeners.OnChangeToolbar;
import com.app.icarwis.models.datalist.Province;
import com.app.icarwis.models.datalist.SimpleList;
import com.app.icarwis.models.eventbus.EventWisataCart;
import com.app.icarwis.models.wisata.RequestWisata;
import com.app.icarwis.models.wisata.Wisata;
import com.app.icarwis.modules.order.wisata.cart2.WisataCart2FragmentBuilder;
import com.app.icarwis.modules.order.wisata.detailwisata.DetailWisataFragment;
import com.app.icarwis.modules.order.wisata.detailwisata.DetailWisataFragmentBuilder;
import com.app.icarwis.utility.CommonUtilities;
import com.app.icarwis.utility.DialogListOption;
import com.app.icarwis.utility.Logs;
import com.app.icarwis.utility.listview.EndlessRecyclerOnScrollListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.greenrobot.eventbus.EventBus;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;

import static com.app.icarwis.utility.Config.PAGINATION_LIMIT;

@FragmentWithArgs
public class WisataFragment extends BaseMvpLceFragment<FrameLayout, RequestWisata, XWisataView, WisataPresenter>
        implements XWisataView, ItemLoadingHolder.ItemViewLoadingListener, WisataAdapter.Listener, OnChangeToolbar, MaterialSearchView.OnQueryTextListener {
    private static final String TAG = WisataFragment.class.getSimpleName();
    @Arg HashMap<String, String> params;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.emptyView) TextView emptyView;
    @BindView(R.id.contentView) FrameLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.container) FrameLayout container;
    @BindView(R.id.bntSubmit) TextView bntSubmit;
    @BindView(R.id.dataView) FrameLayout dataView;
    @BindView(R.id.tvSearch) TextView tvSearch;
    @BindView(R.id.btnResetSearch) Button btnResetSearch;
    @BindView(R.id.laySearch) LinearLayout laySearch;
    @BindView(R.id.tvLocation) TextView tvLocation;
    @BindView(R.id.btnChangeLoacation) Button btnChangeLoacation;
    @BindView(R.id.layLocation) LinearLayout layLocation;

    private WisataAdapter adapter;
    private EndlessRecyclerOnScrollListener scrollListener;
    private String querySearch = "";
    private String idProvince = "";
    private String idCity = "";
    private SimpleAdapter simpleAdapter;
    private ArrayList<Province> provinces = new ArrayList<>();

    private CharSequence[] locationOption = {
            "Sekitar Saya",
            "Pilih Kota"
    };

    private AlertDialog.Builder locationDialog;

    public WisataFragment() {

    }

    @Override public WisataPresenter createPresenter() {
        return new WisataPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_wisata;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        EventBus.getDefault().post(new EventWisataCart(EventWisataCart.TYPE_ADD));
        adapter = new WisataAdapter(this, this, WisataAdapter.TYPE_CHOOSE);
        simpleAdapter = new SimpleAdapter();
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        scrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override public void onLoadMore(int page, int totalItemsCount) {
                if (!adapter.isLoading()) {
                    presenter.loadData(false, params, page, querySearch, idProvince, idCity);
                }
            }
        };
        loadData(false);


    }

    @Override public void setData(RequestWisata data) {
        if (provinces.size() == 0){
            Type mapType = new TypeToken<ArrayList<Province>>() {
            }.getType();
            provinces = new Gson().fromJson(CommonUtilities.loadJSONFromAsset(getContext(), "all_province.json"), mapType);
        }
        if (TextUtils.isEmpty(querySearch)) {
            laySearch.setVisibility(View.GONE);
        } else {
            laySearch.setVisibility(View.VISIBLE);
            tvSearch.setText("pencarian " + "'" + querySearch + "'");
        }
        if (adapter.getItemCount() > 0) {
            adapter.clear();
        }
        if (data.getStatus()) {
            emptyView.setVisibility(View.GONE);
            dataView.setVisibility(View.VISIBLE);
            adapter.setItems(data.getWisatas());
            if (data.getWisatas().size() < PAGINATION_LIMIT) {
                recyclerView.removeOnScrollListener(scrollListener);
            }
        } else {
            if (adapter.getItemCount() == 0) {
                emptyView.setVisibility(View.VISIBLE);
                emptyView.setText(data.getText());
                dataView.setVisibility(View.GONE);
            } else {
                recyclerView.removeOnScrollListener(scrollListener);
            }
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        scrollListener.resetState();
        recyclerView.addOnScrollListener(scrollListener);
        presenter.loadData(pullToRefresh, params, 1, querySearch, idProvince, idCity);
    }

    @Override public void showLoadingNextPage(boolean show) {
        adapter.setLoadingNextPage(show);
    }

    @Override public void setNextData(RequestWisata data) {

        if (data.getStatus()) {
            adapter.setItems(data.getWisatas());
            if (data.getWisatas().size() < PAGINATION_LIMIT) {
                recyclerView.removeOnScrollListener(scrollListener);
            }
        } else {
            recyclerView.removeOnScrollListener(scrollListener);
        }
    }

    @Override public void showErrorNextPage(String string) {
        showToast(string);
        adapter.setErrorNextPage("Tap to retry");
        scrollListener.backToPreviousPage();
    }

    @Override public void showProgressDialog(boolean show) {
        if (show) {
            showLoading("Loading...", false);
        } else {
            hideLoading();
        }
    }

    @Override public void showError(String string) {
        showErrorDialog(string);
    }

    @Override public void onNextDirection(RequestWisata data) {
        if (data.getStatus()) {
//            mListener.gotoPage(new WisataCartFragmentBuilder(new Gson().toJson(data), params).build(), false, null);
        } else {
            showErrorDialog(data.getText());
        }
    }

    @Override public void updateDataAdapter() {
        adapter.notifyDataSetChanged();
    }

    @Override public void onRetryNextPage() {
        adapter.setErrorNextPage(null);
        presenter.loadData(false, params, scrollListener.getCurrentPage(), querySearch, idProvince, idCity);
    }

    @Override public boolean onCheck(final int position, final Wisata wisata, final boolean checked) {
//        try {
//            if (adapter.getItem(position) != null && !adapter.isBusy()) {
//                if (checked) {
//                    adapter.updateItemChecked(wisata, position);
//                } else {
//                    adapter.updateItemUnChecked(position, wisata);
//                }
//                return true;
//            }
//        } catch (IndexOutOfBoundsException iobe) {
//            iobe.printStackTrace();
//        }
        return false;
    }

    @Override public void scrollToPosition(int position) {
//        recyclerView.smoothScrollToPosition(position);
    }

    @Override public void onDetail(int position) {
        String tag = DetailWisataFragment.class.getSimpleName() + "-" + adapter.getItem(position).getCwId();
        mListener.gotoPage(new DetailWisataFragmentBuilder(adapter.getItem(position)).build(), false, tag);
    }

    @OnClick(R.id.bntSubmit) public void onViewClicked() {

        int count = presenter.getPrefs().getWisataCartPreferences().size();
//        if (adapter.getItemCount() == 0) {
//            return;
//        }
//        StringBuilder cwIds = new StringBuilder();
//        int countChecked = 0;
//        for (int i = 0; i < adapter.getItemCount(); i++) {
//            Wisata wisata1 = adapter.getItem(i);
//            if (wisata1.isChecked()) {
//                if (countChecked != 0) {
//                    cwIds.append(",");
//                }
//                cwIds.append(wisata1.getCwId());
//                countChecked += 1;
//            }
//        }
        if (count == 0) {
            showToast("Pilih tujuan wisata");
            return;
        }
        mListener.gotoPage(new WisataCart2FragmentBuilder(params).build(), false, null);
//        if (!TextUtils.isEmpty(params.get("cwIds"))) {
//            params.remove("cwIds");
//        }
//        params.put("cwIds", cwIds.toString());
//        presenter.hitungDirection(params);

    }

    @Override public String getTitle() {
        return "Wisata";
    }

    @Override public boolean isSearch() {
        return true;
    }

    @Override public boolean isCart() {
        return true;
    }

    @Override public boolean onQueryTextSubmit(String query) {
        if (!TextUtils.isEmpty(query)) {
            this.querySearch = query;
        }
        loadData(false);
        return true;
    }

    @Override public boolean onQueryTextChange(String newText) {
        return false;
    }

    @OnClick({R.id.btnResetSearch, R.id.btnChangeLoacation}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnResetSearch:
                querySearch = "";
                loadData(false);
                break;
            case R.id.btnChangeLoacation:
                if (locationDialog == null) {
                    locationDialog = new AlertDialog.Builder(getActivity());
                    locationDialog.setTitle(null);
                    locationDialog.setItems(locationOption, new DialogInterface.OnClickListener() {
                        @Override public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 1:
                                    showDialog(provinces, "Provinsi");
                                    break;
                                default:
                                    idProvince = "";
                                    idCity = "";
                                    tvLocation.setText(locationOption[which]);
                                    loadData(false);
                                    break;
                            }
                        }
                    });
                    locationDialog.create();
                }
                locationDialog.show();
                break;
        }
    }

    public void showDialog(ArrayList<? extends SimpleList> data, final String title) {
        if (simpleAdapter.getItemCount() > 0) {
            simpleAdapter.clear();
        }
        simpleAdapter.addAll(data);
        new DialogListOption(getContext(), title, simpleAdapter) {
            @Override public void onItemClicked(int position) {
                if (position != -1) {
                    switch (title.toLowerCase()) {
                        case "provinsi":
                            idProvince = simpleAdapter.getItem(position).getId();
                            idCity = "";
                            Logs.d(TAG, "onItemClicked ==> " + idProvince);
                            if (idProvince.equals("0")){
                                tvLocation.setText("di " + simpleAdapter.getItem(position).getName());
                                loadData(false);
                            }else{
                                showDialog(provinces.get(position).getCities(), "Kota");
                            }
                            break;
                        case "kota":
                            tvLocation.setText("di " + simpleAdapter.getItem(position).getName());
                            idCity =  simpleAdapter.getItem(position).getId();
                            loadData(false);
                            break;
                    }

                }
            }

            @Override public void onFilter(CharSequence s) {
                simpleAdapter.filter(s);
            }
        };
    }
}