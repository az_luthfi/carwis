package com.app.icarwis.modules.order.wisata.cart2;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import java.util.HashMap;

public interface XWisataCart2Presenter extends MvpPresenter<XWisataCart2View> {

    void loadCart(boolean pullToRefresh, HashMap<String, String> params);

    void hitungDirection(HashMap<String, String> params);
}