package com.app.icarwis.adapter.wisata;

import com.app.icarwis.base.BaseRecyclerViewAdapter;
import com.app.icarwis.holder.wisata.ItemWisataCartPriceHolder;
import com.app.icarwis.models.wisata.Wisata;

/**
 * Created by Luthfi Aziz on 19/11/2017.
 */
public class WisataCartPriceAdapter extends BaseRecyclerViewAdapter<Wisata, ItemWisataCartPriceHolder> {

    public WisataCartPriceAdapter() {
        super(ItemWisataCartPriceHolder.class);
    }

    @Override protected void populateViewHolder(ItemWisataCartPriceHolder viewHolder, Wisata model, int position) {
        viewHolder.bindView(model);
    }
}