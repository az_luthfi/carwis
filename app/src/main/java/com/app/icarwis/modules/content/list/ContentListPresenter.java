package com.app.icarwis.modules.content.list;

import android.content.Context;

import com.app.icarwis.R;
import com.app.icarwis.app.App;
import com.app.icarwis.base.BaseRxLcePresenter;
import com.app.icarwis.models.berita.RequestContent;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.app.icarwis.utility.Config.CUSTOMER_ID;


public class ContentListPresenter extends BaseRxLcePresenter<XContentListView, RequestContent>
        implements XContentListPresenter {

    public ContentListPresenter(Context context) {
        super(context);
    }

    @Override public void loadData(boolean pullToRefresh) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("act", "list");
        params.put("section", "berita");
        params.put("page", "1");
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));

        subscribe(App.getService().apiContentRequest(params), pullToRefresh);

    }

    @Override public void loadDataNextPage(int page) {
        if (isViewAttached()) {
            getView().showLoadingNextPage(true);
        }
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("page", String.valueOf(page));
        params.put("act", "list");
        params.put("section", "berita");
        params.put("page", String.valueOf(page));
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));

        Observer<RequestContent> observer1 = new Observer<RequestContent>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestContent data) {
                if (isViewAttached()) {
                    getView().setNextData(data);
                    getView().showLoadingNextPage(false);
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showErrorNextPage(context.getString(R.string.error_network));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiContentRequest(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer1);
    }

}