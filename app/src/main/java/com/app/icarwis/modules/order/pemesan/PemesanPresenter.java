package com.app.icarwis.modules.order.pemesan;

import android.content.Context;

import com.app.icarwis.base.BasePresenter;

public class PemesanPresenter extends BasePresenter<XPemesanView>
        implements XPemesanPresenter {

    public PemesanPresenter(Context context) {
        super(context);
    }

}