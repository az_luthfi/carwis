package com.app.icarwis.modules.help.list;


import com.app.icarwis.models.pesan.Message;
import com.app.icarwis.models.pesan.RequestMessage;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XMessageListView extends MvpLceView<RequestMessage> {

    void showLoadingNextPage(boolean show);

    void showErrorNextPage(String msg);

    void setNextData(RequestMessage data);

    void addNewItem(Message message);
}