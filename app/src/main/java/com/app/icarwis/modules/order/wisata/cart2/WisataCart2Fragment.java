package com.app.icarwis.modules.order.wisata.cart2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.icarwis.R;
import com.app.icarwis.adapter.wisata.WisataCartAdapter;
import com.app.icarwis.base.BaseLceRefreshFragment;
import com.app.icarwis.models.eventbus.EventWisataCart;
import com.app.icarwis.models.wisata.RequestWisata;
import com.app.icarwis.models.wisata.Wisata;
import com.app.icarwis.modules.order.wisata.car.CarListFragmentBuilder;
import com.app.icarwis.modules.order.wisata.detailwisata.DetailWisataFragment;
import com.app.icarwis.modules.order.wisata.detailwisata.DetailWisataFragmentBuilder;
import com.app.icarwis.utility.CommonUtilities;
import com.app.icarwis.utility.Config;
import com.google.gson.Gson;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

@FragmentWithArgs
public class WisataCart2Fragment extends BaseLceRefreshFragment<SwipeRefreshLayout, RequestWisata, XWisataCart2View, WisataCart2Presenter>
        implements XWisataCart2View, WisataCartAdapter.onActionListener {

    @Arg HashMap<String, String> params;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.emptyView) TextView emptyView;
    @BindView(R.id.tvTotal) TextView tvTotal;
    @BindView(R.id.bntSubmit) TextView bntSubmit;
    @BindView(R.id.contentView) SwipeRefreshLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;

    private WisataCartAdapter adapter;
    int itemsCount = 0;
    int positionDeleted = -1;
    private Double total = 0.0;
    public WisataCart2Fragment() {

    }

    @Override public WisataCart2Presenter createPresenter() {
        return new WisataCart2Presenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_wisata_cart2;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new WisataCartAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
        ViewCompat.setNestedScrollingEnabled(recyclerView, false);

        loadData(false);
    }

    @Override public void setData(RequestWisata data) {
        if (adapter.getItemCount() > 0){
            adapter.clear();
        }
        if (data.getStatus()){
            itemsCount = data.getWisatas().size();
            recyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            adapter.setItems(data.getWisatas());

        }else{
            itemsCount = 0;
            setEmptyCart(data.getText());
        }
        ArrayList<String> wisataCarts = presenter.getPrefs().getWisataCartPreferences();
        if (wisataCarts.size() != adapter.getItemCount()){
            OUTER : for (String id: wisataCarts){
                for (Wisata wisata: adapter.getList()){
                    if (wisata.getCwId().equals(id)){
                        continue OUTER;
                    }
                }
                presenter.getPrefs().removeWisataCartPreference(id);
                EventBus.getDefault().post(new EventWisataCart(EventWisataCart.TYPE_REMOVE));
            }
        }
        setTotal();
    }

    private void setTotal() {
        total = 0.0;
        if (adapter.getItemCount() > 0){
            for (Wisata wisata: adapter.getList()){
                total += (wisata.getAmount() + wisata.getCwService());
            }
        }
        tvTotal.setText(CommonUtilities.toRupiahNumberFormat(total));
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadCart(pullToRefresh, params);
    }

    @OnClick(R.id.bntSubmit) public void onViewClicked() {
        presenter.hitungDirection(params);
    }

    @Override public void onClickUp(int position) {
        if (position - 1 >= 0){
            ArrayList<String> wisataCarts = presenter.getPrefs().getWisataCartPreferences();
            Collections.swap(wisataCarts, position -1, position);
            presenter.getPrefs().savePreferences(Config.PREFS_WISATA_CART, new Gson().toJson(wisataCarts));

            Wisata itemTop = adapter.getItem(position -1);
            Wisata itemCurrent  = adapter.getItem(position);

            adapter.updateItem(position, itemTop);
            adapter.updateItem(position -1, itemCurrent);
            adapter.notifyItemMoved(position, position -1);
        }

    }

    @Override public void onClickDown(int position) {
        if (position + 1 <= itemsCount){
            ArrayList<String> wisataCarts = presenter.getPrefs().getWisataCartPreferences();
            Collections.swap(wisataCarts, position +1, position);
            presenter.getPrefs().savePreferences(Config.PREFS_WISATA_CART, new Gson().toJson(wisataCarts));

            Wisata itemBottom = adapter.getItem(position +1);
            Wisata itemCurrent  = adapter.getItem(position);

            adapter.updateItem(position, itemBottom);
            adapter.updateItem(position +1, itemCurrent);
            adapter.notifyItemMoved(position, position +1);
        }
    }

    @Override public void onClickDelete(int position) {
        positionDeleted = position;
        showConfirmDialog(SweetAlertDialog.WARNING_TYPE, "Peringatan", "Apakah anda yakin akan menghapus wisata ini?", "Hapus", "Batal", "delete-cart", true);
    }

    @Override public void onClickDetail(int position) {
        String tag = DetailWisataFragment.class.getSimpleName() + "-" + adapter.getItem(position).getCwId();
        mListener.gotoPage(new DetailWisataFragmentBuilder(adapter.getItem(position)).build(), false, tag);
    }

    @Override public int getItemsCount() {
        return itemsCount;
    }

    @Override public void setEmptyCart(String text) {
        showContent();
        recyclerView.setVisibility(View.GONE);
        emptyView.setVisibility(View.VISIBLE);
        emptyView.setText(text);
    }

    @Override public void showProgressDialog(boolean show) {
        if (show){
            showLoading("Loading...", false);
        }else{
            hideLoading();
        }
    }

    @Override public void onNextDirection(RequestWisata data) {
        if (data.getStatus()){
            if (!TextUtils.isEmpty(params.get("travelDuration"))) {
                params.remove("travelDuration");
            }
            if (!TextUtils.isEmpty(params.get("visitDuration"))) {
                params.remove("visitDuration");
            }
            params.put("travelDuration", String.valueOf(data.getDuration().getValue()));
            Float totalVisitDuration = 0f;
            for (Wisata wisata1 : adapter.getList()) {
                totalVisitDuration += wisata1.getCwTimeVisit();
            }
            params.put("visitDuration", String.valueOf(totalVisitDuration));

            mListener.gotoPage(new CarListFragmentBuilder(new Gson().toJson(data), params).build(), false, null);
        }else{
            showConfirmDialog(SweetAlertDialog.WARNING_TYPE, "Peringatan", data.getText(), "OK", null, "", true);
        }

    }

    @Override public void showError(String string) {
        showToast(string);
    }

    @Override public void onAlertConfirm(String action) {
        switch (action){
            case "delete-cart":
                presenter.getPrefs().removeWisataCartPreference(adapter.getItem(positionDeleted).getCwId());
                adapter.removeItem(positionDeleted);
                adapter.notifyDataSetChanged();
                itemsCount -=1;
                if (itemsCount == 0){
                    setEmptyCart("Anda belum memilih wisata");
                }
                setTotal();
                EventBus.getDefault().post(new EventWisataCart(EventWisataCart.TYPE_REMOVE));
                break;
        }
    }
}