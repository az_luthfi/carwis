package com.app.icarwis.modules.splash.login;

import android.support.annotation.Nullable;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XLoginPresenter extends MvpPresenter<XLoginView> {

    void resetPassword(String emailReset);

    void requestLogin(String email, @Nullable String password, boolean isSocial);
}