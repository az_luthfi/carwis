package com.app.icarwis.modules.booking.detail;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.icarwis.R;
import com.app.icarwis.adapter.wisata.WisataCartPriceAdapter;
import com.app.icarwis.base.BaseLceRefreshFragment;
import com.app.icarwis.listeners.OnChangeToolbar;
import com.app.icarwis.models.DataNotification;
import com.app.icarwis.models.eventbus.EventDashboard;
import com.app.icarwis.models.transaction.PaymentGroup;
import com.app.icarwis.models.transaction.RequestTransaction;
import com.app.icarwis.models.transaction.Transaction;
import com.app.icarwis.modules.dashboard.DashboardFragmentBuilder;
import com.app.icarwis.utility.CommonUtilities;
import com.app.icarwis.utility.FragmentStack;
import com.app.icarwis.utility.Str;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.midtrans.sdk.corekit.callback.TransactionFinishedCallback;
import com.midtrans.sdk.corekit.core.MidtransSDK;
import com.midtrans.sdk.corekit.core.PaymentMethod;
import com.midtrans.sdk.corekit.core.TransactionRequest;
import com.midtrans.sdk.corekit.core.themes.CustomColorTheme;
import com.midtrans.sdk.corekit.models.BankType;
import com.midtrans.sdk.corekit.models.snap.CreditCard;
import com.midtrans.sdk.corekit.models.snap.TransactionResult;
import com.midtrans.sdk.uikit.SdkUIFlowBuilder;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.OnClick;

import static com.app.icarwis.BuildConfig.BASE_URL;
import static com.app.icarwis.BuildConfig.CLIENT_KEY;

@FragmentWithArgs
public class DetailMyBookingFragment extends BaseLceRefreshFragment<SwipeRefreshLayout, RequestTransaction, XDetailMyBookingView, DetailMyBookingPresenter>
        implements XDetailMyBookingView, OnChangeToolbar, FragmentStack.OnBackPressedHandlingFragment, TransactionFinishedCallback {

    @Arg String bookingCode;
    @BindView(R.id.tvStatusText) TextView tvStatusText;
    @BindView(R.id.tvStatusInfo) TextView tvStatusInfo;
    @BindView(R.id.recyclerView2) RecyclerView recyclerView2;
    @BindView(R.id.layWisata) LinearLayout layWisata;
    @BindView(R.id.tvCarName) TextView tvCarName;
    @BindView(R.id.tvCarAmount) TextView tvCarAmount;
    @BindView(R.id.layRentCar) LinearLayout layRentCar;
    @BindView(R.id.tvPaketWisataName) TextView tvPaketWisataName;
    @BindView(R.id.tvAmountPaketWisata) TextView tvAmountPaketWisata;
    @BindView(R.id.layPaketWisata) LinearLayout layPaketWisata;
    @BindView(R.id.contentView) SwipeRefreshLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.tvPaymentMethod) TextView tvPaymentMethod;
    @BindView(R.id.tvPemesan) TextView tvPemesan;
    @BindView(R.id.tvPenjemputanAddress) TextView tvPenjemputanAddress;
    @BindView(R.id.tvDepartNotes) TextView tvDepartNotes;
    @BindView(R.id.tvKepulanganAddress) TextView tvKepulanganAddress;
    @BindView(R.id.tvReturnNotes) TextView tvReturnNotes;
    @BindView(R.id.layKepulangan) CardView layKepulangan;
    @BindView(R.id.tvAdultText) TextView tvAdultText;
    @BindView(R.id.tvJmlKursiAdult) TextView tvJmlKursiAdult;
    @BindView(R.id.tvJmlKursiChild) TextView tvJmlKursiChild;
    @BindView(R.id.layChild) LinearLayout layChild;
    @BindView(R.id.tvDate) TextView tvDate;
    @BindView(R.id.tvTime) TextView tvTime;
    @BindView(R.id.layForm) CardView layForm;
    @BindView(R.id.layAdult) LinearLayout layAdult;
    @BindView(R.id.tvInvoiceCreateDate) TextView tvInvoiceCreateDate;
    @BindView(R.id.layInvoiceCreateDate) LinearLayout layInvoiceCreateDate;
    @BindView(R.id.tvInvoicePaidDate) TextView tvInvoicePaidDate;
    @BindView(R.id.layInvoicePaidDate) LinearLayout layInvoicePaidDate;
    @BindView(R.id.tvInvoiceProcessDate) TextView tvInvoiceProcessDate;
    @BindView(R.id.layInvoiceProcessDate) LinearLayout layInvoiceProcessDate;
    @BindView(R.id.tvInvoiceFinishDate) TextView tvInvoiceFinishDate;
    @BindView(R.id.layInvoiceFinishDate) LinearLayout layInvoiceFinishDate;
    @BindView(R.id.tvInvoiceCancelDate) TextView tvInvoiceCancelDate;
    @BindView(R.id.layInvoiceCancelDate) LinearLayout layInvoiceCancelDate;
    @BindView(R.id.tvInvoiceRefundDate) TextView tvInvoiceRefundDate;
    @BindView(R.id.layInvoiceRefundDate) LinearLayout layInvoiceRefundDate;
    @BindView(R.id.tvNote) TextView tvNote;
    @BindView(R.id.layNote) CardView layNote;
    //    @BindView(R.id.btnCancel) Button btnCancel;
    @BindView(R.id.btnPay) Button btnPay;
    @BindView(R.id.layActionBottom) LinearLayout layActionBottom;
    @BindView(R.id.tvTotalPrice) TextView tvTotalPrice;
    @BindView(R.id.tvDiscountNominal) TextView tvDiscountNominal;
    @BindView(R.id.tvServiceNominal) TextView tvServiceNominal;
    @BindView(R.id.tvTotalPayment) TextView tvTotalPayment;

    private Transaction transaction;
    private WisataCartPriceAdapter priceAdapter;

    public DetailMyBookingFragment() {

    }

    @Override public DetailMyBookingPresenter createPresenter() {
        return new DetailMyBookingPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_detail_my_booking;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        priceAdapter = new WisataCartPriceAdapter();
        recyclerView2.setHasFixedSize(true);
        recyclerView2.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView2.setNestedScrollingEnabled(false);
        ViewCompat.setNestedScrollingEnabled(recyclerView2, false);
        loadData(false);
    }

    @SuppressLint("SetTextI18n") @Override public void setData(RequestTransaction data) {
        if (data.getStatus()) {
            this.transaction = data.getTransaction();
            tvStatusText.setText(transaction.getCtStatusText());
            tvStatusInfo.setText(transaction.getCtStatusInfoText());
            if (transaction.getRental() != null) {
                layRentCar.setVisibility(View.VISIBLE);
                tvCarName.setText(new StringBuilder("- ").append(transaction.getRental().getCcrName()));
                tvCarAmount.setText(CommonUtilities.toRupiahNumberFormat(transaction.getRental().getCcpAmount()));
            } else {
                layRentCar.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(transaction.getCtNote())) {
                layNote.setVisibility(View.VISIBLE);
                tvNote.setText(transaction.getCtNote());
            } else {
                layNote.setVisibility(View.GONE);
            }
            if (transaction.getWisatas().size() > 0) {
                layWisata.setVisibility(View.VISIBLE);
                if (priceAdapter.getItemCount() > 0){
                    priceAdapter.clear();
                }
                recyclerView2.setAdapter(priceAdapter);
                priceAdapter.setItems(transaction.getWisatas());
            } else {
                layWisata.setVisibility(View.GONE);
            }
            if (transaction.getPaket() != null) {
                layPaketWisata.setVisibility(View.VISIBLE);
                tvPaketWisataName.setText(new StringBuilder("- ").append(transaction.getPaket().getCpwName()));
                tvAmountPaketWisata.setText(CommonUtilities.toRupiahNumberFormat(transaction.getPaket().getCpwAmount()));
            } else {
                layPaketWisata.setVisibility(View.GONE);
            }

            tvTotalPrice.setText(CommonUtilities.toRupiahNumberFormat(transaction.getCtAmountSubtotal()));
            tvDiscountNominal.setText(CommonUtilities.toRupiahNumberFormat(transaction.getCtAmountVoucher()));
            tvServiceNominal.setText(CommonUtilities.toRupiahNumberFormat(transaction.getCtAmountPaymentAdmin()));
            tvTotalPayment.setText(CommonUtilities.toRupiahNumberFormat(transaction.getCtAmountTotal()));


            if (transaction.getPaymentMethodGroup().equals(transaction.getPaymentMethodName())) {
                tvPaymentMethod.setText(transaction.getPaymentMethodName());
            } else {
                tvPaymentMethod.setText(transaction.getPaymentMethodGroup() + " " + transaction.getPaymentMethodName());
            }

            tvPemesan.setText(transaction.getContactName() + "\n" + transaction.getContactEmail() + "\n" + transaction.getContactPhone());

            tvPenjemputanAddress.setText(Str.getText(transaction.getPickupAddress(), ""));
            if (!TextUtils.isEmpty(transaction.getPickupNote())) {
                tvDepartNotes.setVisibility(View.VISIBLE);
                tvDepartNotes.setText(transaction.getPickupNote());
            } else {
                tvDepartNotes.setVisibility(View.GONE);
            }

            if (!transaction.getReturnLat().equals("0")) {
                layKepulangan.setVisibility(View.VISIBLE);
                tvKepulanganAddress.setText(Str.getText(transaction.getReturnAddress(), ""));
                if (!TextUtils.isEmpty(transaction.getReturnNote())) {
                    tvReturnNotes.setVisibility(View.VISIBLE);
                    tvReturnNotes.setText(transaction.getReturnNote());
                }
            } else {
                layKepulangan.setVisibility(View.GONE);
            }


            switch (transaction.getCtType()) {
                case "wisata":
                    tvAdultText.setText("Dewasa (Umur 12+)");
                    tvJmlKursiChild.setText(String.valueOf(transaction.getChVisitorChild()));
                    tvJmlKursiAdult.setText(String.valueOf(transaction.getChVisitorAdult()));
                    break;
                case "paket":
                    layChild.setVisibility(View.GONE);
                    layAdult.setVisibility(View.GONE);
                    break;
                case "rental":
                    layChild.setVisibility(View.GONE);
                    tvAdultText.setText("Jumlah Kursi");
                    tvJmlKursiAdult.setText(String.valueOf(transaction.getChVisitorAdult()));
                    break;
                default:
                    mListener.back();
                    break;
            }

            tvDate.setText(CommonUtilities.getFormatedDate(transaction.getCtBookingTime(), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy"));
            tvTime.setText(CommonUtilities.getFormatedDate(transaction.getCtBookingTime(), "yyyy-MM-dd HH:mm:ss", "HH:mm"));

            if (notEmtpyDate(transaction.getCtCreateDate())) {
                layInvoiceCreateDate.setVisibility(View.VISIBLE);
                tvInvoiceCreateDate.setText(transaction.getCtCreateDate());
            }
            if (notEmtpyDate(transaction.getCtPaidDate())) {
                layInvoicePaidDate.setVisibility(View.VISIBLE);
                tvInvoicePaidDate.setText(transaction.getCtPaidDate());
            }
            if (notEmtpyDate(transaction.getCtProcessDate())) {
                layInvoiceProcessDate.setVisibility(View.VISIBLE);
                tvInvoiceProcessDate.setText(transaction.getCtProcessDate());
            }
            if (notEmtpyDate(transaction.getCtFinishDate())) {
                layInvoiceFinishDate.setVisibility(View.VISIBLE);
                tvInvoiceFinishDate.setText(transaction.getCtFinishDate());
            }
            if (notEmtpyDate(transaction.getCtCancelDate())) {
                layInvoiceCancelDate.setVisibility(View.VISIBLE);
                tvInvoiceCancelDate.setText(transaction.getCtCancelDate());
            }
            if (notEmtpyDate(transaction.getCtRefundDate())) {
                layInvoiceRefundDate.setVisibility(View.VISIBLE);
                tvInvoiceRefundDate.setText(transaction.getCtRefundDate());
            }

            if (transaction.getCtStatus().equals("new") && !transaction.getPaymentMethodGroup().equals("COD")) {
                layActionBottom.setVisibility(View.VISIBLE);
                btnPay.setVisibility(View.VISIBLE);
            } else {
//                btnCancel.setVisibility(View.GONE);
                layActionBottom.setVisibility(View.GONE);
            }
        } else {
            showToast(data.getText());
            mListener.back();
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadData(pullToRefresh, bookingCode);
    }

    @Override public String getTitle() {
        return "BOOKING " + bookingCode;
    }

    @Override public boolean isSearch() {
        return false;
    }

    @Override public boolean isCart() {
        return false;
    }

    @Override public boolean onBackPressed() {
        EventBus.getDefault().post(new EventDashboard(1));
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                mListener.gotoPage(new DashboardFragmentBuilder(1).build(), false, null);
            }
        }, 200);
        return true;
    }


    private boolean notEmtpyDate(String date) {
        if (!TextUtils.isEmpty(date)) {
            if (!date.equals("0000-00-00 00:00:00")) {
                return true;
            }
        }
        return false;
    }

//    @OnClick({R.id.btnCancel, R.id.btnPay}) public void onViewClicked(View view) {
//        switch (view.getId()) {
//            case R.id.btnCancel:
//                break;
//            case R.id.btnPay:
//                break;
//        }
//    }

    @OnClick(R.id.btnPay) public void onViewClicked() {
        String examplePrimary = "#a31414";
        String examplePrimaryDark = "#6d0000";
        String exampleSecondary = "#F57F17";
        CustomColorTheme colorTheme = new CustomColorTheme(examplePrimary, examplePrimaryDark, exampleSecondary);

        SdkUIFlowBuilder.init(getContext(), CLIENT_KEY, BASE_URL, this)
                .setColorTheme(colorTheme)
                .enableLog(true)
                .useBuiltInTokenStorage(false)
                .buildSDK();

        TransactionRequest transactionRequest = new TransactionRequest(transaction.getCtBookingCode(), Double.parseDouble(String.valueOf(transaction.getCtAmountTotal())));
        CreditCard creditCardOptions = new CreditCard();
        creditCardOptions.setSaveCard(false);
        creditCardOptions.setSecure(true);
        creditCardOptions.setBank(BankType.BRI);
        creditCardOptions.setChannel(CreditCard.MIGS);
        transactionRequest.setCreditCard(creditCardOptions);

        MidtransSDK.getInstance().setTransactionRequest(transactionRequest);
//        MidtransSDK.getInstance().startPaymentUiFlow(getActivity());

        switch (transaction.getPaymentMethodGroup()) {
            case PaymentGroup.PAYMENT_GROUP_CC:
                MidtransSDK.getInstance().startPaymentUiFlow(getActivity(), PaymentMethod.CREDIT_CARD);
                break;
            case PaymentGroup.PAYEMNT_GROUP_VA:
                switch (transaction.getPaymentMethodName()) {
                    case com.app.icarwis.models.transaction.PaymentMethod.VA_MANDIRI:
                        MidtransSDK.getInstance().startPaymentUiFlow(getActivity(), PaymentMethod.BANK_TRANSFER_MANDIRI);
                        break;
                    case com.app.icarwis.models.transaction.PaymentMethod.VA_BCA:
                        MidtransSDK.getInstance().startPaymentUiFlow(getActivity(), PaymentMethod.BANK_TRANSFER_BCA);
                        break;
                    case com.app.icarwis.models.transaction.PaymentMethod.VA_BNI:
                        MidtransSDK.getInstance().startPaymentUiFlow(getActivity(), PaymentMethod.BANK_TRANSFER_BNI);
                        break;
                    case com.app.icarwis.models.transaction.PaymentMethod.VA_PERMATA:
                        MidtransSDK.getInstance().startPaymentUiFlow(getActivity(), PaymentMethod.BANK_TRANSFER_PERMATA);
                        break;
                    case com.app.icarwis.models.transaction.PaymentMethod.VA_OTHER:
                        MidtransSDK.getInstance().startPaymentUiFlow(getActivity(), PaymentMethod.BANK_TRANSFER_OTHER);
                        break;
                }
                break;
        }
    }

    @Override public void onTransactionFinished(TransactionResult result) {
        String status = null;
        String desc = "";
        if (result.getResponse() != null) {
            switch (result.getStatus()) {
                case TransactionResult.STATUS_SUCCESS:
                    status = "paid";
                    desc = "Pembayaran berhasil";
                    break;
                case TransactionResult.STATUS_PENDING:
                    status = "pending";
                    desc = "Pembayaran pending";
                    if (transaction.getPaymentMethodGroup().equals(PaymentGroup.PAYEMNT_GROUP_VA)){
                        showToast("Cara pembayaran telah dikirim ke email anda");
                    }
                    break;
                case TransactionResult.STATUS_FAILED:
                    status = "cancel";
                    desc = "Pembayaran failed";
                    break;
            }
            if (status != null){
                presenter.updateTransaction(transaction.getCtBookingCode(), status, desc);
            }

            result.getResponse().getValidationMessages();
        } else if (result.isTransactionCanceled()) {
//            Toast.makeText(getContext(), "Transaction Canceled", Toast.LENGTH_LONG).show();
        } else {
            if (result.getStatus().equalsIgnoreCase(TransactionResult.STATUS_INVALID)) {
                Toast.makeText(getContext(), "Transaction Invalid", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getContext(), "Transaction Finished with failure.", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override public void shoProgressDialog(boolean show) {
        if (show){
            showLoading("Loading...", false);
        }else{
            hideLoading();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventDataNotification(DataNotification dataNotification) {
        String s = dataNotification.getMethod().toLowerCase();
        if (s.equals("booking") && getContext() != null && dataNotification.getDataId().equals(bookingCode)) {
            loadData(false);
        }
    }

    @Override public void onAttach(Context context) {
        super.onAttach(context);
        EventBus.getDefault().register(this);
    }

    @Override public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }
}