package com.app.icarwis.modules.splash.activity;

import com.app.icarwis.models.customer.Customer;
import com.hannesdorfmann.mosby3.mvp.MvpView;

/**
 * Created by Luthfi on 01/08/2017.
 */

public interface XSplashView extends MvpView {
    void showContent();

    void showConfirmDialog(int alertType, String title, String content, String confirmLabel, String cancelLabel, final String action);

    void OnSuccessAuth(Customer customer);
}
