package com.app.icarwis.models.eventbus;

/**
 * Created by luthfiaziz on 27/02/18.
 */

public class EventWisataCart {
    public static final int TYPE_ADD = 1;
    public static final int TYPE_REMOVE = 2;
    private int type;

    /**
     * @param type   Layout orientation. Should be {@link #TYPE_ADD} or {@link
     *                      #TYPE_REMOVE}.
     */

    public EventWisataCart(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
