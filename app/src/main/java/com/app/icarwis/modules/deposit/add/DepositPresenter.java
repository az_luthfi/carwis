package com.app.icarwis.modules.deposit.add;

import android.content.Context;

import com.app.icarwis.R;
import com.app.icarwis.app.App;
import com.app.icarwis.base.BaseRxLcePresenter;
import com.app.icarwis.models.deposit.RequestDeposit;
import com.app.icarwis.utility.Config;
import com.app.icarwis.utility.Logs;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DepositPresenter extends BaseRxLcePresenter<XDepositView, RequestDeposit>
        implements XDepositPresenter {

    public DepositPresenter(Context context) {
        super(context);
    }

    @Override public void getActiveDeposit() {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "active_deposit");
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(Config.CUSTOMER_ID)));

        Logs.d("FragmentPulsaPresenter => requestActiveDeposit ==> " + params);
        subscribe(App.getService().apiDepositRequest(params), false);

    }

    @Override public void requestDeposit(String nominal, String bankSelected) {
        if (isViewAttached()){
            getView().showProgressDialog(true);
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "deposit_add");
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(Config.CUSTOMER_ID)));
        params.put("amount", nominal);
        params.put("bankTo", bankSelected);
        Logs.d("FragmentPulsaPresenter => sendDeposit ==> " + params);

        Observer<RequestDeposit> observer = new Observer<RequestDeposit>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestDeposit requestDeposit) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    if (requestDeposit.getStatus()) {
                        getView().showDepositConfirmation(requestDeposit.getDeposit());
                    } else {
                        getView().initBank(requestDeposit.getBanks());
                    }
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()){
                    getView().showProgressDialog(false);
                    getView().onError(context.getString(R.string.error_network_light));
                }
            }

            @Override public void onComplete() {
            }
        };

        App.getService().apiDepositRequest(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);

    }
}
