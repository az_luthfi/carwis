package com.app.icarwis.modules.order.paketwisata.list;

import com.app.icarwis.models.wisata.RequestWisata;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XPaketWisataView extends MvpLceView<RequestWisata> {
    void showLoadingNextPage(boolean show);

    void setNextData(RequestWisata data);

    void showErrorNextPage(String string);

}