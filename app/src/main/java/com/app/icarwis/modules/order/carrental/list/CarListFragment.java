package com.app.icarwis.modules.order.carrental.list;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.icarwis.R;
import com.app.icarwis.adapter.car.CarAdapter;
import com.app.icarwis.base.BaseLceRefreshFragment;
import com.app.icarwis.holder.ItemLoadingHolder;
import com.app.icarwis.models.car.RequestCar;
import com.app.icarwis.modules.order.carrental.carparent.CarParentFragment;
import com.app.icarwis.utility.listview.DividerItemDecoration;
import com.app.icarwis.utility.listview.EndlessRecyclerOnScrollListener;
import com.app.icarwis.utility.listview.ItemClickSupport;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import java.util.HashMap;

import butterknife.BindView;

import static com.app.icarwis.utility.Config.PAGINATION_LIMIT;

@FragmentWithArgs
public class CarListFragment extends BaseLceRefreshFragment<SwipeRefreshLayout, RequestCar, XCarListView, CarListPresenter>
        implements XCarListView, ItemLoadingHolder.ItemViewLoadingListener {

    @Arg String hour;
    @Arg HashMap<String, String> params;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.emptyView) TextView emptyView;
    @BindView(R.id.contentView) SwipeRefreshLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.container) FrameLayout container;

    private CarAdapter adapter;
    private EndlessRecyclerOnScrollListener scrollListener;


    public CarListFragment() {

    }

    @Override public CarListPresenter createPresenter() {
        return new CarListPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_with_recyclerview_refresh;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new CarAdapter(this);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.LIST_VERTICAL));
        recyclerView.setAdapter(adapter);

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                if (((CarParentFragment) getParentFragment()) != null) {
                    ((CarParentFragment) getParentFragment()).onCarSelected(adapter.getItem(position));
                }
            }
        });

        scrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override public void onLoadMore(int page, int totalItemsCount) {
                if (!adapter.isLoading()) {
                    presenter.loadData(false, hour, page, params);
                }
            }
        };

        loadData(false);
    }

    @Override public void setData(RequestCar data) {
        if (adapter.getItemCount() > 0) {
            adapter.clear();
        }
        if (data.getStatus()) {
            recyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            adapter.setItems(data.getCars());
            if (data.getCars().size() < PAGINATION_LIMIT) {
                recyclerView.removeOnScrollListener(scrollListener);
            }
        } else {
            if (adapter.getItemCount() == 0) {
                emptyView.setVisibility(View.VISIBLE);
                emptyView.setText(data.getText());
                recyclerView.setVisibility(View.GONE);
            } else {
                recyclerView.removeOnScrollListener(scrollListener);
            }
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        scrollListener.resetState();
        recyclerView.addOnScrollListener(scrollListener);
        presenter.loadData(pullToRefresh, hour, 1, params);
    }

    @Override public void onRetryNextPage() {
        adapter.setErrorNextPage(null);
        presenter.loadData(false, hour, scrollListener.getCurrentPage(), params);
    }

    @Override public void showLoadingNextPage(boolean show) {
        adapter.setLoadingNextPage(show);
    }

    @Override public void setNextData(RequestCar data) {
        if (data.getStatus()) {
            adapter.setItems(data.getCars());
            if (data.getCars().size() < PAGINATION_LIMIT) {
                recyclerView.removeOnScrollListener(scrollListener);
            }
        } else {
            recyclerView.removeOnScrollListener(scrollListener);
        }
    }

    @Override public void showErrorNextPage(String string) {
        showToast(string);
        adapter.setErrorNextPage("Tap to retry");
        scrollListener.backToPreviousPage();
    }
}