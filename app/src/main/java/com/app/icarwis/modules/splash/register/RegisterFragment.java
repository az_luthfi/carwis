package com.app.icarwis.modules.splash.register;

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.app.icarwis.R;
import com.app.icarwis.adapter.SimpleAdapter;
import com.app.icarwis.models.berita.RequestContent;
import com.app.icarwis.models.customer.Customer;
import com.app.icarwis.models.datalist.SimpleList;
import com.app.icarwis.modules.splash.BaseSplashFragment;
import com.app.icarwis.utility.CommonUtilities;
import com.app.icarwis.utility.DialogListOption;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;

import static com.app.icarwis.utility.Config.MIN_LENGHT_PASSWORD;


public class RegisterFragment extends BaseSplashFragment<XRegisterView, RegisterPresenter>
        implements XRegisterView {

    @BindView(R.id.etName) MaterialEditText etName;
    @BindView(R.id.etEmail) MaterialEditText etEmail;
    @BindView(R.id.etPassword) MaterialEditText etPassword;
    @BindView(R.id.etPhone) MaterialEditText etPhone;
    @BindView(R.id.etAddress) MaterialEditText etAddress;
    @BindView(R.id.etProvince) MaterialEditText etProvince;
    @BindView(R.id.cbTerm) CheckBox cbTerm;
    @BindView(R.id.btnRegister) Button btnRegister;
    @BindView(R.id.container) NestedScrollView container;
    @BindView(R.id.tvTermAndCon) TextView tvTermAndCon;
    @BindView(R.id.etBank) MaterialEditText etBank;
    @BindView(R.id.radioWni) RadioButton radioWni;
    @BindView(R.id.radioWna) RadioButton radioWna;
    @BindView(R.id.radioGender) RadioGroup radioGender;

    private String[] email;
    private SimpleAdapter adapter;

    public RegisterFragment() {

    }

    @Override public RegisterPresenter createPresenter() {
        return new RegisterPresenter(getContext(), getActivity());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_register;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new SimpleAdapter();

        SpannableString span = new SpannableString(tvTermAndCon.getText().toString().trim());
        span.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getActivity(), R.color.colorAccent)), 34, 54, 0);
        span.setSpan(new RelativeSizeSpan(1.2f), 34, 54, 0);
        span.setSpan(new StyleSpan(Typeface.BOLD_ITALIC), 34, 54, 0);
        span.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                presenter.loadTermAndCon();
            }
        }, 34, 54, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvTermAndCon.setText(span);
        tvTermAndCon.setMovementMethod(LinkMovementMethod.getInstance());
        radioWni.setChecked(true);
    }

    @OnClick({R.id.etEmail, R.id.etProvince, R.id.btnRegister, R.id.etBank}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.etEmail:
                presenter.requestPermissionsAccounts();
                break;
            case R.id.etProvince:
                presenter.loadProvince();
                break;
            case R.id.btnRegister:
                validateForm();
                break;
            case R.id.etBank:
//                presenter.loadBank();
                break;
        }
    }

    private void validateForm() {
        if (isEmpty(etName, "Nama wajib diisi")) return;
        if (isEmpty(etEmail, "Email wajib diisi")) return;
        if (isEmpty(etPassword, "Password wajib diisi")) return;
        if (etPassword.length() < MIN_LENGHT_PASSWORD) {
            setError(etPassword, "Password minimal " + MIN_LENGHT_PASSWORD);
            return;
        }
        if (isEmpty(etPhone, "No. Telp / HP wajib diisi")) return;
        if (etPhone.length() < 10) {
            setError(etPhone, "No. Telp / HP tidak sesuai");
            return;
        }
        if (isEmpty(etAddress, "Alamat wajib diisi")) return;
        if (isEmpty(etProvince, "Provinsi wajib diisi")) return;
        if (!cbTerm.isChecked()) {
            showToast("Anda belum menyutujui syarat dan ketentuan");
            return;
        }
        String kewarganegaraan = null;
        if (radioWni.isChecked()) {
            kewarganegaraan = "wni";
        }
        if (radioWna.isChecked()) {
            kewarganegaraan = "wna";
        }
        if (TextUtils.isEmpty(kewarganegaraan)) {
            showToast("Anda belum memilih kewarganegaraan");
            return;
        }

        HashMap<String, String> params = new HashMap<>();
        params.put("name", etName.getText().toString());
        params.put("email", etEmail.getText().toString());
        params.put("pass", CommonUtilities.md5(CommonUtilities.md5(etPassword.getText().toString())));
        params.put("msisdn", etPhone.getText().toString());
        params.put("address", etAddress.getText().toString());
        params.put("province", etProvince.getText().toString());
        params.put("kewarganegaraan", kewarganegaraan);

        presenter.requestRegister(params);
    }

    private boolean isEmpty(EditText editText, String errorMsg) {
        if (TextUtils.isEmpty(editText.getText())) {
            setError(editText, errorMsg);
            return true;
        }
        return false;
    }

    private boolean isEmpty(TextView textView, String errorMsg) {
        if (TextUtils.isEmpty(textView.getText())) {
            showToast(errorMsg);
            return true;
        }
        return false;
    }

    private void setError(EditText editText, String errorMsg) {
        editText.setError(errorMsg);
        editText.requestFocus();
    }

    @Override public void showAccountsDialog() {
        final ArrayList<String> emailList = CommonUtilities.getEmailOnPhone(getActivity());
        email = new String[emailList.size()];
        for (int a = 0; a < emailList.size(); a++) {
            email[a] = emailList.get(a);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Pilih Email");
        builder.setItems(email, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                etEmail.setText(email[item]);
            }
        });
        builder.show();
    }

    @Override public void showDialog(ArrayList<? extends SimpleList> data, final String title) {
        if (adapter.getItemCount() > 0) {
            adapter.clear();
        }
        adapter.addAll(data);
        new DialogListOption(getContext(), title, adapter) {
            @Override public void onItemClicked(int position) {
                if (position != -1) {
                    switch (title.toLowerCase()) {
                        case "provinsi":
                            etProvince.setText(adapter.getItem(position).getName());
                            break;
                        case "bank":
                            etBank.setText(adapter.getItem(position).getName());
                            break;
                    }

                }
            }

            @Override public void onFilter(CharSequence s) {
                adapter.filter(s);
            }
        };
    }

    @Override public void showProgressDialog(boolean show) {
        if (show) {
            showLoading("Loading...", false);
        } else {
            hideLoading();
        }
    }

    @Override public void showError(String text) {
        final Snackbar snackbar = Snackbar.make(container, text, Snackbar.LENGTH_LONG);
        snackbar.setAction("Tutup", new View.OnClickListener() {
            @Override public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }

    @Override public void onSuccessRegister(Customer customer) {
        splashListener.OnSuccessAuth(customer);
    }

    @Override public void shoDialogTermAndCon(RequestContent data) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.alertsyarat, null);
        final TextView ket = (TextView) alertLayout.findViewById(R.id.tvKet);
        Button btnok = (Button) alertLayout.findViewById(R.id.btOk);
        final AlertDialog alert = new AlertDialog.Builder(getActivity()).create();
        alert.setView(alertLayout);
        alert.setCancelable(false);
        ket.setText(CommonUtilities.toHtml(data.getContent().getContentDesc()));
        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        alert.setView(alertLayout);
        alert.show();
    }
}