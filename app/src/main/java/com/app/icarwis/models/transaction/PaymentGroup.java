package com.app.icarwis.models.transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Luthfi Aziz on 08/12/2017.
 */

public class PaymentGroup {
    public static final String PAYMENT_GROUP_CC = "Kartu Debit/Kredit";
    public static final String PAYEMNT_GROUP_VA = "Virtual Account";
    public static final String PAYMENT_GROUP_TRANSFER = "Transfer";

    @SerializedName("payment_method_id")
    @Expose
    private String paymentMethodId;
    @SerializedName("payment_method_group")
    @Expose
    private String paymentMethodGroup;
    @SerializedName("payment_method_name")
    @Expose
    private String paymentMethodName;
    @SerializedName("payment_method_3rdparty")
    @Expose
    private String paymentMethod3rdparty;
    @SerializedName("payment_method_admin_price")
    @Expose
    private String paymentMethodAdminPrice;
    @SerializedName("payment_method_desc")
    @Expose
    private String paymentMethodDesc;
    @SerializedName("payment_method_status")
    @Expose
    private String paymentMethodStatus;
    @SerializedName("payment_method_create_date")
    @Expose
    private String paymentMethodCreateDate;
    @SerializedName("payment_method_alias")
    @Expose
    private String paymentMethodAlias;
    @SerializedName("payment_method_code")
    @Expose
    private String paymentMethodCode;
    @SerializedName("payment_methods")
    @Expose
    private ArrayList<PaymentMethod> paymentMethods = new ArrayList<>();
    private boolean selected;
    private int positionPayment = -1;

    public String getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(String paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentMethodGroup() {
        return paymentMethodGroup;
    }

    public void setPaymentMethodGroup(String paymentMethodGroup) {
        this.paymentMethodGroup = paymentMethodGroup;
    }

    public String getPaymentMethodName() {
        return paymentMethodName;
    }

    public void setPaymentMethodName(String paymentMethodName) {
        this.paymentMethodName = paymentMethodName;
    }

    public String getPaymentMethod3rdparty() {
        return paymentMethod3rdparty;
    }

    public void setPaymentMethod3rdparty(String paymentMethod3rdparty) {
        this.paymentMethod3rdparty = paymentMethod3rdparty;
    }

    public String getPaymentMethodAdminPrice() {
        return paymentMethodAdminPrice;
    }

    public void setPaymentMethodAdminPrice(String paymentMethodAdminPrice) {
        this.paymentMethodAdminPrice = paymentMethodAdminPrice;
    }

    public String getPaymentMethodDesc() {
        return paymentMethodDesc;
    }

    public void setPaymentMethodDesc(String paymentMethodDesc) {
        this.paymentMethodDesc = paymentMethodDesc;
    }

    public String getPaymentMethodStatus() {
        return paymentMethodStatus;
    }

    public void setPaymentMethodStatus(String paymentMethodStatus) {
        this.paymentMethodStatus = paymentMethodStatus;
    }

    public String getPaymentMethodCreateDate() {
        return paymentMethodCreateDate;
    }

    public void setPaymentMethodCreateDate(String paymentMethodCreateDate) {
        this.paymentMethodCreateDate = paymentMethodCreateDate;
    }

    public String getPaymentMethodAlias() {
        return paymentMethodAlias;
    }

    public void setPaymentMethodAlias(String paymentMethodAlias) {
        this.paymentMethodAlias = paymentMethodAlias;
    }

    public String getPaymentMethodCode() {
        return paymentMethodCode;
    }

    public void setPaymentMethodCode(String paymentMethodCode) {
        this.paymentMethodCode = paymentMethodCode;
    }

    public ArrayList<PaymentMethod> getPaymentMethods() {
        return paymentMethods;
    }

    public void setPaymentMethods(ArrayList<PaymentMethod> paymentMethods) {
        this.paymentMethods = paymentMethods;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int getPositionPayment() {
        return positionPayment;
    }

    public void setPositionPayment(int positionPayment) {
        this.positionPayment = positionPayment;
    }
}
