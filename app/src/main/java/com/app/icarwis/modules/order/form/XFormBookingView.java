package com.app.icarwis.modules.order.form;

import com.app.icarwis.models.eventbus.EventCoordinateBooking;
import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface XFormBookingView extends MvpView {

    void updateCoordinate(EventCoordinateBooking event);
}