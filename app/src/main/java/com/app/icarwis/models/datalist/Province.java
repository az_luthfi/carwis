package com.app.icarwis.models.datalist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Luthfi on 02/08/2017.
 */

public class Province implements SimpleList{
    @SerializedName("province_id")
    @Expose
    private String provinceId;
    @SerializedName("province_name")
    @Expose
    private String provinceName;
    @SerializedName("cities")
    @Expose
    private ArrayList<City> cities = new ArrayList<>();

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    @Override public String getName() {
        return provinceName;
    }

    @Override public String getId() {
        return provinceId;
    }

    public ArrayList<City> getCities() {
        return cities;
    }
}
