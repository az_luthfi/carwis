package ${packageName};

import ${applicationPackage}.R;

import ${applicationPackage}.base.BaseMvpFragment;

public class ${className}Fragment extends BaseMvpFragment<X${className}View, ${className}Presenter>
        implements X${className}View{

    public ${className}Fragment()
    {

    }

    @Override public ${className}Presenter createPresenter() {
            return new ${className}Presenter(getContext());
    }

    @Override protected int getLayoutRes() {
    <#if includeLayout>
        return R.layout.fragment_${classToResource(className)};
    <#else>
        return 0;
    </#if>
    }

}