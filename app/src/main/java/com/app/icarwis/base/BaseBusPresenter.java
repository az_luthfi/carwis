package com.app.icarwis.base;

import android.content.Context;

import com.app.icarwis.utility.Preferences;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by j3p0n on 3/22/2017.
 */

public class BaseBusPresenter<V extends MvpView> extends MvpBasePresenter<V> {
    protected Context context;
    protected Preferences prefs;

    public BaseBusPresenter(Context context) {
        if (prefs == null) {
            prefs = new Preferences(context);
        }
        this.context = context;
    }

    public Preferences getPrefs() {
        return prefs;
    }

    @Override public void detachView(boolean retainInstance) {
        if (EventBus.getDefault().isRegistered(getView())) {
            EventBus.getDefault().unregister(getView());
        }
        super.detachView(retainInstance);
        if (!retainInstance) {
        }
    }

    @Override public void attachView(V view) {
        super.attachView(view);
        if (!EventBus.getDefault().isRegistered(getView())) {
            EventBus.getDefault().register(getView());
        }
    }

}
