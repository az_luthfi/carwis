package com.app.icarwis.holder.wisata;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.icarwis.R;
import com.app.icarwis.adapter.wisata.WisataAdapter;
import com.app.icarwis.models.eventbus.EventWisataCart;
import com.app.icarwis.models.wisata.Wisata;
import com.app.icarwis.utility.CommonUtilities;
import com.app.icarwis.utility.Preferences;
import com.app.icarwis.utility.Str;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ItemWisataHolder extends RecyclerView.ViewHolder {
    private static final String TAG = ItemWisataHolder.class.getSimpleName();
    @BindView(R.id.ivImage) ImageView ivImage;
    @BindView(R.id.tvName) TextView tvName;
    @BindView(R.id.tvAddress) TextView tvAddress;
    @BindView(R.id.cbCheck) CheckBox cbCheck;
    @BindView(R.id.tvAmount) TextView tvAmount;
    @BindView(R.id.layName) LinearLayout layName;

    private CompoundButton.OnCheckedChangeListener onCheckedChangeListener;
    private int type;

    public ItemWisataHolder(LayoutInflater inflater, ViewGroup parent, int type) {
        this(inflater.inflate(R.layout.item_wisata, parent, false));
        this.type = type;
    }

    private Wisata wisata;
    private WisataAdapter.Listener listener;
    private Preferences prefs;
    private boolean isChecked = false;

    public ItemWisataHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
        prefs = new Preferences(itemView.getContext());
    }

    public void bindView(final Wisata model, final WisataAdapter.Listener listener) {
        this.listener = listener;
        this.wisata = model;
        if (!TextUtils.isEmpty(model.getMediaValue())) {
            Picasso.with(itemView.getContext()).load(model.getMediaValue()).placeholder(R.color.md_grey_500).resize(500, 0).into(ivImage);
        } else {
            ivImage.setImageResource(R.color.md_grey_500);
        }

        tvName.setText(model.getCwName());
        tvAddress.setText(Str.getText(model.getCwAddress(), ""));
        if (type == WisataAdapter.TYPE_CHOOSE) {
            if (prefs.wisataCartPreferencesExist(model.getCwId())){
                isChecked = true;
            }else{
                isChecked = false;
            }
            tvAmount.setText(CommonUtilities.toRupiahNumberFormat(model.getAmount() + model.getCwService()));
            cbCheck.setChecked(isChecked);
//            cbCheck.setClickable(true);
//            cbCheck.setLongClickable(true);
            cbCheck.setVisibility(View.VISIBLE);
            tvAmount.setVisibility(View.VISIBLE);
        } else {
            cbCheck.setVisibility(View.GONE);
            tvAmount.setVisibility(View.GONE);
        }

    }

    @OnClick({R.id.layName, R.id.cbCheck}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layName:
                listener.onDetail(getAdapterPosition());
                break;
            case R.id.cbCheck:
                if (isChecked){
                    prefs.removeWisataCartPreference(wisata.getCwId());
                    isChecked = false;
                }else{
                    prefs.addWisataCartPreferences(wisata.getCwId());
                    isChecked = true;
                }
                cbCheck.setChecked(isChecked);
                EventBus.getDefault().post(new EventWisataCart(EventWisataCart.TYPE_ADD));
//                if (listener != null) {
//                    cbCheck.setChecked(!isChecked);
//
////                    cbCheck.setClickable(false);
////                    cbCheck.setLongClickable(false);
////                    if (listener.onCheck(getAdapterPosition(), wisata, cbCheck.isChecked())) {
////                        cbCheck.setChecked(!wisata.isChecked());
////                    } else {
////                        cbCheck.setClickable(true);
////                        cbCheck.setLongClickable(true);
////                        cbCheck.setChecked(wisata.isChecked());
////                    }
//                }
                break;
        }
    }
}
