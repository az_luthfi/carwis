package ${packageName};

import android.content.Context;

import ${applicationPackage}.base.BaseRxLcePresenter;

public class ${className}Presenter extends BaseRxLcePresenter<X${className}View, ${modelName}>
        implements X${className}Presenter{

    public ${className}Presenter(Context context)
    {
        super(context);
    }

}