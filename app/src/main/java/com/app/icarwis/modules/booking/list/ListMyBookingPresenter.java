package com.app.icarwis.modules.booking.list;

import android.content.Context;

import com.app.icarwis.R;
import com.app.icarwis.app.App;
import com.app.icarwis.base.BaseRxLcePresenter;
import com.app.icarwis.models.transaction.RequestTransaction;
import com.app.icarwis.utility.Config;
import com.app.icarwis.utility.Logs;

import java.util.HashMap;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ListMyBookingPresenter extends BaseRxLcePresenter<XListMyBookingView, RequestTransaction>
        implements XListMyBookingPresenter {
    private static final String TAG = ListMyBookingPresenter.class.getSimpleName();
    public ListMyBookingPresenter(Context context) {
        super(context);
    }

    @Override public void loadData(boolean pullToRefresh, int page, String status) {
        HashMap<String, String> params = new HashMap<>();
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(Config.CUSTOMER_ID)));
        params.put("page", String.valueOf(page));
        params.put("act", "history");
        params.put("status", status);
        Logs.d(TAG, "loadData ==> status=> " + status);

        Observable<RequestTransaction> observable = App.getService().apiTransaction(params);
        if (page == 1) {
            subscribe(observable, pullToRefresh);
        } else {
            if (isViewAttached()) {
                getView().showLoadingNextPage(true);
            }
            Observer<RequestTransaction> observer1 = new Observer<RequestTransaction>() {
                @Override public void onSubscribe(Disposable d) {

                }

                @Override public void onNext(RequestTransaction data) {
                    if (isViewAttached()) {
                        getView().showLoadingNextPage(false);
                        getView().setNextData(data);
                    }
                }

                @Override public void onError(Throwable e) {
                    if (isViewAttached()) {
                        getView().showErrorNextPage(context.getString(R.string.error_network));
                    }
                }

                @Override public void onComplete() {

                }
            };

            observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(observer1);
        }
    }


}