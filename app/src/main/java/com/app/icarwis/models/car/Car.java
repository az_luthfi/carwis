package com.app.icarwis.models.car;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Luthfi Aziz on 19/11/2017.
 */

public class Car {
    @SerializedName("ccr_id")
    @Expose
    private String ccrId;
    @SerializedName("ccr_name")
    @Expose
    private String ccrName;
    @SerializedName("ccr_nopol")
    @Expose
    private String ccrNopol;
    @SerializedName("ccr_capacity")
    @Expose
    private String ccrCapacity;
    @SerializedName("ccr_type")
    @Expose
    private String ccrType;
    @SerializedName("ccr_extra")
    @Expose
    private Double ccrExtra;
    @SerializedName("ccr_is_driver")
    @Expose
    private String ccrIsDriver;
    @SerializedName("ccr_is_gasoline")
    @Expose
    private String ccrIsGasoline;
    @SerializedName("ccp_id")
    @Expose
    private String ccpId;
    @SerializedName("ccp_hour")
    @Expose
    private String ccpHour;
    @SerializedName("ccp_amount")
    @Expose
    private Double ccpAmount;
    @SerializedName("ccp_amount_disc")
    @Expose
    private Double ccpAmountDisc;
    @SerializedName("media_value")
    @Expose
    private String mediaValue;

    public String getCcrId() {
        return ccrId;
    }

    public void setCcrId(String ccrId) {
        this.ccrId = ccrId;
    }

    public String getCcrName() {
        return ccrName;
    }

    public void setCcrName(String ccrName) {
        this.ccrName = ccrName;
    }

    public String getCcrNopol() {
        return ccrNopol;
    }

    public void setCcrNopol(String ccrNopol) {
        this.ccrNopol = ccrNopol;
    }

    public String getCcrCapacity() {
        return ccrCapacity;
    }

    public void setCcrCapacity(String ccrCapacity) {
        this.ccrCapacity = ccrCapacity;
    }

    public String getCcrType() {
        return ccrType;
    }

    public void setCcrType(String ccrType) {
        this.ccrType = ccrType;
    }

    public Double getCcrExtra() {
        return ccrExtra;
    }

    public void setCcrExtra(Double ccrExtra) {
        this.ccrExtra = ccrExtra;
    }

    public String getCcrIsDriver() {
        return ccrIsDriver;
    }

    public void setCcrIsDriver(String ccrIsDriver) {
        this.ccrIsDriver = ccrIsDriver;
    }

    public String getCcrIsGasoline() {
        return ccrIsGasoline;
    }

    public void setCcrIsGasoline(String ccrIsGasoline) {
        this.ccrIsGasoline = ccrIsGasoline;
    }

    public String getCcpId() {
        return ccpId;
    }

    public void setCcpId(String ccpId) {
        this.ccpId = ccpId;
    }

    public String getCcpHour() {
        return ccpHour;
    }

    public void setCcpHour(String ccpHour) {
        this.ccpHour = ccpHour;
    }

    public Double getCcpAmount() {
        return ccpAmount;
    }

    public void setCcpAmount(Double ccpAmount) {
        this.ccpAmount = ccpAmount;
    }

    public Double getCcpAmountDisc() {
        return ccpAmountDisc;
    }

    public void setCcpAmountDisc(Double ccpAmountDisc) {
        this.ccpAmountDisc = ccpAmountDisc;
    }

    public String getMediaValue() {
        return mediaValue;
    }

    public void setMediaValue(String mediaValue) {
        this.mediaValue = mediaValue;
    }
}
