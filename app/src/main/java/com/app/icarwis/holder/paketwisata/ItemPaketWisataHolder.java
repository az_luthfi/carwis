package com.app.icarwis.holder.paketwisata;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.icarwis.R;
import com.app.icarwis.models.wisata.PaketWisata;
import com.app.icarwis.utility.CommonUtilities;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemPaketWisataHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.ivImage) ImageView ivImage;
    @BindView(R.id.tvName) TextView tvName;
    @BindView(R.id.tvAmount) TextView tvAmount;

    public ItemPaketWisataHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_paket_wisata, parent, false));
    }

    public ItemPaketWisataHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void bindView(PaketWisata model) {
        if (!TextUtils.isEmpty(model.getMediaValue())) {
            Picasso.with(itemView.getContext()).load(model.getMediaValue()).placeholder(R.color.md_grey_500).resize(500, 0).into(ivImage);
        } else {
            ivImage.setImageResource(R.color.md_grey_500);
        }
        tvName.setText(model.getCpwName());
        tvAmount.setText(CommonUtilities.toRupiahNumberFormat(model.getCpwAmount()));
    }
}
