package com.app.icarwis.listeners;

import io.reactivex.annotations.Nullable;

/**
 * Created by Luthfi Aziz on 10/11/2017.
 */

public interface OnChangeToolbar {
    @Nullable String getTitle();

    boolean isSearch();

    boolean isCart();
}
