package com.app.icarwis.adapter.wisata;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.app.icarwis.base.BaseRecyclerViewLoadingAdapter;
import com.app.icarwis.holder.ItemLoadingHolder;
import com.app.icarwis.holder.paketwisata.ItemPaketWisataHolder;
import com.app.icarwis.models.wisata.PaketWisata;

/**
 * Created by Luthfi Aziz on 22/11/2017.
 */

public class PaketWisataAdapter extends BaseRecyclerViewLoadingAdapter<com.app.icarwis.models.wisata.PaketWisata> {

    public PaketWisataAdapter(ItemLoadingHolder.ItemViewLoadingListener loadingListener) {
        super(loadingListener);
    }

    @Override protected int getItemViewTypeChild(int position) {
        return 1;
    }

    @Override protected RecyclerView.ViewHolder onCreateViewHolderChild(ViewGroup parent, int viewType, LayoutInflater inflater) {
        return new ItemPaketWisataHolder(inflater, parent);
    }

    @Override protected void onBindViewHolderChild(RecyclerView.ViewHolder holder, int position, PaketWisata model) {
        if (holder instanceof ItemPaketWisataHolder){
            ((ItemPaketWisataHolder) holder).bindView(model);
        }
    }
}
