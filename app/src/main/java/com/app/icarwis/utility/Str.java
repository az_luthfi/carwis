package com.app.icarwis.utility;

import android.text.TextUtils;

/**
 * Created by Luthfi on 30/08/2017.
 */

public class Str {
    public static String getText(String text, String textDefault){
        if (TextUtils.isEmpty(text)){
            if (TextUtils.isEmpty(textDefault)){
                return "";
            }
            return textDefault;
        }
        return text;
    }

    public static String getText(Integer text, String textDefault){
        if (text == null){
            if (TextUtils.isEmpty(textDefault)){
                return "";
            }
            return textDefault;
        }
        return String.valueOf(text);
    }

    public static String getAlias(String text){
        String[] myName = text.split(" ");
        String alias = "";
        int count = myName.length;
        if (count > 2){
            count = 2;
        }
        for (int i = 0; i < count; i++) {
            alias = alias + myName[i].charAt(0);
        }
        return alias.toUpperCase();
    }

    public static String toTheCapitalize(String givenString) {
        if (TextUtils.isEmpty(givenString)){
            return "";
        }
        String[] arr = givenString.split(" ");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < arr.length; i++) {
            sb.append(Character.toUpperCase(arr[i].charAt(0)))
                    .append(arr[i].substring(1)).append(" ");
        }
        return sb.toString().trim();
    }

}
