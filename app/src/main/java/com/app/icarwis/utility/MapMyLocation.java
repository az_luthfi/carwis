package com.app.icarwis.utility;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;

import com.app.icarwis.R;
import com.google.android.gms.location.LocationRequest;
import com.patloew.rxlocation.RxLocation;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.util.List;
import java.util.concurrent.TimeUnit;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by luthfi on 09/05/2017.
 */

public class MapMyLocation {
    private static final String TAG = MapMyLocation.class.getSimpleName();
    private int ERROR_PERMISSON = 1;
    private int ERROR_GPS_DISABLE = 2;
    private int ERROR_RX_LOCATION = 3;
    private final Activity activity;
    private LocationInterface listener;
    private RxPermissions rxPermissions;
    private RxLocation rxLocation;
    private LocationRequest locationRequest;
    private CompositeDisposable disposable;
    private boolean actionLister = false;
    private Disposable disposableItem;
    private OnAddressUpdateListener onAddressUpdateListener;
    private boolean isRequiredGpsActive = true;


    public MapMyLocation(Activity activity, int maxUpdate) {
        this.activity = activity;
        this.rxPermissions = new RxPermissions(activity);
        this.rxLocation = new RxLocation(activity);
        init(maxUpdate);
    }

    public MapMyLocation(Activity activity, int maxUpdate, boolean isRequiredGpsActive) {
        this.isRequiredGpsActive = isRequiredGpsActive;
        this.activity = activity;
        this.rxPermissions = new RxPermissions(activity);
        this.rxLocation = new RxLocation(activity);
        init(maxUpdate);
    }

//    public MapMyLocation(Activity activity, RxPermissions rxPermissions, int maxUpdate) {
//        this.activity = activity;
//        this.rxPermissions = rxPermissions;
//        this.rxLocation = new RxLocation(activity);
//        init(maxUpdate);
//    }
//
//    public MapMyLocation(Activity activity, RxPermissions rxPermissions, RxLocation rxLocation, int maxUpdate) {
//        this.activity = activity;
//        this.rxPermissions = rxPermissions;
//        this.rxLocation = rxLocation;
//        init(maxUpdate);
//    }

    private void init(int maxUpdate) {
        disposable = new CompositeDisposable();
        rxLocation.setDefaultTimeout(15, TimeUnit.SECONDS);

        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(5000);
        if (maxUpdate > 0) {
            locationRequest.setNumUpdates(maxUpdate);
        }

        RxJavaPlugins.setErrorHandler(new Consumer<Throwable>() {
            @Override public void accept(@NonNull Throwable throwable) throws Exception {
                Logs.e("MapMyLocation => RxJavaPlugins.setErrorHandler => " + throwable.getMessage());
            }
        });
    }

    public MapMyLocation setDisposableItem(Disposable disposableItem) {
        this.disposableItem = disposableItem;
        return this;
    }

    public MapMyLocation setOnAddressUpdateListener(OnAddressUpdateListener onAddressUpdateListener) {
        this.onAddressUpdateListener = onAddressUpdateListener;
        return this;
    }

    public MapMyLocation setListener(LocationInterface listener) {
        this.listener = listener;
        return this;
    }

    public MapMyLocation getMyLocation() {
        rxPermissions
                .request(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                .subscribe(new Observer<Boolean>() {
                    @Override public void onSubscribe(Disposable d) {
                    }

                    @Override public void onNext(Boolean aBoolean) {
                        if (aBoolean) {
                            releaseGps();
                        } else {
                            if (listener != null) {
                                listener.OnLocationError("Izin lokasi ditolak, tidak dapat mengaktifkan lokasi", ERROR_PERMISSON);
                            }
                        }
                    }

                    @Override public void onError(Throwable e) {
                    }

                    @Override public void onComplete() {
                    }
                });
        return this;
    }

    private void releaseGps() {
        actionLister = false;
        final LocationManager manager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER) || !manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            if (isRequiredGpsActive) {
                SweetAlertDialog alert = CommonUtilities.buildAlert(activity, SweetAlertDialog.WARNING_TYPE, "Informasi", activity.getString(R.string.text_location_off), "Pengaturan", "Tutup");
                alert.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        actionLister = true;
                        sweetAlertDialog.cancel();
                        if (listener != null) {
                            listener.OnLocationError("Pengaturan lokasi dinonaktifkan, tidak dapat menggunakan lokasi", ERROR_GPS_DISABLE);
                        }
                    }
                });
                alert.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        actionLister = true;
                        sweetAlertDialog.cancel();
                        if (listener != null) {
                            listener.gotoLocationSettings();
                        }
                    }
                });
                alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override public void onDismiss(DialogInterface dialog) {
                        if (listener != null && !actionLister) {
                            listener.OnLocationError("Pengaturan lokasi dinonaktifkan, tidak dapat menggunakan lokasi", ERROR_GPS_DISABLE);
                        }
                    }
                });
            }else{
                if (listener != null) {
                    listener.OnLocationError("Pengaturan lokasi dinonaktifkan, tidak dapat menggunakan lokasi", ERROR_GPS_DISABLE);
                }
            }
        } else {
            startLocationRefresh();
        }
    }

    private void startLocationRefresh() {
        if (disposableItem != null) {
            disposable.add(disposableItem);
        }
    }

    public Disposable getLocationWithoutAddress() {
        return rxLocation.settings().checkAndHandleResolution(locationRequest)
                .flatMapObservable(new Function<Boolean, ObservableSource<Location>>() {
                    @Override public ObservableSource<Location> apply(@NonNull Boolean aBoolean) throws Exception {
                        return getLocationObservable(aBoolean);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }

    public Disposable getLocationWithAddress() {
        return rxLocation.settings().checkAndHandleResolution(locationRequest)
                .flatMapObservable(new Function<Boolean, ObservableSource<Address>>() {
                    @Override public ObservableSource<Address> apply(@NonNull Boolean aBoolean) throws Exception {
                        return getAddressObservable(aBoolean);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<Address>() {
                    @Override public void accept(@NonNull Address address) throws Exception {
                        if (onAddressUpdateListener != null) {
                            onAddressUpdateListener.onAddressUpdate(address);
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override public void accept(@NonNull Throwable throwable) throws Exception {
                        Logs.e("MapMyLocation => getLocationWithAddress => " + throwable.getMessage());
                    }
                });

//        , new Consumer<Throwable>() {
//            @Override public void accept(@NonNull Throwable throwable) throws Exception {
//                if (onAddressUpdateListener != null){
//                    onAddressUpdateListener.onAddressError(throwable);
//                }
//            }
//        }
    }

    private Observable<Location> getLocationObservable(final boolean success) {
        if (success) {
            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                getMyLocation();
                return null;
            }
            return rxLocation.location().updates(locationRequest)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext(new Consumer<Location>() {
                        @Override public void accept(@NonNull Location location) throws Exception {
                            if (listener != null) {
                                listener.OnLocationUpdates(location);
                            }
                        }
                    });

//            .doOnError(new Consumer<Throwable>() {
//                @Override public void accept(@NonNull Throwable throwable) throws Exception {
//                    if (listener != null) {
//                        listener.OnLocationError("Gagal mengambil lokasi terbaru", ERROR_RX_LOCATION);
//                    }
//                }
//            })
        }

        return rxLocation.location().lastLocation()
                .doOnSuccess(new Consumer<Location>() {
                    @Override public void accept(@NonNull Location location) throws Exception {
                        if (listener != null) {
                            listener.OnLocationLast(location);
                        }
                    }
                })
                .toObservable();
//         .doOnError(new Consumer<Throwable>() {
//            @Override public void accept(@NonNull Throwable throwable) throws Exception {
//                if (listener != null) {
//                    listener.OnLocationError("Gagal mengambil lokasi terakhir", ERROR_RX_LOCATION);
//                }
//            }
//        })


    }

    private Observable<Address> getAddressObservable(final boolean success) {
        if (success) {
            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                getMyLocation();
                return null;
            }
            return rxLocation.location().updates(locationRequest)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext(new Consumer<Location>() {
                        @Override public void accept(@NonNull Location location) throws Exception {
                            if (listener != null) {
                                listener.OnLocationUpdates(location);
                            }
                        }
                    }).flatMap(new Function<Location, ObservableSource<Address>>() {
                        @Override public ObservableSource<Address> apply(@NonNull Location location) throws Exception {
                            return getAddressFromLocation(location);
                        }
                    });
//            .doOnError(new Consumer<Throwable>() {
//                @Override public void accept(@NonNull Throwable throwable) throws Exception {
//                    if (listener != null) {
//                        listener.OnLocationError("Gagal mengambil lokasi terbaru", ERROR_RX_LOCATION);
//                    }
//                }
//            })
        }

        return rxLocation.location().lastLocation()
                .doOnSuccess(new Consumer<Location>() {
                    @Override public void accept(@NonNull Location location) throws Exception {
                        if (listener != null) {
                            listener.OnLocationLast(location);
                        }
                    }
                })
                .flatMapObservable(new Function<Location, ObservableSource<? extends Address>>() {
                    @Override public ObservableSource<? extends Address> apply(@NonNull Location location) throws Exception {
                        return getAddressFromLocation(location);
                    }
                });


//                .doOnError(new Consumer<Throwable>() {
//            @Override public void accept(@NonNull Throwable throwable) throws Exception {
//                if (listener != null) {
//                    listener.OnLocationError("Gagal mengambil lokasi terakhir", ERROR_RX_LOCATION);
//                }
//            }
//        })


    }

    private Observable<Address> getAddressFromLocation(Location location) {
        return rxLocation.geocoding().fromLocation(location).toObservable()
                .subscribeOn(Schedulers.io());
    }

    public void getAddressByLoc(double latitude, double longitude) {
        Observable<List<Address>> reverseGeocodeObservable = rxLocation.geocoding().fromLocation(latitude, longitude, 1)
                .toObservable();
        reverseGeocodeObservable
                .subscribeOn(Schedulers.io())               // use I/O thread to query for addresses
                .observeOn(AndroidSchedulers.mainThread())  // return result in main android thread to manipulate UI
                .subscribe(new Observer<List<Address>>() {
                    @Override public void onSubscribe(Disposable d) {

                    }

                    @Override public void onNext(List<Address> addresses) {
                        if (onAddressUpdateListener != null) {
                            if (addresses != null && addresses.size() > 0) {
                                onAddressUpdateListener.onAddressUpdate(addresses.get(0));
                            }else{
                                onAddressUpdateListener.onAddressError(new Throwable());
                            }
                        }
                    }

                    @Override public void onError(Throwable e) {
                        e.printStackTrace();
                        if (onAddressUpdateListener != null) {
                            onAddressUpdateListener.onAddressError(e);
                        }
                    }

                    @Override public void onComplete() {

                    }
                });
    }

    public void unregister() {
        disposable.clear();
        listener = null;
        onAddressUpdateListener = null;
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }

    public interface LocationInterface {

        void OnLocationUpdates(Location location);

        void OnLocationLast(Location location);

        void gotoLocationSettings();

        void OnLocationError(String msg, int code);
    }

    public interface OnAddressUpdateListener {
        void onAddressUpdate(Address address);

        void onAddressError(Throwable throwable);
    }
}
