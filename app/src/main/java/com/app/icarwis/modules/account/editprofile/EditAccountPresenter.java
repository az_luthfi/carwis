package com.app.icarwis.modules.account.editprofile;

import android.Manifest;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.app.icarwis.R;
import com.app.icarwis.app.App;
import com.app.icarwis.base.BaseRxLcePresenter;
import com.app.icarwis.models.customer.Customer;
import com.app.icarwis.models.customer.RequestCustomer;
import com.app.icarwis.models.datalist.Province;
import com.app.icarwis.models.eventbus.EventUpdateProfile;
import com.app.icarwis.utility.CommonUtilities;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.greenrobot.eventbus.EventBus;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.app.icarwis.utility.Config.CUSTOMER_AVATAR;
import static com.app.icarwis.utility.Config.CUSTOMER_ID;
import static com.app.icarwis.utility.Config.CUSTOMER_NAME;


public class EditAccountPresenter extends BaseRxLcePresenter<XEditAccountView, RequestCustomer>
        implements XEditAccountPresenter {
    private RxPermissions rxPermissions;
    private ArrayList<Province> provinces;
    public EditAccountPresenter(Context context, FragmentActivity activity) {
        super(context);
        rxPermissions = new RxPermissions(activity);
    }

    @Override public void loadData(boolean pullToRefresh) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "profile");
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));

        subscribe(App.getService().apiCustomer(params), pullToRefresh);
    }

    @Override public void loadProvince() {
        if (provinces == null){
            Type mapType = new TypeToken<ArrayList<Province>>() {
            }.getType();
            provinces = new Gson().fromJson(CommonUtilities.loadJSONFromAsset(context, "province.json"), mapType);
        }

        if (isViewAttached()){
            getView().showDialog(provinces, "Provinsi");
        }
    }

    @Override public void requestPermissionGallery() {
        rxPermissions
                .request(Manifest.permission.READ_EXTERNAL_STORAGE)
                .subscribe(new Observer<Boolean>() {
                    @Override public void onSubscribe(Disposable d) {
                    }

                    @Override public void onNext(Boolean aBoolean) {
                        if (isViewAttached()) {
                            if (aBoolean) {
                                getView().imageFromGallery();
                            } else {
                                Toast.makeText(context, "Permission denied, can't open the gallery", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override public void onError(Throwable e) {
                    }

                    @Override public void onComplete() {
                    }
                });
    }

    @Override public void requestUpdateProfile(HashMap<String, RequestBody> params, MultipartBody.Part requestImage) {
        if (isViewAttached()) {
            getView().showLoadingUpdate(true);
        }
        params.put("act", RequestBody.create(MediaType.parse("multipart/form-data"), "saveProfile"));
        params.put("customerId", RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID))));

        Observer<RequestCustomer> observer1 = new Observer<RequestCustomer>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestCustomer data) {
                if (isViewAttached()) {
                    getView().showLoadingUpdate(false);
                    saveSession(data.getCustomer());
                    Toast.makeText(context, data.getText(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    Toast.makeText(context, R.string.error_network_light, Toast.LENGTH_SHORT).show();
                    getView().showLoadingUpdate(false);
                }
            }

            @Override public void onComplete() {

            }
        };
        App.getService().apiRegisterMember(params, requestImage).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer1);
    }

    private void saveSession(Customer customer) {
        prefs.savePreferences(CUSTOMER_NAME, customer.getCustomerName());
        prefs.savePreferences(CUSTOMER_AVATAR, customer.getCustomerImage());
        EventBus.getDefault().post(new EventUpdateProfile());
        if (isViewAttached()) {
            getView().gotoBack();
        }
    }
}