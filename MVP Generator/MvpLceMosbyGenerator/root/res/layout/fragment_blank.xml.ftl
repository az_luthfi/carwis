<FrameLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent" >

    <RelativeLayout
            android:id="@+id/contentView"
            android:layout_width="match_parent"
            android:layout_height="match_parent">

    </RelativeLayout>

    <include layout="@layout/view_error"/>

    <include layout="@layout/view_loading"/>
</FrameLayout>
