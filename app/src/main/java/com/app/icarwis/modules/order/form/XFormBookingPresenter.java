package com.app.icarwis.modules.order.form;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XFormBookingPresenter extends MvpPresenter<XFormBookingView> {

}