package com.app.icarwis.modules.order.paketwisata.detail;

import android.content.Context;

import com.app.icarwis.base.BasePresenter;

public class PaketWisataDetailPresenter extends BasePresenter<XPaketWisataDetailView>
        implements XPaketWisataDetailPresenter {

    public PaketWisataDetailPresenter(Context context) {
        super(context);
    }

}