package com.app.icarwis.holder.wisata;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.icarwis.R;
import com.app.icarwis.adapter.wisata.WisataCartAdapter;
import com.app.icarwis.models.wisata.Wisata;
import com.app.icarwis.utility.CommonUtilities;
import com.app.icarwis.utility.Str;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ItemWisataCartHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.ivImage) ImageView ivImage;
    @BindView(R.id.tvName) TextView tvName;
    @BindView(R.id.tvAddress) TextView tvAddress;
    @BindView(R.id.layName) LinearLayout layName;
    @BindView(R.id.tvAmount) TextView tvAmount;
    @BindView(R.id.layUp) FrameLayout layUp;
    @BindView(R.id.layDelete) FrameLayout layDelete;
    @BindView(R.id.layDown) FrameLayout layDown;

    private WisataCartAdapter.onActionListener listener;
    public ItemWisataCartHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_wisata_cart, parent, false));
    }

    public ItemWisataCartHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void bindView(Wisata model, WisataCartAdapter.onActionListener listener){
        this.listener = listener;
        if (!TextUtils.isEmpty(model.getMediaValue())) {
            Picasso.with(itemView.getContext()).load(model.getMediaValue()).placeholder(R.color.md_grey_500).resize(500, 0).into(ivImage);
        } else {
            ivImage.setImageResource(R.color.md_grey_500);
        }

        tvName.setText(model.getCwName());
        tvAddress.setText(Str.getText(model.getCwAddress(), ""));
        tvAmount.setText(CommonUtilities.toRupiahNumberFormat(model.getAmount() + model.getCwService()));
        if (listener.getItemsCount() == getAdapterPosition() +1){
            layDown.setVisibility(View.INVISIBLE);
        }else{
            layDown.setVisibility(View.VISIBLE);
        }
        if (getAdapterPosition() == 0){
            layUp.setVisibility(View.INVISIBLE);
        }else{
            layUp.setVisibility(View.VISIBLE);
        }
    }

    @OnClick({R.id.layUp, R.id.layDelete, R.id.layDown, R.id.layName}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layUp:
                listener.onClickUp(getAdapterPosition());
                break;
            case R.id.layDelete:
                listener.onClickDelete(getAdapterPosition());
                break;
            case R.id.layDown:
                listener.onClickDown(getAdapterPosition());
                break;
            case R.id.layName:
                listener.onClickDetail(getAdapterPosition());
                break;
        }
    }
}
