package com.app.icarwis.modules.order.wisata.wisata;

import com.app.icarwis.models.wisata.RequestWisata;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XWisataView extends MvpLceView<RequestWisata> {

    void showLoadingNextPage(boolean show);

    void setNextData(RequestWisata data);

    void showErrorNextPage(String string);

    void showProgressDialog(boolean show);

    void showError(String string);

    void onNextDirection(RequestWisata data);

    void updateDataAdapter();
}