package com.app.icarwis.modules.order.wisata.cart;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XWisataCartPresenter extends MvpPresenter<XWisataCartView> {

}