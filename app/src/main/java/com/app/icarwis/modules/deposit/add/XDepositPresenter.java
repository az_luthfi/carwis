package com.app.icarwis.modules.deposit.add;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XDepositPresenter extends MvpPresenter<XDepositView> {

    void getActiveDeposit();

    void requestDeposit(String nominal, String bankSelected);
}