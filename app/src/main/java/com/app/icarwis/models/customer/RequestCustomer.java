package com.app.icarwis.models.customer;

import com.app.icarwis.models.RequestBase;
import com.app.icarwis.models.deposit.Bank;
import com.app.icarwis.models.deposit.Deposit;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by luthfi on 21/07/2017.
 */

public class RequestCustomer extends RequestBase {
    @SerializedName("customer")
    @Expose
    private Customer customer;
    @SerializedName("customer_password")
    @Expose
    private String customerPassword;
    @SerializedName("deposit")
    @Expose
    private Deposit deposit;
    @SerializedName("banks")
    @Expose
    private ArrayList<Bank> banks = new ArrayList<>();

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getCustomerPassword() {
        return customerPassword;
    }

    public void setCustomerPassword(String customerPassword) {
        this.customerPassword = customerPassword;
    }

    public Deposit getDeposit() {
        return deposit;
    }

    public ArrayList<Bank> getBanks() {
        return banks;
    }
}
