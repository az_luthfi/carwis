package com.app.icarwis.modules.home;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XHomePresenter extends MvpPresenter<XHomeView> {

    void loadData(boolean pullToRefresh);
}