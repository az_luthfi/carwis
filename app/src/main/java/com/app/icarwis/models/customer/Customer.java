package com.app.icarwis.models.customer;

import com.app.icarwis.models.deposit.Bank;
import com.app.icarwis.models.deposit.Deposit;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by luthfi on 21/07/2017.
 */

public class Customer {

    @SerializedName("customer_id")
    @Expose
    private Integer customerId;
    @SerializedName("customer_level")
    @Expose
    private String customerLevel;
    @SerializedName("merchant_id")
    @Expose
    private Integer merchantId;
    @SerializedName("customer_reg_id")
    @Expose
    private String customerRegId;
    @SerializedName("customer_min")
    @Expose
    private String customerMin;
    @SerializedName("customer_mdn")
    @Expose
    private String customerMdn;
    @SerializedName("customer_msisdn")
    @Expose
    private String customerMsisdn;
    @SerializedName("customer_handset")
    @Expose
    private String customerHandset;
    @SerializedName("customer_provider")
    @Expose
    private String customerProvider;
    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("customer_email")
    @Expose
    private String customerEmail;
    @SerializedName("customer_phone")
    @Expose
    private String customerPhone;
    @SerializedName("customer_password")
    @Expose
    private String customerPassword;
    @SerializedName("customer_image")
    @Expose
    private String customerImage;
    @SerializedName("customer_image_name")
    @Expose
    private String customerImageName;
    @SerializedName("customer_birth_place")
    @Expose
    private String customerBirthPlace;
    @SerializedName("customer_birth_date")
    @Expose
    private String customerBirthDate;
    @SerializedName("customer_gender")
    @Expose
    private String customerGender;
    @SerializedName("customer_address")
    @Expose
    private String customerAddress;
    @SerializedName("customer_city")
    @Expose
    private String customerCity;
    @SerializedName("customer_province")
    @Expose
    private String customerProvince;
    @SerializedName("customer_kewarganegaraan")
    @Expose
    private String customerKewarganegaraan;
    @SerializedName("customer_postcode")
    @Expose
    private String customerPostcode;
    @SerializedName("customer_status")
    @Expose
    private String customerStatus;
    @SerializedName("customer_create_date")
    @Expose
    private String customerCreateDate;
    @SerializedName("customer_saldo")
    @Expose
    private Integer customerSaldo;
    @SerializedName("register_price")
    @Expose
    private String registerPrice; // register
    @SerializedName("product_code")
    @Expose
    private String productCode; // register
    @SerializedName("banks")
    @Expose
    private ArrayList<Bank> banks = new ArrayList<>(); // register
    @SerializedName("deposit")
    @Expose
    private Deposit deposit; // register

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getCustomerLevel() {
        return customerLevel;
    }

    public void setCustomerLevel(String customerLevel) {
        this.customerLevel = customerLevel;
    }

    public Integer getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Integer merchantId) {
        this.merchantId = merchantId;
    }

    public String getCustomerRegId() {
        return customerRegId;
    }

    public void setCustomerRegId(String customerRegId) {
        this.customerRegId = customerRegId;
    }

    public String getCustomerMin() {
        return customerMin;
    }

    public void setCustomerMin(String customerMin) {
        this.customerMin = customerMin;
    }

    public String getCustomerMdn() {
        return customerMdn;
    }

    public void setCustomerMdn(String customerMdn) {
        this.customerMdn = customerMdn;
    }

    public String getCustomerMsisdn() {
        return customerMsisdn;
    }

    public void setCustomerMsisdn(String customerMsisdn) {
        this.customerMsisdn = customerMsisdn;
    }

    public String getCustomerHandset() {
        return customerHandset;
    }

    public void setCustomerHandset(String customerHandset) {
        this.customerHandset = customerHandset;
    }

    public String getCustomerProvider() {
        return customerProvider;
    }

    public void setCustomerProvider(String customerProvider) {
        this.customerProvider = customerProvider;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getCustomerPassword() {
        return customerPassword;
    }

    public void setCustomerPassword(String customerPassword) {
        this.customerPassword = customerPassword;
    }

    public String getCustomerImage() {
        return customerImage;
    }

    public void setCustomerImage(String customerImage) {
        this.customerImage = customerImage;
    }

    public String getCustomerBirthPlace() {
        return customerBirthPlace;
    }

    public void setCustomerBirthPlace(String customerBirthPlace) {
        this.customerBirthPlace = customerBirthPlace;
    }

    public String getCustomerBirthDate() {
        return customerBirthDate;
    }

    public void setCustomerBirthDate(String customerBirthDate) {
        this.customerBirthDate = customerBirthDate;
    }

    public String getCustomerGender() {
        return customerGender;
    }

    public void setCustomerGender(String customerGender) {
        this.customerGender = customerGender;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerCity() {
        return customerCity;
    }

    public void setCustomerCity(String customerCity) {
        this.customerCity = customerCity;
    }

    public String getCustomerProvince() {
        return customerProvince;
    }

    public void setCustomerProvince(String customerProvince) {
        this.customerProvince = customerProvince;
    }

    public String getCustomerPostcode() {
        return customerPostcode;
    }

    public void setCustomerPostcode(String customerPostcode) {
        this.customerPostcode = customerPostcode;
    }

    public String getCustomerStatus() {
        return customerStatus;
    }

    public void setCustomerStatus(String customerStatus) {
        this.customerStatus = customerStatus;
    }

    public String getCustomerCreateDate() {
        return customerCreateDate;
    }

    public void setCustomerCreateDate(String customerCreateDate) {
        this.customerCreateDate = customerCreateDate;
    }

    public Integer getCustomerSaldo() {
        return customerSaldo;
    }

    public void setCustomerSaldo(Integer customerSaldo) {
        this.customerSaldo = customerSaldo;
    }

    public String getCustomerImageName() {
        return customerImageName;
    }

    public void setCustomerImageName(String customerImageName) {
        this.customerImageName = customerImageName;
    }

    public String getRegisterPrice() {
        return registerPrice;
    }

    public void setRegisterPrice(String registerPrice) {
        this.registerPrice = registerPrice;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public ArrayList<Bank> getBanks() {
        return banks;
    }

    public Deposit getDeposit() {
        return deposit;
    }

    public String getCustomerKewarganegaraan() {
        return customerKewarganegaraan;
    }
}
