package com.app.icarwis.modules.account.balance;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XMyBalancePresenter extends MvpPresenter<XMyBalanceView> {

}