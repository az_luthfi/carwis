package com.app.icarwis.modules.order.wisata.cart;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.app.icarwis.R;
import com.app.icarwis.adapter.wisata.WisataAdapter;
import com.app.icarwis.adapter.wisata.WisataCartPriceAdapter;
import com.app.icarwis.base.BaseMvpFragment;
import com.app.icarwis.listeners.OnChangeToolbar;
import com.app.icarwis.models.wisata.RequestWisata;
import com.app.icarwis.models.wisata.Wisata;
import com.app.icarwis.modules.order.wisata.car.CarListFragmentBuilder;
import com.app.icarwis.utility.CommonUtilities;
import com.google.gson.Gson;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;

@FragmentWithArgs
public class WisataCartFragment extends BaseMvpFragment<XWisataCartView, WisataCartPresenter>
        implements XWisataCartView, OnChangeToolbar {

    @Arg String jsonRequestWisata;
    @Arg HashMap<String, String> params;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.recyclerView2) RecyclerView recyclerView2;
    @BindView(R.id.tvSubtotal) TextView tvSubtotal;
    @BindView(R.id.bntSubmit) TextView bntSubmit;

    private RequestWisata requestWisata;
    private WisataAdapter wisataAdapter = new WisataAdapter(null, null, WisataAdapter.TYPE_CART);
    private WisataCartPriceAdapter priceAdapter = new WisataCartPriceAdapter();

    public WisataCartFragment() {

    }

    @Override public WisataCartPresenter createPresenter() {
        return new WisataCartPresenter(getContext());
    }

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWisata = new Gson().fromJson(jsonRequestWisata, RequestWisata.class);
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_wisata_cart;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView.setHasFixedSize(true);
        recyclerView2.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView2.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView2.setNestedScrollingEnabled(false);
        ViewCompat.setNestedScrollingEnabled(recyclerView, false);
        ViewCompat.setNestedScrollingEnabled(recyclerView2, false);
        recyclerView.setAdapter(wisataAdapter);
        recyclerView2.setAdapter(priceAdapter);
        wisataAdapter.setItems(requestWisata.getWisatas());
        priceAdapter.setItems(requestWisata.getWisatas());

        tvSubtotal.setText(CommonUtilities.toRupiahNumberFormat(requestWisata.getSubtotal()));
    }

    @Override public String getTitle() {
        return "Wisata Cart";
    }

    @Override public boolean isSearch() {
        return false;
    }

    @Override public boolean isCart() {
        return false;
    }


    @OnClick(R.id.bntSubmit) public void onViewClicked() {
        if (!TextUtils.isEmpty(params.get("travelDuration"))) {
            params.remove("travelDuration");
        }
        if (!TextUtils.isEmpty(params.get("visitDuration"))) {
            params.remove("visitDuration");
        }
        params.put("travelDuration", String.valueOf(requestWisata.getDuration().getValue()));
        Float totalVisitDuration = 0f;
        for (Wisata wisata1 : wisataAdapter.getItems()) {
            totalVisitDuration += wisata1.getCwTimeVisit();
        }
        params.put("visitDuration", String.valueOf(totalVisitDuration));
        mListener.gotoPage(new CarListFragmentBuilder(jsonRequestWisata, params).build(), false, null);

    }
}