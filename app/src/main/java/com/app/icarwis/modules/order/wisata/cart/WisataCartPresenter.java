package com.app.icarwis.modules.order.wisata.cart;

import android.content.Context;

import com.app.icarwis.base.BasePresenter;

public class WisataCartPresenter extends BasePresenter<XWisataCartView>
        implements XWisataCartPresenter {

    public WisataCartPresenter(Context context) {
        super(context);
    }

}