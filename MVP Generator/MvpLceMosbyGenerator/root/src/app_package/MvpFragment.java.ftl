package ${packageName};

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.RelativeLayout;
import android.view.View;

import ${applicationPackage}.R;

import ${applicationPackage}.base.BaseMvpLceFragment;

public class ${className}Fragment extends BaseMvpLceFragment<RelativeLayout, ${modelName}, X${className}View, ${className}Presenter>
        implements X${className}View{

    public ${className}Fragment()
    {

    }

    @Override public ${className}Presenter createPresenter() {
            return new ${className}Presenter(getContext());
    }

    @Override protected int getLayoutRes() {
    <#if includeLayout>
        return R.layout.fragment_${classToResource(className)};
    <#else>
        return 0;
    </#if>
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData(false);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return e.getMessage();
    }

    @Override public void setData(${modelName} data) {

    }

    @Override public void loadData(boolean pullToRefresh) {

    }
}