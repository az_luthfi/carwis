package com.app.icarwis.holder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.app.icarwis.R;
import com.app.icarwis.models.datalist.SimpleList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemSimpleHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvText) TextView tvText;
    @BindView(R.id.container) FrameLayout container;

    public ItemSimpleHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_simple, parent, false));
    }

    public ItemSimpleHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void bindView(SimpleList item, boolean isSelected) {
        tvText.setText(item.getName());
        container.setSelected(isSelected);
    }
}
