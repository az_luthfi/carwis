package com.app.icarwis.models.car;

import com.app.icarwis.models.RequestBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Luthfi Aziz on 19/11/2017.
 */

public class RequestCar extends RequestBase {
    @SerializedName("cars")
    @Expose
    private ArrayList<Car> cars = new ArrayList<>();
    @SerializedName("car_prices")
    @Expose
    private ArrayList<CarPrice> carPrices = new ArrayList<>();

    public ArrayList<Car> getCars() {
        return cars;
    }

    public ArrayList<CarPrice> getCarPrices() {
        return carPrices;
    }
}
