package com.app.icarwis.holder.content;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.icarwis.R;
import com.app.icarwis.models.berita.Content;
import com.app.icarwis.utility.CommonUtilities;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ItemContentListHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.ivContentImage) ImageView ivContentImage;
    @BindView(R.id.tvContentTitle) TextView tvContentTitle;
    @BindView(R.id.tvContentDate) TextView tvContentDate;
    @BindView(R.id.itemContainer) RelativeLayout itemContainer;

    public ItemContentListHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_content_list, parent, false));
}

    public ItemContentListHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void bindView(final Content item) {

        Context context = itemView.getContext();
        if (item.getContentImage() != null) {
            Picasso.with(context).load(item.getContentImage()).into(ivContentImage);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ivContentImage.setTransitionName(item.getContentId());
            }
        }
        tvContentTitle.setText(CommonUtilities.toHtml(item.getContentName()));
        tvContentDate.setText(item.getContentCreateDate());
    }
}
