package com.app.icarwis.adapter.transaction;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.app.icarwis.base.BaseRecyclerViewLoadingAdapter;
import com.app.icarwis.holder.ItemLoadingHolder;
import com.app.icarwis.holder.transaction.ItemBookingHolder;
import com.app.icarwis.models.transaction.Transaction;

/**
 * Created by Luthfi Aziz on 09/12/2017.
 */

public class BookingAdapter extends BaseRecyclerViewLoadingAdapter<Transaction> {

    public BookingAdapter(ItemLoadingHolder.ItemViewLoadingListener loadingListener) {
        super(loadingListener);
    }

    @Override protected int getItemViewTypeChild(int position) {
        return 1;
    }

    @Override protected RecyclerView.ViewHolder onCreateViewHolderChild(ViewGroup parent, int viewType, LayoutInflater inflater) {
        return new ItemBookingHolder(inflater, parent);
    }

    @Override protected void onBindViewHolderChild(RecyclerView.ViewHolder holder, int position, Transaction model) {
        if (holder instanceof ItemBookingHolder){
            ((ItemBookingHolder) holder).bindView(model);
        }
    }
}
