package com.app.icarwis.modules.splash.activity;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.app.icarwis.R;
import com.app.icarwis.base.BaseFragment;
import com.app.icarwis.modules.splash.login.LoginFragment;
import com.app.icarwis.modules.splash.register.RegisterFragment;
import com.app.icarwis.utility.SmartFragmentStatePagerAdapter;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class SplashFragment extends BaseFragment {


    @BindView(R.id.tabLayout) TabLayout tabLayout;
    @BindView(R.id.viewPager) ViewPager viewPager;
    private SplashPagerAdapter pagerAdapter;

    public SplashFragment() {
        // Required empty public constructor
    }


    @Override protected int getLayoutRes() {
        return R.layout.fragment_splash;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pagerAdapter = new SplashPagerAdapter(getActivity().getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);

        viewPager.setCurrentItem(1);
        // Give the TabLayout the ViewPager
        tabLayout.setupWithViewPager(viewPager);
    }

    private class SplashPagerAdapter extends SmartFragmentStatePagerAdapter {
        private String tabTitles[] = new String[]{"REGISTER", "LOGIN"};

        public SplashPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return tabTitles.length;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 1:
                    return new LoginFragment();
                default:
                    return new RegisterFragment();
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            return tabTitles[position];
        }
    }
}
