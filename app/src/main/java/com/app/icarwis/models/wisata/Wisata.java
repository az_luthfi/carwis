package com.app.icarwis.models.wisata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Luthfi Aziz on 18/11/2017.
 */

public class Wisata implements Serializable{
    @SerializedName("cw_id")
    @Expose
    private String cwId;
    @SerializedName("cw_name")
    @Expose
    private String cwName;
    @SerializedName("cw_address")
    @Expose
    private String cwAddress;
    @SerializedName("cw_lat")
    @Expose
    private Double cwLat;
    @SerializedName("cw_long")
    @Expose
    private Double cwLong;
    @SerializedName("cw_time_visit")
    @Expose
    private Float cwTimeVisit;
    @SerializedName("cw_service")
    @Expose
    private Double cwService;
    @SerializedName("cw_desc")
    @Expose
    private String cwDesc;
    @SerializedName("cw_status")
    @Expose
    private String cwStatus;
    @SerializedName("cw_create_date")
    @Expose
    private String cwCreateDate;
    @SerializedName("distance")
    @Expose
    private Double distance;
    @SerializedName("amount")
    @Expose
    private Double amount;
    @SerializedName("media_value")
    @Expose
    private String mediaValue;
    @SerializedName("is_checked")
    @Expose
    private boolean isChecked;
    private int oldPosition;

    public String getCwId() {
        return cwId;
    }

    public void setCwId(String cwId) {
        this.cwId = cwId;
    }

    public String getCwName() {
        return cwName;
    }

    public void setCwName(String cwName) {
        this.cwName = cwName;
    }

    public String getCwAddress() {
        return cwAddress;
    }

    public void setCwAddress(String cwAddress) {
        this.cwAddress = cwAddress;
    }

    public Double getCwLat() {
        return cwLat;
    }

    public void setCwLat(Double cwLat) {
        this.cwLat = cwLat;
    }

    public Double getCwLong() {
        return cwLong;
    }

    public void setCwLong(Double cwLong) {
        this.cwLong = cwLong;
    }

    public Float getCwTimeVisit() {
        return cwTimeVisit;
    }

    public void setCwTimeVisit(Float cwTimeVisit) {
        this.cwTimeVisit = cwTimeVisit;
    }

    public Double getCwService() {
        return cwService;
    }

    public void setCwService(Double cwService) {
        this.cwService = cwService;
    }

    public String getCwDesc() {
        return cwDesc;
    }

    public void setCwDesc(String cwDesc) {
        this.cwDesc = cwDesc;
    }

    public String getCwStatus() {
        return cwStatus;
    }

    public void setCwStatus(String cwStatus) {
        this.cwStatus = cwStatus;
    }

    public String getCwCreateDate() {
        return cwCreateDate;
    }

    public void setCwCreateDate(String cwCreateDate) {
        this.cwCreateDate = cwCreateDate;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getMediaValue() {
        return mediaValue;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public int getOldPosition() {
        return oldPosition;
    }

    public void setOldPosition(int oldPosition) {
        this.oldPosition = oldPosition;
    }
}
