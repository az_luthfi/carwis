package com.app.icarwis.models.deposit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by j3p0n on 1/14/2017.
 */

public class Deposit {

    @SerializedName("deposit_id")
    @Expose
    private String depositId;
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("deposit_amount")
    @Expose
    private String depositAmount;
    @SerializedName("deposit_bank_to")
    @Expose
    private String depositBankTo;
    @SerializedName("deposit_bank_to_acc")
    @Expose
    private String depositBankToAcc;
    @SerializedName("deposit_bank_to_number")
    @Expose
    private String depositBankToNumber;
    @SerializedName("deposit_bank_from")
    @Expose
    private String depositBankFrom;
    @SerializedName("deposit_bank_acc")
    @Expose
    private String depositBankAcc;
    @SerializedName("deposit_text")
    @Expose
    private String depositText;
    @SerializedName("deposit_image")
    @Expose
    private String depositImage;
    @SerializedName("deposit_status")
    @Expose
    private String depositStatus;
    @SerializedName("deposit_create_date")
    @Expose
    private String depositCreateDate;
    @SerializedName("deposit_confirm_date")
    @Expose
    private String depositConfirmDate;
    @SerializedName("deposit_update_date")
    @Expose
    private String depositUpdateDate;
    @SerializedName("deposit_update_text")
    @Expose
    private String depositUpdateText;
    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("customer_saldo")
    @Expose
    private String customerSaldo;
    @SerializedName("dpid")
    @Expose
    private Integer dpid;
    @SerializedName("bank")
    @Expose
    private Bank bank;
    @SerializedName("min_deposit")
    @Expose
    private String minDeposit;

    public String getDepositId() {
        return depositId;
    }

    public void setDepositId(String depositId) {
        this.depositId = depositId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(String depositAmount) {
        this.depositAmount = depositAmount;
    }

    public String getDepositBankTo() {
        return depositBankTo;
    }

    public void setDepositBankTo(String depositBankTo) {
        this.depositBankTo = depositBankTo;
    }

    public String getDepositBankToAcc() {
        return depositBankToAcc;
    }

    public void setDepositBankToAcc(String depositBankToAcc) {
        this.depositBankToAcc = depositBankToAcc;
    }

    public String getDepositBankToNumber() {
        return depositBankToNumber;
    }

    public void setDepositBankToNumber(String depositBankToNumber) {
        this.depositBankToNumber = depositBankToNumber;
    }

    public String getDepositBankFrom() {
        return depositBankFrom;
    }

    public void setDepositBankFrom(String depositBankFrom) {
        this.depositBankFrom = depositBankFrom;
    }

    public String getDepositBankAcc() {
        return depositBankAcc;
    }

    public void setDepositBankAcc(String depositBankAcc) {
        this.depositBankAcc = depositBankAcc;
    }

    public String getDepositText() {
        return depositText;
    }

    public void setDepositText(String depositText) {
        this.depositText = depositText;
    }

    public String getDepositImage() {
        return depositImage;
    }

    public void setDepositImage(String depositImage) {
        this.depositImage = depositImage;
    }

    public String getDepositStatus() {
        return depositStatus;
    }

    public void setDepositStatus(String depositStatus) {
        this.depositStatus = depositStatus;
    }

    public String getDepositCreateDate() {
        return depositCreateDate;
    }

    public void setDepositCreateDate(String depositCreateDate) {
        this.depositCreateDate = depositCreateDate;
    }

    public String getDepositConfirmDate() {
        return depositConfirmDate;
    }

    public void setDepositConfirmDate(String depositConfirmDate) {
        this.depositConfirmDate = depositConfirmDate;
    }

    public String getDepositUpdateDate() {
        return depositUpdateDate;
    }

    public void setDepositUpdateDate(String depositUpdateDate) {
        this.depositUpdateDate = depositUpdateDate;
    }

    public String getDepositUpdateText() {
        return depositUpdateText;
    }

    public void setDepositUpdateText(String depositUpdateText) {
        this.depositUpdateText = depositUpdateText;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerSaldo() {
        return customerSaldo;
    }

    public void setCustomerSaldo(String customerSaldo) {
        this.customerSaldo = customerSaldo;
    }

    public Integer getDpid() {
        return dpid;
    }

    public void setDpid(Integer dpid) {
        this.dpid = dpid;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public String getMinDeposit() {
        return minDeposit;
    }

    public void setMinDeposit(String minDeposit) {
        this.minDeposit = minDeposit;
    }

}