package com.app.icarwis.models.datalist;

/**
 * Created by luthfi on 28/04/2017.
 */

public interface SimpleList {
    String getName();

    String getId();
}
