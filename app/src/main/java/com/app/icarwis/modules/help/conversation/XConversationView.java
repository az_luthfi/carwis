package com.app.icarwis.modules.help.conversation;


import com.app.icarwis.models.pesan.RequestMessage;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XConversationView extends MvpLceView<RequestMessage> {
    void showLoadingNextPage(boolean show);

    void showErrorNextPage(String msg);

    void setNextData(RequestMessage data);

    void imageFromGallery();

    void showProgressSend(boolean show);

    void setDataNewConversation(RequestMessage m);

    void onReplyFromCs(String idPesan);

    void setDataFromNotification(RequestMessage data);
}