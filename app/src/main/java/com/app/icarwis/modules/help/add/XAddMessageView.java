package com.app.icarwis.modules.help.add;

import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface XAddMessageView extends MvpView {

    void showProgressDialog(boolean b);

    void imageFromGallery();

    void showDialogSuccess(String text);

    void showDialogError(String string);
}