package com.app.icarwis.modules.order.paketwisata.list;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XPaketWisataPresenter extends MvpPresenter<XPaketWisataView> {
    void loadData(boolean pullToRefresh, int page);
}