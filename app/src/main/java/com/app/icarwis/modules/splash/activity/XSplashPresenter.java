package com.app.icarwis.modules.splash.activity;

import com.app.icarwis.models.customer.Customer;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

/**
 * Created by Luthfi on 01/08/2017.
 */

public interface XSplashPresenter extends MvpPresenter<XSplashView> {
    void saveRegId(String regId);

    void checkIsLogin();

    void checkVersion();

    void checkIsAuth();

    void saveSession(Customer customer);
}
