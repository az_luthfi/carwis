package com.app.icarwis.modules.deposit.add;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.app.icarwis.R;
import com.app.icarwis.adapter.SimpleAdapter;
import com.app.icarwis.base.BaseMvpLceFragment;
import com.app.icarwis.listeners.OnChangeToolbar;
import com.app.icarwis.models.deposit.Bank;
import com.app.icarwis.models.deposit.Deposit;
import com.app.icarwis.models.deposit.RequestDeposit;
import com.app.icarwis.modules.deposit.confirm.DepositConfirmationFragment;
import com.app.icarwis.modules.deposit.confirm.DepositConfirmationFragmentBuilder;
import com.app.icarwis.utility.CommonUtilities;
import com.app.icarwis.utility.DialogListOption;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.app.icarwis.utility.Config.CUSTOMER_SALDO;


public class DepositFragment extends BaseMvpLceFragment<ScrollView, RequestDeposit, XDepositView, DepositPresenter>
        implements XDepositView, OnChangeToolbar {

    @BindView(R.id.etNominal) EditText etNominal;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.tvSaldo) TextView tvSaldo;
    @BindView(R.id.ivInfo) ImageView ivInfo;
    @BindView(R.id.etBank) TextView etBank;
    @BindView(R.id.buttonNext) Button buttonNext;
    @BindView(R.id.contentView) ScrollView contentView;

    private SimpleAdapter adapter;


    public DepositFragment() {

    }

    @Override public DepositPresenter createPresenter() {
        return new DepositPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_deposit;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new SimpleAdapter();
        tvSaldo.setText(CommonUtilities.toRupiahFormat(presenter.getPrefs().getPreferencesInt(CUSTOMER_SALDO)));
        loadData(false);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return getContext().getString(R.string.error_network);
    }

    @Override public void setData(RequestDeposit data) {
        if (data.getStatus()) {
            showDepositConfirmation(data.getDeposit());
        } else {
            initBank(data.getBanks());
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.getActiveDeposit();
    }

    @Override public void initBank(ArrayList<Bank> bankList) {
        if (adapter.getItemCount() > 0){
            adapter.clear();
        }
        adapter.addAll(bankList);
    }

    private void validateInput() {
        if (TextUtils.isEmpty(etNominal.getText())) {
            showSnackBar("Silahkan masukkan nominal deposit");
            return;
        }
        if (!TextUtils.isDigitsOnly(etNominal.getText())){
            showSnackBar("Nominal bukan angka");
            return;
        }
        Integer nominal = 0;
        try {
            nominal = Integer.parseInt(etNominal.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (nominal < 50000) {
            showSnackBar("Minimal deposit Rp 50.000");
            return;
        }

        if (TextUtils.isEmpty(etBank.getText())) {
            showSnackBar("Silahkan pilih Bank tujuan");
            return;
        }
        presenter.requestDeposit(etNominal.getText().toString(), etBank.getText().toString());
    }

    public void showDepositConfirmation(Deposit deposit) {
        DepositConfirmationFragment page = new DepositConfirmationFragmentBuilder(new Gson().toJson(deposit)).build();
        mListener.back();
        mListener.gotoPage(page, false, "Konfirmasi Deposit");
    }

    @Override public void showProgressDialog(boolean show) {
        if (show) {
            showLoading("Mengirimkan data...", false);
        } else {
            hideLoading();
        }
    }

    @Override public void onError(String contextString) {
        showSnackBar(contextString);
    }

    @Nullable @Override public String getTitle() {
        return "Tambah Deposit";
    }

    @Override public boolean isSearch() {
        return false;
    }

    @Override public boolean isCart() {
        return false;
    }

    @OnClick({R.id.etBank, R.id.buttonNext, R.id.ivInfo}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.etBank:
                new DialogListOption(getContext(), "Bank", adapter) {
                    @Override public void onItemClicked(int position) {
                        if (position != -1) {
                            etBank.setText(adapter.getItem(position).getName());
                        }
                    }

                    @Override public void onFilter(CharSequence s) {
                        adapter.filter(s);
                    }
                };
                break;
            case R.id.buttonNext:
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                String currentDateandTime = sdf.format(new Date());
                try {
                    if (CommonUtilities.isTimeBetweenTwoTime("08:00:00", "20:00:00", currentDateandTime)) {
                        CommonUtilities.hideSoftKeyboard(getActivity());
                        validateInput();
                    } else {
                        showConfirmDialog(SweetAlertDialog.WARNING_TYPE, "Informasi", "Deposit hanya dapat dilakukan pada jam 08:00 sampai 20:00 WIB", "OK", null, "", true);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.ivInfo:
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View alertLayout = inflater.inflate(R.layout.dialog_info_deposit, null);
                Button close = (Button) alertLayout.findViewById(R.id.btnClose);
                final AlertDialog alert = new AlertDialog.Builder(getActivity()).create();
                alert.setView(alertLayout);
                alert.setCancelable(false);
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.dismiss();
                    }
                });

                alert.setView(alertLayout);
                alert.show();
                break;
        }
    }
}