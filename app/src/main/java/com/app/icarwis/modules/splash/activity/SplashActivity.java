package com.app.icarwis.modules.splash.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.app.icarwis.R;
import com.app.icarwis.listeners.FragmentInteractionListener;
import com.app.icarwis.listeners.SplashListener;
import com.app.icarwis.models.customer.Customer;
import com.app.icarwis.modules.main.MainActivity;
import com.app.icarwis.utility.CommonUtilities;
import com.app.icarwis.utility.FragmentStack;
import com.app.icarwis.utility.Logs;
import com.app.icarwis.utility.NotificationHelper;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.hannesdorfmann.mosby3.mvp.MvpActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.app.icarwis.modules.splash.activity.SplashPresenter.ACT_CHECK_VERSION;
import static com.app.icarwis.modules.splash.activity.SplashPresenter.ACT_IS_REGISTERED;


public class SplashActivity extends MvpActivity<XSplashView, XSplashPresenter>
        implements XSplashView, FragmentInteractionListener, SplashListener {

    @BindView(R.id.ivLogo) ImageView ivLogo;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.layLoading) RelativeLayout layLoading;
    @BindView(R.id.container) FrameLayout container;
    private FragmentStack fragmentStack;

    @NonNull @Override public XSplashPresenter createPresenter() {
        return new SplashPresenter(getApplicationContext());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        new NotificationHelper(this);
        FirebaseApp.initializeApp(getApplicationContext());
        layLoading.setVisibility(View.VISIBLE);
        fragmentStack = new FragmentStack(this, getSupportFragmentManager(), R.id.container);
        container.setVisibility(View.GONE);
        registerGcm();
        Logs.d("SplashActivity => onCreate ==> ");
        presenter.checkIsLogin();
        fragmentStack.push(new SplashFragment(), null);
    }

    private void registerGcm() {
        String regId = FirebaseInstanceId.getInstance().getToken();
        if (regId != null) {
            presenter.saveRegId(regId);
        }
        FirebaseMessaging.getInstance().subscribeToTopic(this.getString(R.string.topic_name));
    }

    @Override public void showContent() {
//        fragmentStack.push(new SplashFragment(), null);
        layLoading.setVisibility(View.GONE);
        container.setVisibility(View.VISIBLE);
    }

    @Override public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override public void showConfirmDialog(int alertType, String title, String content, String confirmLabel, String cancelLabel, final String action) {
        SweetAlertDialog alert = CommonUtilities.buildAlert(this, alertType, title, content, confirmLabel, cancelLabel);
        alert.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                onCancel(action);
                sweetAlertDialog.cancel();
            }
        });
        alert.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                onConfirm(action);
                sweetAlertDialog.cancel();
            }
        });
    }

    private void onConfirm(String action) {
        switch (action) {
            case "error" + ACT_CHECK_VERSION:
                presenter.checkVersion();
                break;
            case "error" + ACT_IS_REGISTERED:
                presenter.checkIsAuth();
                break;
            case "update":
                gotoPlayStore();
                break;
            case "maintenance":
                finish();
                break;
        }
    }

    private void onCancel(String action) {

    }

    @Override public void gotoPage(Fragment page, boolean shouldReplace, String tagName) {
        if (shouldReplace) {
            fragmentStack.push(page, tagName);
        } else {
            fragmentStack.pushAdd(page, tagName);
        }
    }

    @Override public void back() {
        onBackPressed();
    }

    @Override public void OnSuccessAuth(Customer customer) {
        Toast.makeText(this, "Anda berhasil masuk", Toast.LENGTH_SHORT).show();
        presenter.saveSession(customer);
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    private void gotoPlayStore() {
        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }
}
