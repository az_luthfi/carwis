package com.app.icarwis.modules.booking.detail;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XDetailMyBookingPresenter extends MvpPresenter<XDetailMyBookingView> {

    void loadData(boolean pullToRefresh, String bookingCode);

    void updateTransaction(String ctId, String status, String desc);
}