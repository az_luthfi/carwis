package com.app.icarwis.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by j3p0n on 8/18/2017.
 */

public class RequestBase {
    @SerializedName("status")
    @Expose
    protected Boolean status;
    @SerializedName("method")
    @Expose
    protected String method;
    @SerializedName("text")
    @Expose
    protected String text;
    @SerializedName("count")
    @Expose
    private Integer count = 0;
    @SerializedName("customer_saldo")
    @Expose
    private int customerSaldo;
    @SerializedName("user_status")
    @Expose
    protected String userStatus;

    public Boolean getStatus() {
        return status;
    }

    public String getMethod() {
        return method;
    }

    public String getText() {
        return text;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public int getCustomerSaldo() {
        return customerSaldo;
    }

    public String getUserStatus() {
        return userStatus;
    }
}
