package com.app.icarwis.modules.order.carrental.carparent;

import android.content.Context;

import com.app.icarwis.app.App;
import com.app.icarwis.base.BaseRxLcePresenter;
import com.app.icarwis.models.car.RequestCar;

import java.util.HashMap;

public class CarParentPresenter extends BaseRxLcePresenter<XCarParentView, RequestCar>
        implements XCarParentPresenter {

    public CarParentPresenter(Context context) {
        super(context);
    }

    @Override public void loadData() {
        HashMap<String, String> paramas = new HashMap<>();
        paramas.put("act", "list-hour");
        subscribe(App.getService().apiCar(paramas), false);
    }
}