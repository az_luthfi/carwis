package com.app.icarwis.modules.booking.list;

import com.app.icarwis.models.transaction.RequestTransaction;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XListMyBookingView extends MvpLceView<RequestTransaction> {
    void showLoadingNextPage(boolean show);

    void setNextData(RequestTransaction data);

    void showErrorNextPage(String string);
}