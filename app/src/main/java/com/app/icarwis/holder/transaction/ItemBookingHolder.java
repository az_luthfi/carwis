package com.app.icarwis.holder.transaction;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.icarwis.R;
import com.app.icarwis.models.transaction.Transaction;
import com.app.icarwis.utility.CommonUtilities;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemBookingHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvBookingCode) TextView tvBookingCode;
    @BindView(R.id.tvCreateDate) TextView tvCreateDate;
    @BindView(R.id.tvTypeBooking) TextView tvTypeBooking;
    @BindView(R.id.tvStatusText) TextView tvStatusText;
    @BindView(R.id.tvLabelCar) TextView tvLabelCar;
    @BindView(R.id.tvCarName) TextView tvCarName;
    @BindView(R.id.tvBookingTime) TextView tvBookingTime;
    @BindView(R.id.tvBookingDate) TextView tvBookingDate;

    public ItemBookingHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_booking, parent, false));
    }

    public ItemBookingHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void bindView(Transaction model) {
        tvBookingCode.setText(model.getCtBookingCode());
        tvTypeBooking.setText(model.getCtTypeText());
        tvStatusText.setText(CommonUtilities.toHtml(model.getCtStatusText()));
        if (model.getRental() == null) {
            tvLabelCar.setVisibility(View.INVISIBLE);
            tvCarName.setVisibility(View.INVISIBLE);
        } else {
            tvLabelCar.setVisibility(View.VISIBLE);
            tvCarName.setVisibility(View.VISIBLE);
            tvCarName.setText(model.getRental().getCcrName());
        }
        tvBookingTime.setText(CommonUtilities.getFormatedDate(model.getCtBookingTime(), "yyyy-MM-dd HH:mm:ss", "HH:mm"));
        tvBookingDate.setText(CommonUtilities.getFormatedDate(model.getCtBookingTime(), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy"));
        tvCreateDate.setText(CommonUtilities.getFormatedDate(model.getCtCreateDate(), "yyyy-MM-dd HH:mm:ss", "dd/MM/yyyy HH:mm"));
    }
}
