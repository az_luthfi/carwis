package com.app.icarwis.modules.deposit.add;


import com.app.icarwis.models.deposit.Bank;
import com.app.icarwis.models.deposit.Deposit;
import com.app.icarwis.models.deposit.RequestDeposit;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import java.util.ArrayList;

public interface XDepositView extends MvpLceView<RequestDeposit> {

    void initBank(ArrayList<Bank> banks);

    void showDepositConfirmation(Deposit deposit);

    void showProgressDialog(boolean show);

    void onError(String contextString);
}