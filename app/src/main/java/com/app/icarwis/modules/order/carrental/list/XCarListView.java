package com.app.icarwis.modules.order.carrental.list;

import com.app.icarwis.models.car.RequestCar;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XCarListView extends MvpLceView<RequestCar> {

    void showLoadingNextPage(boolean show);

    void setNextData(RequestCar data);

    void showErrorNextPage(String string);
}