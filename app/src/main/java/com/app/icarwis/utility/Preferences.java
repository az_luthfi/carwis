package com.app.icarwis.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import static com.app.icarwis.utility.Config.APP_NAME;

/**
 * Created by j3p0n on 12/30/2016.
 */
public class Preferences {
	
	private static final String PREFS_NAME = APP_NAME;
	Context c;
	
	public Preferences(Context c) {
		this.c = c;
	}
	
	public void savePreferences(String key, int value) {
		SharedPreferences settings = c.getSharedPreferences(PREFS_NAME,
				Context.MODE_PRIVATE);
		settings.edit().putInt(key, value).apply();
	}

	public void savePreferences(String key, String value) {
		SharedPreferences settings = c.getSharedPreferences(PREFS_NAME,
				Context.MODE_PRIVATE);
		settings.edit().putString(key, value).apply();
	}
	
	public void savePreferences(String key, boolean value) {
		SharedPreferences settings = c.getSharedPreferences(PREFS_NAME,
				Context.MODE_PRIVATE);
		settings.edit().putBoolean(key, value).apply();
	}
	
	public int getPreferencesInt(String key) {
		SharedPreferences settings = c.getSharedPreferences(PREFS_NAME,
				Context.MODE_PRIVATE);
		return settings.getInt(key, Integer.MIN_VALUE);
	}

	public String getPreferencesString(String key) {
		SharedPreferences settings = c.getSharedPreferences(PREFS_NAME,
				Context.MODE_PRIVATE);
		return settings.getString(key, null);
	}
	
	public boolean getPreferencesBoolean(String key) {
		SharedPreferences settings = c.getSharedPreferences(PREFS_NAME,
				Context.MODE_PRIVATE);
		return settings.getBoolean(key, false);
	}
	
	public void clearAllPreferences() {
		SharedPreferences settings = c.getSharedPreferences(PREFS_NAME,
				Context.MODE_PRIVATE);
		settings.edit().clear().apply();
	}

	public void addWisataCartPreferences(String id) {
		ArrayList<String> wisataCartPreferences = getWisataCartPreferences();
		int countWisataCartPreferences = wisataCartPreferences.size();
		boolean isExist = false;
		for (int i = 0; i < countWisataCartPreferences; i++) {
			if (wisataCartPreferences.get(i).equals(id)) {
				isExist = true;
				break;
			}
		}
		if (!isExist){
			wisataCartPreferences.add(id);
			savePreferences(Config.PREFS_WISATA_CART, new Gson().toJson(wisataCartPreferences));
		}
	}

	public void removeWisataCartPreference(String id) {
		ArrayList<String> wisataCartPreferences = getWisataCartPreferences();
		int countWisataCartPreferences = wisataCartPreferences.size();
		for (int i = 0; i < countWisataCartPreferences; i++) {
			if (wisataCartPreferences.get(i).equals(id)) {
				wisataCartPreferences.remove(i);
				savePreferences(Config.PREFS_WISATA_CART, new Gson().toJson(wisataCartPreferences));
				break;
			}
		}
	}

	public ArrayList<String> getWisataCartPreferences() {
		ArrayList<String> wisataCartPreferences = new ArrayList<>();
		String dmPreference = getPreferencesString(Config.PREFS_WISATA_CART);
		if (!TextUtils.isEmpty(dmPreference)) {
			Type mapType = new TypeToken<ArrayList<String>>() {
			}.getType();
			wisataCartPreferences = new Gson().fromJson(dmPreference, mapType);
		}
		return wisataCartPreferences;
	}

	public boolean wisataCartPreferencesExist(String dmId){
		ArrayList<String> wisataCartPreferences = getWisataCartPreferences();
		int countWisataCartPreferences = wisataCartPreferences.size();
		for (int i = 0; i < countWisataCartPreferences; i++) {
			if (wisataCartPreferences.get(i).equals(dmId)) {
				return true;
			}
		}
		return false;
	}
	

}