package com.app.icarwis.modules.content.list;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XContentListPresenter extends MvpPresenter<XContentListView> {
    void loadData(boolean pullToRefresh);

    void loadDataNextPage(int page);
}