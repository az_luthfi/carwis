package com.app.icarwis.modules.order.wisata.wisata;

import android.content.Context;

import com.app.icarwis.R;
import com.app.icarwis.app.App;
import com.app.icarwis.base.BaseRxLcePresenter;
import com.app.icarwis.models.eventbus.EventWisataCart;
import com.app.icarwis.models.wisata.RequestWisata;
import com.app.icarwis.utility.Config;
import com.app.icarwis.utility.Logs;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class WisataPresenter extends BaseRxLcePresenter<XWisataView, RequestWisata>
        implements XWisataPresenter {
    private static final String TAG = WisataPresenter.class.getSimpleName();
    public WisataPresenter(Context context) {
        super(context);
    }

    @Override public void loadData(boolean pullToRefresh, HashMap<String, String> params, int page, String querySearch, String idProvince, String idCity) {
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(Config.CUSTOMER_ID)));
        params.put("page", String.valueOf(page));
        params.put("act", "list");
        params.put("search", querySearch);
        params.put("idProvince", idProvince);
        params.put("idCity", idCity);

        Observable<RequestWisata> observable = App.getService().apiWisata(params);
        if (page == 1) {
            subscribe(observable, pullToRefresh);
        } else {
            if (isViewAttached()) {
                getView().showLoadingNextPage(true);
            }
            Observer<RequestWisata> observer1 = new Observer<RequestWisata>() {
                @Override public void onSubscribe(Disposable d) {

                }

                @Override public void onNext(RequestWisata data) {
                    if (isViewAttached()) {
                        getView().showLoadingNextPage(false);
                        getView().setNextData(data);
                    }
                }

                @Override public void onError(Throwable e) {
                    if (isViewAttached()) {
                        getView().showErrorNextPage(context.getString(R.string.error_network));
                    }
                }

                @Override public void onComplete() {

                }
            };

            observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(observer1);
        }
    }

    @Override public void hitungDirection(HashMap<String, String> params) {
        params.put("act", "direction");
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(Config.CUSTOMER_ID)));
        Logs.d(TAG, "hitungDirection ==> params => " + params.toString());
        if (isViewAttached()) {
            getView().showProgressDialog(true);
        }

        Observer<RequestWisata> observer = new Observer<RequestWisata>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestWisata data) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().onNextDirection(data);
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().showError(context.getString(R.string.error_network_light));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiWisata(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventWisataCart(EventWisataCart event) {
        if (isViewAttached()){
            if (event.getType() == EventWisataCart.TYPE_REMOVE){
                getView().updateDataAdapter();
            }

        }
    }

    @Override public void attachView(XWisataView view) {
        super.attachView(view);
        EventBus.getDefault().register(this);
    }

    @Override public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        EventBus.getDefault().unregister(this);

    }
}