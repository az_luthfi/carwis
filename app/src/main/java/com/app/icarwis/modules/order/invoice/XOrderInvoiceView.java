package com.app.icarwis.modules.order.invoice;

import com.app.icarwis.models.transaction.RequestTransaction;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XOrderInvoiceView extends MvpLceView<RequestTransaction> {

    void showProgressDialog(boolean show);

    void onNextBooking(RequestTransaction data);

    void showError(String string);
}