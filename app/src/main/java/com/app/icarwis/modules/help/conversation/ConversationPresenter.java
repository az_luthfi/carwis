package com.app.icarwis.modules.help.conversation;

import android.Manifest;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;


import com.app.icarwis.R;
import com.app.icarwis.app.App;
import com.app.icarwis.base.BaseRxLcePresenter;
import com.app.icarwis.models.eventbus.EventMessage;
import com.app.icarwis.models.pesan.RequestMessage;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ConversationPresenter extends BaseRxLcePresenter<XConversationView, RequestMessage>
        implements XConversationPresenter {

    private RxPermissions rxPermissions;

    public ConversationPresenter(Context context, FragmentActivity activity) {
        super(context);
        rxPermissions = new RxPermissions(activity);
    }

    @Override public void loadMessage(String messageId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "conversation");
        params.put("pesanId", messageId);
        params.put("page", "1");

        subscribe(App.getService().apiMessageRequest(params), false);
    }

    @Override public void loadDataNextPage(int page, String messageId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "conversation");
        params.put("pesanId", messageId);
        params.put("page", String.valueOf(page));

        if (isViewAttached()) {
            getView().showLoadingNextPage(true);
        }

        Observer<RequestMessage> observer1 = new Observer<RequestMessage>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestMessage data) {
                if (isViewAttached()) {
                    getView().setNextData(data);
                    getView().showLoadingNextPage(false);
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showErrorNextPage(context.getString(R.string.error_network));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiMessageRequest(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer1);
    }

    @Override public void requestPermissionGallery() {
        rxPermissions
                .request(Manifest.permission.READ_EXTERNAL_STORAGE)
                .subscribe(new Observer<Boolean>() {
                    @Override public void onSubscribe(Disposable d) {
                    }

                    @Override public void onNext(Boolean aBoolean) {
                        if (isViewAttached()) {
                            if (aBoolean) {
                                getView().imageFromGallery();
                            } else {
                                Toast.makeText(context, "Permission denied, can't open the gallery", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override public void onError(Throwable e) {
                    }

                    @Override public void onComplete() {
                    }
                });
    }

    @Override public void requestSend(String messageId, String text, MultipartBody.Part requestImage, String lastId) {
        if (isViewAttached()) {
            getView().showProgressSend(true);
        }

        Map<String, RequestBody> params = new HashMap<>();
        params.put("act", RequestBody.create(MediaType.parse("multipart/form-data"), "replay"));
        params.put("pesanId", RequestBody.create(MediaType.parse("multipart/form-data"), messageId));
        params.put("isi", RequestBody.create(MediaType.parse("multipart/form-data"), text));
        params.put("lastId", RequestBody.create(MediaType.parse("multipart/form-data"), lastId));

        Observer<RequestMessage> observer2 = new Observer<RequestMessage>() {

            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestMessage m) {
                if (isViewAttached()) {
                    getView().setDataNewConversation(m);
                    getView().showProgressSend(false);
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    Toast.makeText(context, R.string.error_network_light, Toast.LENGTH_SHORT).show();
                    getView().showProgressSend(false);
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().sendMessage(params, requestImage)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer2);
    }

    @Override public void loadDataLast(String lastId, String messageId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "lastConversation");
        params.put("pesanId", messageId);
        params.put("lastId", lastId);

        Observer<RequestMessage> observer1 = new Observer<RequestMessage>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestMessage data) {
                if (isViewAttached()) {
                    getView().setDataFromNotification(data);
                }
            }

            @Override public void onError(Throwable e) {
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiMessageRequest(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer1);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(EventMessage event) {
        switch (event.getType()) {
            case EventMessage.NOTIFICATION:
                if (isViewAttached()) {
                    getView().onReplyFromCs(event.getMessage().getIdPesan());
                }
                break;
        }

    }

    @Override public void attachView(XConversationView view) {
        super.attachView(view);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        EventBus.getDefault().unregister(this);
    }
}