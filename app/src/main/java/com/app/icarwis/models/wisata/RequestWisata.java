package com.app.icarwis.models.wisata;

import com.app.icarwis.models.RequestBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Luthfi Aziz on 18/11/2017.
 */

public class RequestWisata extends RequestBase {
    @SerializedName("avg_km_hour")
    @Expose
    private String avgKmHour;
    @SerializedName("duration")
    @Expose
    private Duration duration;
    @SerializedName("distance")
    @Expose
    private Distance distance;
    @SerializedName("subtotal")
    @Expose
    private Double subtotal;
    @SerializedName("wisatas")
    @Expose
    private ArrayList<Wisata> wisatas = new ArrayList<>();
    @SerializedName("paket_wisatas")
    @Expose
    private ArrayList<PaketWisata> paketWisatas = new ArrayList<>();

    public String getAvgKmHour() {
        return avgKmHour;
    }

    public ArrayList<Wisata> getWisatas() {
        return wisatas;
    }

    public Duration getDuration() {
        return duration;
    }

    public Distance getDistance() {
        return distance;
    }

    public Double getSubtotal() {
        return subtotal;
    }

    public ArrayList<PaketWisata> getPaketWisatas() {
        return paketWisatas;
    }
}
