package com.app.icarwis.modules.order.form;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.app.icarwis.R;
import com.app.icarwis.base.BaseMvpFragment;
import com.app.icarwis.listeners.OnChangeToolbar;
import com.app.icarwis.models.eventbus.EventCoordinateBooking;
import com.app.icarwis.modules.order.carrental.carparent.CarParentFragmentBuilder;
import com.app.icarwis.modules.order.map.MapActivity;
import com.app.icarwis.modules.order.pemesan.PemesanFragment;
import com.app.icarwis.modules.order.pemesan.PemesanFragmentBuilder;
import com.app.icarwis.modules.order.wisata.wisata.WisataFragmentBuilder;
import com.app.icarwis.utility.CommonUtilities;
import com.app.icarwis.utility.Logs;
import com.app.icarwis.utility.MapMyLocation;
import com.app.icarwis.utility.Str;
import com.google.android.gms.maps.model.LatLng;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;

@FragmentWithArgs
public class FormBookingFragment extends BaseMvpFragment<XFormBookingView, FormBookingPresenter>
        implements XFormBookingView, OnChangeToolbar, MapMyLocation.OnAddressUpdateListener, MapMyLocation.LocationInterface {
    private static final String TAG = FormBookingFragment.class.getSimpleName();

    public static final String RENTCAR_WISATA = "rent_wisata";
    public static final String RENTCAR_MOBIL = "rent_mobil";
    public static final String PAKET_WISATA = "paket_wisata";

    @Arg String typeBooking;
    @Arg(required = false) String jsonPaketWisata;
    @BindView(R.id.tvPenjemputanAddress) TextView tvPenjemputanAddress;
    @BindView(R.id.layPenjemputanAddNotes) LinearLayout layPenjemputanAddNotes;
    @BindView(R.id.etPenjemputanNotes) MaterialEditText etPenjemputanNotes;
    @BindView(R.id.btnSetPenjemputan) Button btnSetPenjemputan;
    @BindView(R.id.tvKepulanganAddress) TextView tvKepulanganAddress;
    @BindView(R.id.layKepulanganAddNotes) LinearLayout layKepulanganAddNotes;
    @BindView(R.id.etKepulanganNotes) MaterialEditText etKepulanganNotes;
    @BindView(R.id.btnSetKepulangan) Button btnSetKepulangan;
    @BindView(R.id.tvDate) TextView tvDate;
    @BindView(R.id.tvTime) TextView tvTime;
    @BindView(R.id.bntSubmit) TextView bntSubmit;
    @BindView(R.id.ivJmlKursiAdultRemove) ImageView ivJmlKursiAdultRemove;
    @BindView(R.id.tvJmlKursiAdult) TextView tvJmlKursiAdult;
    @BindView(R.id.ivJmlKursiAdultAdd) ImageView ivJmlKursiAdultAdd;
    @BindView(R.id.ivJmlKursiChildRemove) ImageView ivJmlKursiChildRemove;
    @BindView(R.id.tvJmlKursiChild) TextView tvJmlKursiChild;
    @BindView(R.id.ivJmlKursiChildAdd) ImageView ivJmlKursiChildAdd;
    @BindView(R.id.layKepulangan) CardView layKepulangan;
    @BindView(R.id.tvAdultText) TextView tvAdultText;
    @BindView(R.id.layChild) LinearLayout layChild;
    @BindView(R.id.layForm) CardView layForm;

    private MapMyLocation mapMyLocation;
    private boolean gotoSettingLocation = false;
    private int minSeat = 1;
    private int maxSeat = 6;
    private int minHour = 3;
    private int maxDay = 6;
    private Calendar calendar;
    private Calendar minDate;
    private Calendar maxDate;
    private String dateNowString;
    private String timeNowString;
    private LatLng latLngJemput;
    private LatLng latLngPulang;
    private String addressJemput;
    private String addressPulang;

    public FormBookingFragment() {

    }

    @Override public FormBookingPresenter createPresenter() {
        return new FormBookingPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_form_booking;
    }

    @SuppressLint("SetTextI18n") @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        switch (typeBooking) {
            case RENTCAR_WISATA:
                layKepulangan.setVisibility(View.VISIBLE);
                layChild.setVisibility(View.VISIBLE);
                tvAdultText.setText("Dewasa (Umur 12+)");
                layForm.setVisibility(View.VISIBLE);
                break;
            case PAKET_WISATA:
                layKepulangan.setVisibility(View.GONE);
                layChild.setVisibility(View.GONE);
                layForm.setVisibility(View.GONE);
                break;
            case RENTCAR_MOBIL:
                layKepulangan.setVisibility(View.GONE);
                layChild.setVisibility(View.GONE);
                tvAdultText.setText("Jumlah Kursi");
                layForm.setVisibility(View.VISIBLE);
                break;
            default:
                mListener.back();
                break;
        }
        mapMyLocation = new MapMyLocation(getActivity(), 1);
        mapMyLocation.setOnAddressUpdateListener(this);
        mapMyLocation.setListener(this);
        mapMyLocation.setDisposableItem(mapMyLocation.getLocationWithAddress());
        mapMyLocation.getMyLocation();

        calendar = Calendar.getInstance();
        minDate = Calendar.getInstance();
        maxDate = Calendar.getInstance();
        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(Calendar.HOUR, 23);
        calendar1.set(Calendar.MINUTE, 59);
        long diff = calendar.getTimeInMillis() - calendar1.getTimeInMillis();
        // jam saat ini lebih besar
        Logs.d(TAG, "onViewCreated ==> diff time = " + (diff / (60 * 1000)));
        if ((diff / (60 * 1000)) >= (minHour * 60)) {
            minDate.add(Calendar.DAY_OF_MONTH, 1);
            maxDate.add(Calendar.DAY_OF_MONTH, 1);
        }
        maxDate.add(Calendar.DAY_OF_MONTH, maxDay);
        minDate.add(Calendar.HOUR, minHour);
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");
        dateNowString = sdfDate.format(minDate.getTime());
        timeNowString = sdfTime.format(minDate.getTime());

        tvDate.setText("00/00/0000");
        tvTime.setText("00:00");

        if (typeBooking.equals(RENTCAR_MOBIL)) {
            bntSubmit.setText(R.string.form_booking_btn_rent_car);
        } else if (typeBooking.equals(RENTCAR_WISATA)) {
            bntSubmit.setText(R.string.form_booking_btn_wisata);
        } else if (typeBooking.equals(PAKET_WISATA)) {
            bntSubmit.setText("LANJUTKAN");
        }
    }

    @OnClick({R.id.ivJmlKursiAdultRemove, R.id.ivJmlKursiAdultAdd, R.id.ivJmlKursiChildRemove, R.id.ivJmlKursiChildAdd}) public void onSeatClicked(View view) {
        Integer countSeat;
        switch (view.getId()) {
            case R.id.ivJmlKursiAdultRemove:
                countSeat = Integer.parseInt(tvJmlKursiAdult.getText().toString());
                if (countSeat > minSeat) {
                    tvJmlKursiAdult.setText(String.valueOf(countSeat - 1));
                }
                break;
            case R.id.ivJmlKursiAdultAdd:
                countSeat = Integer.parseInt(tvJmlKursiAdult.getText().toString()) + Integer.parseInt(tvJmlKursiChild.getText().toString());
                if (countSeat < maxSeat) {
                    tvJmlKursiAdult.setText(String.valueOf(Integer.parseInt(tvJmlKursiAdult.getText().toString()) + 1));
                } else {
                    showToast("Kapasitas maksimal penumpang satu mobil hanya 6 orang. Silahkan order kendaraan tambahan pada transaksi selanjutnya");
                }
                break;
            case R.id.ivJmlKursiChildRemove:
                countSeat = Integer.parseInt(tvJmlKursiChild.getText().toString());
                if (countSeat > 0) {
                    tvJmlKursiChild.setText(String.valueOf(countSeat - 1));
                }
                break;
            case R.id.ivJmlKursiChildAdd:
                countSeat = Integer.parseInt(tvJmlKursiAdult.getText().toString()) + Integer.parseInt(tvJmlKursiChild.getText().toString());
                if (countSeat < maxSeat) {
                    tvJmlKursiChild.setText(String.valueOf(Integer.parseInt(tvJmlKursiChild.getText().toString()) + 1));
                }else {
                    showToast("Kapasitas maksimal penumpang satu mobil hanya 6 orang. Silahkan order kendaraan tambahan pada transaksi selanjutnya");
                }
                break;
        }
    }

    @OnClick({R.id.layPenjemputanAddNotes, R.id.btnSetPenjemputan, R.id.layKepulanganAddNotes, R.id.btnSetKepulangan, R.id.tvDate, R.id.tvTime, R.id.bntSubmit}) public void onViewClicked(View view) {
        Intent intentMap;
        switch (view.getId()) {
            case R.id.layPenjemputanAddNotes:
                layPenjemputanAddNotes.setVisibility(View.GONE);
                etPenjemputanNotes.setVisibility(View.VISIBLE);
                etPenjemputanNotes.setText("");
                break;
            case R.id.btnSetPenjemputan:
                intentMap = new Intent(getContext(), MapActivity.class);
                if (latLngJemput != null && !String.valueOf(latLngJemput.latitude).equals("0.0")) {
                    Logs.d(TAG, "onViewClicked ==> latlang null");
                    intentMap.putExtra(MapActivity.EXTRA_LAT, latLngJemput.latitude);
                    intentMap.putExtra(MapActivity.EXTRA_LNG, latLngJemput.longitude);
                    intentMap.putExtra(MapActivity.EXTRA_ADDRESS, addressJemput);
                }
                intentMap.putExtra(MapActivity.EXTRA_TYPE, EventCoordinateBooking.TYPE_JEMPUT);
                startActivity(intentMap);
                break;
            case R.id.layKepulanganAddNotes:
                layKepulanganAddNotes.setVisibility(View.GONE);
                etKepulanganNotes.setVisibility(View.VISIBLE);
                etKepulanganNotes.setText("");
                break;
            case R.id.btnSetKepulangan:
                intentMap = new Intent(getContext(), MapActivity.class);
                if (latLngPulang != null && !String.valueOf(latLngPulang.latitude).equals("0.0")) {
                    intentMap.putExtra(MapActivity.EXTRA_LAT, latLngPulang.latitude);
                    intentMap.putExtra(MapActivity.EXTRA_LNG, latLngPulang.longitude);
                    intentMap.putExtra(MapActivity.EXTRA_ADDRESS, addressPulang);
                }
                intentMap.putExtra(MapActivity.EXTRA_TYPE, EventCoordinateBooking.TYPE_PULANG);
                startActivity(intentMap);
                break;
            case R.id.tvDate:
                if (!TextUtils.isEmpty(tvDate.getText())) {
                    Date date = CommonUtilities.getDateFromString(tvDate.getText().toString(), "dd/MM/yyyy");
                    if (date != null) {
                        calendar.setTime(date);
                    }
                }
                DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
                    @SuppressLint("DefaultLocale") @Override public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        tvDate.setText(new StringBuilder(String.format("%02d", dayOfMonth) + "/" + String.format("%02d", month + 1) + "/" + year));
                        if (tvDate.getText().toString().equals(dateNowString)) {
                            Date timePick = CommonUtilities.getDateFromString(tvTime.getText().toString(), "HH:mm");
                            Date timeMin = CommonUtilities.getDateFromString(timeNowString, "HH:mm");
                            long diff = timePick.getTime() - timeMin.getTime();
                            Logs.d(TAG, "onDateSet ==> diff => " + (diff / (60 * 1000)));
                            if ((diff / (60 * 1000)) < (minHour * 60)) {
                                tvTime.setText(timeNowString);
                                showToast("Minimal booking " + minHour + " jam dari sekarang");
                            }
                        }
                    }
                };
                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        getActivity(),
                        dateSetListener,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)
                );
                datePickerDialog.getDatePicker().setMinDate(minDate.getTimeInMillis());
                datePickerDialog.getDatePicker().setMaxDate(maxDate.getTimeInMillis());
                datePickerDialog.show();
                break;
            case R.id.tvTime:
                TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(),
                        new TimePickerDialog.OnTimeSetListener() {

                            @SuppressLint("DefaultLocale") @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                // jika hari ini dan waktu kurang dari minHour
                                if (tvDate.getText().toString().equals(dateNowString) &&
                                        (((hourOfDay * 60) + minute) <= ((minDate.get(Calendar.HOUR_OF_DAY) * 60) + minDate.get(Calendar.MINUTE)))) {
                                    tvTime.setText(timeNowString);
                                    showToast("Minimal booking " + minHour + " jam dari sekarang");
                                } else {
                                    tvTime.setText(new StringBuilder(String.format("%02d", hourOfDay)).append(":").append(String.format("%02d", minute)));
                                }
                            }
                        }, minDate.get(Calendar.HOUR_OF_DAY), minDate.get(Calendar.MINUTE), true);
                timePickerDialog.show();
                break;
            case R.id.bntSubmit:
                validationForm();
                break;
        }
    }

    private void validationForm() {
        HashMap<String, String> params = new HashMap<>();
        if (latLngJemput == null || String.valueOf(latLngJemput.latitude).equals("0.0")) {
            showToast("Anda belum mengatur tempat penjemputan");
            return;
        }
        if (!typeBooking.equals(PAKET_WISATA)) {
            if (TextUtils.isEmpty(tvDate.getText().toString())) {
                showToast("Tanggal berangkat belum dipilih");
                return;
            }
            if (TextUtils.isEmpty(tvTime.getText().toString())) {
                showToast("Jam berangkat belum dipilih");
                return;
            }
            if (tvDate.getText().toString().equals("00/00/0000")) {
                showToast("Tanggal berangkat belum dipilih");
                return;
            }
            if (tvTime.getText().toString().equals("00:00")) {
                showToast("Jam berangkat belum dipilih");
                return;
            }
        }

        params.put("departLat", String.valueOf(latLngJemput.latitude));
        params.put("departLong", String.valueOf(latLngJemput.longitude));
        params.put("departAddress", Str.getText(addressJemput, ""));
        params.put("departNote", Str.getText(etPenjemputanNotes.getText().toString(), ""));
        params.put("bookingDate", tvDate.getText().toString());
        params.put("bookingTime", tvTime.getText().toString());

        switch (typeBooking) {
            case RENTCAR_WISATA:
                if (latLngPulang == null || String.valueOf(latLngPulang.latitude).equals("0.0")) {
                    showToast("Anda belum mengatur tempat kepulangan");
                    return;
                }
                params.put("returnLat", String.valueOf(latLngPulang.latitude));
                params.put("returnLong", String.valueOf(latLngPulang.longitude));
                params.put("returnAddress", Str.getText(addressPulang, ""));
                params.put("returnNote", Str.getText(etKepulanganNotes.getText().toString(), ""));
                params.put("countAdult", Str.getText(tvJmlKursiAdult.getText().toString(), "1"));
                params.put("countChild", Str.getText(tvJmlKursiChild.getText().toString(), "0"));

                mListener.gotoPage(new WisataFragmentBuilder(params).build(), false, null);
                break;
            case PAKET_WISATA:
                mListener.gotoPage(new PemesanFragmentBuilder(params, PemesanFragment.TYPE_PAKET_WISATA).jsonPaketWisata(jsonPaketWisata).build(), false, null);
                break;
            case RENTCAR_MOBIL:
                params.put("countSeat", Str.getText(tvJmlKursiAdult.getText().toString(), "1"));
                mListener.gotoPage(new CarParentFragmentBuilder(params).build(), false, null);
                break;
        }
    }

    @Override public String getTitle() {
        switch (typeBooking) {
            case RENTCAR_MOBIL:
                return getString(R.string.item_menu_home_rental_mobil);
            case RENTCAR_WISATA:
                return getString(R.string.item_menu_home_rentcar_wisata);
            case PAKET_WISATA:
                return getString(R.string.item_menu_home_paket_wisata);
        }
        return null;
    }

    @Override public boolean isSearch() {
        return false;
    }

    @Override public boolean isCart() {
        return false;
    }

    @Override public void updateCoordinate(EventCoordinateBooking event) {
        switch (event.getType()) {
            case EventCoordinateBooking.TYPE_JEMPUT:
                latLngJemput = event.getLatLng();
                updateAddress(event.getAddress(), 0);
                break;
            case EventCoordinateBooking.TYPE_PULANG:
                latLngPulang = event.getLatLng();
                updateAddress(event.getAddress(), 1);
                break;
        }
    }


    private void updateAddress(String address, int type) {
        switch (type) {
            case 0:
                addressJemput = address;
                tvPenjemputanAddress.setVisibility(View.VISIBLE);
                tvPenjemputanAddress.setText(address);
                layPenjemputanAddNotes.setVisibility(View.VISIBLE);
                etPenjemputanNotes.setVisibility(View.GONE);
                etPenjemputanNotes.setText("");
                break;
            case 1:
                addressPulang = address;
                tvKepulanganAddress.setVisibility(View.VISIBLE);
                tvKepulanganAddress.setText(address);
                layKepulanganAddNotes.setVisibility(View.VISIBLE);
                etKepulanganNotes.setVisibility(View.GONE);
                etKepulanganNotes.setText("");
                break;
        }
    }

    @Override public void onAddressUpdate(Address address) {
        if (address != null && address.getMaxAddressLineIndex() > -1) {
            updateAddress(address.getAddressLine(0), 0);
            updateAddress(address.getAddressLine(0), 1);
        }

    }

    @Override public void onAddressError(Throwable throwable) {

    }

    @Override public void OnLocationUpdates(Location location) {
        latLngJemput = new LatLng(location.getLatitude(), location.getLongitude());
        latLngPulang = new LatLng(location.getLatitude(), location.getLongitude());
    }

    @Override public void OnLocationLast(Location location) {

    }

    @Override public void gotoLocationSettings() {

        startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 154);
        gotoSettingLocation = true;
    }

    @Override public void OnLocationError(String msg, int code) {

    }

    @Override public void onResume() {
        super.onResume();
        if (gotoSettingLocation && mapMyLocation != null) {
            mapMyLocation.getMyLocation();
            gotoSettingLocation = false;
        }
    }
}