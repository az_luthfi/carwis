package com.app.icarwis.base;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.app.icarwis.listeners.FragmentInteractionListener;
import com.app.icarwis.utility.CommonUtilities;
import com.hannesdorfmann.fragmentargs.FragmentArgs;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by j3p0n on 1/2/2017.
 */

public abstract class BaseFragment extends Fragment {

    protected FragmentInteractionListener mListener;
    private SweetAlertDialog pDialog;
    private Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentArgs.inject(this);
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
    }

    @LayoutRes protected abstract int getLayoutRes();

    @Nullable @Override public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutRes(), container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    /**
     * @param alertType    {@link SweetAlertDialog} Alert type Ex: SweetAlertDialog.WARNING_TYPE
     * @param title        Judul popup
     * @param content      Isi popup
     * @param confirmLabel Positive button label
     * @param cancelLabel  Negative button label
     * @param action       Callback action
     */
    public void showConfirmDialog(int alertType, String title, String content, String confirmLabel, String cancelLabel, final String action, boolean cancelable) {

        final SweetAlertDialog alert = CommonUtilities.buildAlert(getActivity(), alertType, title, content, confirmLabel, cancelLabel);
        alert.setCancelable(cancelable);
        alert.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.cancel();
                onAlertCancel(action);
            }
        });
        alert.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.cancel();
                onAlertConfirm(action);

            }
        });
    }

    public void showToast(String text) {
        Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
    }

    public void showSnackBar(String message) {
        final Snackbar snackbar = Snackbar.make(getView().getRootView(), message, Snackbar.LENGTH_LONG);
        snackbar.setAction("Tutup", new View.OnClickListener() {
            @Override public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }

    public void showErrorDialog(String message){
        showConfirmDialog(SweetAlertDialog.ERROR_TYPE, "Error!", message, "Tutup", null, "error_dialog", true);
    }

    public void showLoading(String content, boolean cancelable) {
        if (pDialog == null){
            pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        }
        pDialog.setTitleText(content);
        pDialog.setCancelable(cancelable);
        pDialog.show();
    }

    public void hideLoading() {
        if (pDialog != null) {
            pDialog.dismiss();
        }
    }

    public void onAlertConfirm(String action) {

    }

    public void onAlertCancel(String action) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentInteractionListener) {
            mListener = (FragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
