package com.app.icarwis.modules.order.invoice;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import java.util.HashMap;

public interface XOrderInvoicePresenter extends MvpPresenter<XOrderInvoiceView> {

    void requestBooking(HashMap<String, String> params);

    void loadPaymentMethod(boolean pullToRefresh, HashMap<String, String> params);
}