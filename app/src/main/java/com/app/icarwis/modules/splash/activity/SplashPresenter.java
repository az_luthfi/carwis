package com.app.icarwis.modules.splash.activity;

import android.content.Context;
import android.text.TextUtils;

import com.app.icarwis.R;
import com.app.icarwis.app.App;
import com.app.icarwis.base.BasePresenter;
import com.app.icarwis.models.customer.Customer;
import com.app.icarwis.models.customer.RequestCustomer;
import com.app.icarwis.utility.CommonUtilities;
import com.app.icarwis.utility.Logs;

import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.app.icarwis.utility.Config.CUSTOMER_AVATAR;
import static com.app.icarwis.utility.Config.CUSTOMER_EMAIL;
import static com.app.icarwis.utility.Config.CUSTOMER_ID;
import static com.app.icarwis.utility.Config.CUSTOMER_NAME;
import static com.app.icarwis.utility.Config.CUSTOMER_PHONE;
import static com.app.icarwis.utility.Config.CUSTOMER_PIN;
import static com.app.icarwis.utility.Config.CUSTOMER_REG_ID;
import static com.app.icarwis.utility.Config.CUSTOMER_SALDO;


/**
 * Created by Luthfi on 01/08/2017.
 */

public class SplashPresenter extends BasePresenter<XSplashView>
        implements XSplashPresenter {
    public static final String ACT_CHECK_VERSION = "checkVersion";
    public static final String ACT_IS_REGISTERED = "isRegister";

    public SplashPresenter(Context context) {
        super(context);
    }

    @Override public void saveRegId(String regId) {
        prefs.savePreferences(CUSTOMER_REG_ID, regId);
    }

    @Override public void checkIsLogin() {
        if (TextUtils.isEmpty(prefs.getPreferencesString(CUSTOMER_EMAIL))) {
            checkVersion();
        } else {
            checkIsAuth();
        }
    }

    @Override public void checkIsAuth() {
//        if (isViewAttached()){
//            getView().showContent();
//        }
        HashMap<String, String> params = new HashMap<>();
        params.put("act", ACT_IS_REGISTERED);
        params.put("regId", getRegId());
        params.put("email", prefs.getPreferencesString(CUSTOMER_EMAIL));
        params.put("handset", CommonUtilities.getDeviceName());
        params.put("app_version", String.valueOf(CommonUtilities.getAppVersion(context)));

        Logs.d("SplashPresenter => checkIsAuth ==> " +params.toString());

        Observer<RequestCustomer> observer = new Observer<RequestCustomer>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestCustomer requestCustomer) {
                if (isViewAttached()) {
                    switch (requestCustomer.getUserStatus()){
                        case "maintenance":
                            getView().showConfirmDialog(SweetAlertDialog.WARNING_TYPE, "Informasi", requestCustomer.getText(), "Tutup", null, requestCustomer.getUserStatus());
                            break;
                        case "update":
                            getView().showContent();
                            getView().showConfirmDialog(SweetAlertDialog.NORMAL_TYPE, "Informasi", requestCustomer.getText(), "Update", "Lain kali", "update");
                            getView().OnSuccessAuth(requestCustomer.getCustomer());
                            break;
                        case "new":
                        case "active":
                            getView().OnSuccessAuth(requestCustomer.getCustomer());
                            break;
                        case "block":
                        case "not-registered":
                            getView().showContent();
                            prefs.clearAllPreferences();
                            getView().showConfirmDialog(SweetAlertDialog.WARNING_TYPE, "Peringatan", requestCustomer.getText(), "Ok", null, requestCustomer.getUserStatus());
                            break;
                    }
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showConfirmDialog(SweetAlertDialog.WARNING_TYPE, "Peringatan", context.getString(R.string.error_network_popup), "Ulangi", "Tutup", "error" + ACT_IS_REGISTERED);
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiCustomer(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    @Override public void saveSession(Customer customer) {
        prefs.savePreferences(CUSTOMER_ID, customer.getCustomerId());
        prefs.savePreferences(CUSTOMER_EMAIL, customer.getCustomerEmail());
        prefs.savePreferences(CUSTOMER_NAME, customer.getCustomerName());
        prefs.savePreferences(CUSTOMER_PIN, customer.getCustomerPassword());
        prefs.savePreferences(CUSTOMER_SALDO, customer.getCustomerSaldo());
        prefs.savePreferences(CUSTOMER_PHONE, customer.getCustomerMsisdn());
        if (prefs.getPreferencesString(CUSTOMER_AVATAR) == null) {
            prefs.savePreferences(CUSTOMER_AVATAR, customer.getCustomerImage());
        }
    }

    private String getRegId() {
        return prefs.getPreferencesString(CUSTOMER_REG_ID) == null ? "" : prefs.getPreferencesString(CUSTOMER_REG_ID);
    }

    @Override public void checkVersion() {
//        if (isViewAttached()){
//            getView().showContent();
//        }
////        return;
        HashMap<String, String> params = new HashMap<>();
        params.put("act", ACT_CHECK_VERSION);
        params.put("app_version", String.valueOf(CommonUtilities.getAppVersion(context)));
        Logs.d("SplashPresenter => checkVerwsion ==> " +params.toString());

        Observer<RequestCustomer> observer = new Observer<RequestCustomer>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestCustomer requestCustomer) {
                if (isViewAttached()) {
                    if (requestCustomer.getStatus()) {
                        getView().showContent();
                        getView().showConfirmDialog(SweetAlertDialog.NORMAL_TYPE, "Informasi", requestCustomer.getText(), "Update", "Lain kali", "update");
                    } else {
                        getView().showContent();
                    }
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showConfirmDialog(SweetAlertDialog.WARNING_TYPE, "Peringatan", context.getString(R.string.error_network_popup), "Ulangi", "Tutup", "error" + ACT_CHECK_VERSION);
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiCustomer(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }


}
