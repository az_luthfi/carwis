package com.app.icarwis.modules.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.icarwis.R;
import com.app.icarwis.listeners.FragmentInteractionListener;
import com.app.icarwis.listeners.OnChangeToolbar;
import com.app.icarwis.models.DataNotification;
import com.app.icarwis.models.eventbus.EventWisataCart;
import com.app.icarwis.modules.booking.detail.DetailMyBookingFragment;
import com.app.icarwis.modules.booking.detail.DetailMyBookingFragmentBuilder;
import com.app.icarwis.modules.dashboard.DashboardFragment;
import com.app.icarwis.modules.dashboard.DashboardFragmentBuilder;
import com.app.icarwis.modules.order.wisata.wisata.WisataFragment;
import com.app.icarwis.utility.Config;
import com.app.icarwis.utility.FragmentStack;
import com.app.icarwis.utility.Preferences;
import com.google.gson.Gson;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity
        implements FragmentInteractionListener {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.ivToolbarImage) ImageView ivToolbarImage;
    @BindView(R.id.tvToolbarTitle) TextView tvToolbarTitle;
    @BindView(R.id.container) FrameLayout container;
    @BindView(R.id.appBar) AppBarLayout appBar;
    @BindView(R.id.laySearch) FrameLayout laySearch;
    @BindView(R.id.layAction) LinearLayout layAction;
    @BindView(R.id.search_view) MaterialSearchView searchView;
    @BindView(R.id.layCart) FrameLayout layCart;
    @BindView(R.id.tvCountCart) TextView tvCountCart;

    private FragmentStack fragmentStack;
    private ActionBar actionBar;
        private Preferences prefs;
//    private DataNotification dataNotification;

    private static final int TIME_DELAY = 2000;
    private static long back_pressed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        prefs = new Preferences(getApplicationContext());
        fragmentStack = new FragmentStack(this, getSupportFragmentManager(), R.id.container);
        fragmentStack.push(new DashboardFragmentBuilder(0).build(), null);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        setupToolbar();

        searchView.setVoiceSearch(false);
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.closeSearch();
                Fragment top = fragmentStack.peek();
                if (top instanceof MaterialSearchView.OnQueryTextListener) {
                    return ((MaterialSearchView.OnQueryTextListener) top).onQueryTextSubmit(query);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        ivToolbarImage.setVisibility(View.VISIBLE);
        tvToolbarTitle.setVisibility(View.GONE);
        laySearch.setVisibility(View.GONE);
        handleIntent(getIntent());
    }

    @Override protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (intent == null) {
            return;
        }
        if (intent.getExtras() == null) {
            return;
        }
        String jsonDataNotification = intent.getExtras().getString(Config.JSON_DATA_NOTIFICATION);
        if (jsonDataNotification == null) {
            return;
        }
        DataNotification dataNotification = new Gson().fromJson(jsonDataNotification, DataNotification.class);
        if (dataNotification == null) {
            return;
        }
        switch (dataNotification.getMethod().toLowerCase()) {
            case "booking":
                if (TextUtils.isEmpty(dataNotification.getDataId())) {
                    return;
                }
                gotoPage(new DetailMyBookingFragmentBuilder(dataNotification.getDataId()).build(), false, DetailMyBookingFragment.class.getSimpleName() + dataNotification.getDataId());
                break;
        }
    }

    private void setupToolbar() {
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
    }

    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else {
            if (fragmentStack.size() > 1) {
                fragmentStack.back();
                toggleShowUpNavigation();
            } else {

                if (!fragmentStack.back()) {
                    if (back_pressed + TIME_DELAY > System.currentTimeMillis()) {
                        finish();
                    } else {
                        Toast.makeText(getBaseContext(), "Tekan lagi untuk keluar",
                                Toast.LENGTH_SHORT).show();
                    }
                    back_pressed = System.currentTimeMillis();
                }
            }


        }
    }

    private void toggleShowUpNavigation() {
        Fragment top = fragmentStack.peek();
        String toolbarTitle = null;
        if (top instanceof DashboardFragment) {
            ivToolbarImage.setVisibility(View.VISIBLE);
            tvToolbarTitle.setVisibility(View.GONE);
            laySearch.setVisibility(View.GONE);
            layCart.setVisibility(View.GONE);
        } else {
            if (top instanceof OnChangeToolbar) {
                toolbarTitle = ((OnChangeToolbar) top).getTitle();
            }
            if (toolbarTitle == null) {
                ivToolbarImage.setVisibility(View.VISIBLE);
                tvToolbarTitle.setVisibility(View.GONE);
            } else {
                ivToolbarImage.setVisibility(View.GONE);
                tvToolbarTitle.setVisibility(View.VISIBLE);
                tvToolbarTitle.setText(toolbarTitle);
            }
            if (top instanceof OnChangeToolbar) {
                if (((OnChangeToolbar) top).isSearch()) {
                    laySearch.setVisibility(View.VISIBLE);
                } else {
                    laySearch.setVisibility(View.GONE);
                }
                if (((OnChangeToolbar) top).isCart()) {
                    layCart.setVisibility(View.VISIBLE);
                } else {
                    layCart.setVisibility(View.GONE);
                }
            } else {
                laySearch.setVisibility(View.GONE);
                layCart.setVisibility(View.GONE);
            }
        }
        if (fragmentStack.size() > 1) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        } else {
            setupToolbar();
        }
    }

    @Override public void gotoPage(Fragment page, boolean shouldReplace, String tagName) {
        if (shouldReplace) {
            fragmentStack.push(page, tagName);
        } else {
            fragmentStack.pushAdd(page, tagName);
        }
        toggleShowUpNavigation();
    }

    @Override public void back() {
        onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick({R.id.layCart, R.id.laySearch}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layCart:
                Fragment fragment = fragmentStack.peek();
                if (fragment instanceof WisataFragment){
                    ((WisataFragment) fragment).onViewClicked();
                }
                break;
            case R.id.laySearch:
                searchView.showSearch();
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventWisataCart(EventWisataCart event) {
        int countWisataCart = prefs.getWisataCartPreferences().size();
        if (countWisataCart > 0){
            tvCountCart.setVisibility(View.VISIBLE);
            tvCountCart.setText(String.valueOf(countWisataCart));
        }else{
            tvCountCart.setVisibility(View.GONE);
            tvCountCart.setText("0");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }
}
