package com.app.icarwis.modules.account.balance;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.icarwis.R;
import com.app.icarwis.base.BaseMvpFragment;
import com.app.icarwis.modules.deposit.add.DepositFragment;
import com.app.icarwis.utility.CommonUtilities;

import butterknife.BindView;
import butterknife.OnClick;

import static com.app.icarwis.utility.Config.CUSTOMER_SALDO;

public class MyBalanceFragment extends BaseMvpFragment<XMyBalanceView, MyBalancePresenter>
        implements XMyBalanceView {

    @BindView(R.id.tvSaldo) TextView tvSaldo;
    @BindView(R.id.layTopUp) LinearLayout layTopUp;
    @BindView(R.id.layHistory) LinearLayout layHistory;

    public MyBalanceFragment() {

    }

    @Override public MyBalancePresenter createPresenter() {
        return new MyBalancePresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_my_balance;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvSaldo.setText(CommonUtilities.toRupiahNumberFormat(presenter.getPrefs().getPreferencesInt(CUSTOMER_SALDO)));
    }

    @OnClick({R.id.layTopUp, R.id.layHistory}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layTopUp:
                mListener.gotoPage(new DepositFragment(), false, null);
                break;
            case R.id.layHistory:
                break;
        }
    }
}