package ${packageName};

import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface X${className}View extends MvpLceView<${modelName}> {

}