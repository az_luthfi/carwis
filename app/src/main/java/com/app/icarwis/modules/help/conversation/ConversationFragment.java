package com.app.icarwis.modules.help.conversation;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.icarwis.R;
import com.app.icarwis.adapter.message.ConversationAdapter;
import com.app.icarwis.base.BaseMvpLceFragment;
import com.app.icarwis.holder.ItemLoadingHolder;
import com.app.icarwis.listeners.OnChangeToolbar;
import com.app.icarwis.models.pesan.RequestMessage;
import com.app.icarwis.modules.common.FragmentPhotoViewSingle;
import com.app.icarwis.modules.common.FragmentPhotoViewSingleBuilder;
import com.app.icarwis.utility.CommonUtilities;
import com.app.icarwis.utility.Logs;
import com.app.icarwis.utility.ProgressRequestBody;
import com.app.icarwis.utility.listview.EndlessRecyclerOnScrollListener;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;
import com.yalantis.ucrop.UCrop;
import com.yalantis.ucrop.util.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;

import static com.app.icarwis.utility.Config.MESSAGE_IMAGE_CROP_HEIGHT;
import static com.app.icarwis.utility.Config.MESSAGE_IMAGE_CROP_WIDTH;


@FragmentWithArgs
public class ConversationFragment extends BaseMvpLceFragment<RelativeLayout, RequestMessage, XConversationView, ConversationPresenter>
        implements XConversationView, ItemLoadingHolder.ItemViewLoadingListener, OnChangeToolbar {
    private static final String TAG = ConversationFragment.class.getSimpleName();
    private static final int CHOICE_IMAGE_FROM_GALLERY = 912;

    @Arg String messageId;
    @Arg String messageStatus;
    @BindView(R.id.etText) EditText etText;
    @BindView(R.id.layText) FrameLayout layText;
    @BindView(R.id.ivImageGallery) ImageView ivImageGallery;
    @BindView(R.id.layButtonSend) RelativeLayout layButtonSend;
    @BindView(R.id.progressSend) AVLoadingIndicatorView progressSend;
    @BindView(R.id.actContainer) RelativeLayout actContainer;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.contentView) RelativeLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.ivAttachmentImage) ImageView ivAttachmentImage;
    @BindView(R.id.tvAttachmentName) TextView tvAttachmentName;
    @BindView(R.id.layImagePreview) RelativeLayout layImagePreview;
    @BindView(R.id.layInput) LinearLayout layInput;

    private ConversationAdapter adapter;
    private EndlessRecyclerOnScrollListener scrollListener;
    private int maxCountItems = 0;
    private File fileImage;

    public ConversationFragment() {

    }

    @Override public ConversationPresenter createPresenter() {
        return new ConversationPresenter(getContext(), getActivity());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_pesan_conversation;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new ConversationAdapter(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, true);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

        scrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (!adapter.isLoading()) {
                    if (adapter.getItemCount() < maxCountItems) {

                        presenter.loadDataNextPage(page, messageId);
                    }
                }
            }
        };
        etText.setHorizontallyScrolling(false);
        etText.setMaxLines(3);

        recyclerView.addOnScrollListener(scrollListener);

        if (messageStatus.equalsIgnoreCase("closed")){
            layInput.setVisibility(View.GONE);
        }
        loadData(false);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return getContext().getString(R.string.error_network);
    }

    @Override public void setData(RequestMessage data) {
        if (data.getStatus()) {
            maxCountItems = data.getCount();
            adapter.setItems(data.getConversations());
        } else {
            showToast(data.getText());
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadMessage(messageId);
    }

    @OnClick({R.id.ivImageGallery, R.id.layButtonSend, R.id.ivAttachmentImage}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivImageGallery:
                presenter.requestPermissionGallery();
                break;
            case R.id.layButtonSend:
                if (!TextUtils.isEmpty(etText.getText())) {
                    presenter.requestSend(messageId, etText.getText().toString(), getRequestImage(), adapter.getLastId());
                }
                break;
            case R.id.ivAttachmentImage:
                FragmentPhotoViewSingle fragmentPhotoView = new FragmentPhotoViewSingleBuilder(fileImage.getPath()).build();
                fragmentPhotoView.setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogFragmentTheme);
                fragmentPhotoView.show(getFragmentManager(), "photo");
                break;
        }
    }

    @Override public void onRetryNextPage() {
        adapter.setErrorNextPage(null);
        presenter.loadDataNextPage(scrollListener.getCurrentPage(), messageId);
    }

    @Override public void showLoadingNextPage(boolean show) {
        adapter.setLoadingNextPage(show);
    }

    @Override public void showErrorNextPage(String msg) {
        showToast(msg);
        adapter.setErrorNextPage("Sentuh untuk mengulangi");
        scrollListener.backToPreviousPage();
    }

    @Override public void setNextData(RequestMessage data) {
        if (data.getStatus()) {
            adapter.setItems(data.getConversations());
        }
    }

    @Override public void imageFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "Pilih Gambar"), CHOICE_IMAGE_FROM_GALLERY);
    }

    @Override public void showProgressSend(boolean show) {
        etText.setEnabled(!show);
        layButtonSend.setClickable(!show);
        if (show) {
            layInput.setBackgroundColor(Color.parseColor("#967b7b7b"));
            progressSend.smoothToShow();
        } else {
            layInput.setBackgroundColor(0x00000000);
            progressSend.smoothToHide();
        }
    }

    @Override public void setDataNewConversation(RequestMessage m) {
        if (m.getStatus()) {
            adapter.addNewItems(m.getConversations());
            etText.setText("");
            CommonUtilities.hideSoftKeyboard(getActivity());
            maxCountItems += m.getConversations().size();
            recyclerView.smoothScrollToPosition(0);
            ivAttachmentImage.setVisibility(View.GONE);
            tvAttachmentName.setVisibility(View.GONE);
            tvAttachmentName.setText("");
            fileImage = null;
        } else {
            showToast(m.getText());
        }
    }

    @Override public void onReplyFromCs(String idPesan) {
        if (!TextUtils.isEmpty(idPesan) && idPesan.equals(messageId)){
            presenter.loadDataLast(adapter.getLastId(), messageId);
        }
    }

    @Override public void setDataFromNotification(RequestMessage data) {
        if (data.getStatus()){
            adapter.addNewItems(data.getConversations());
            maxCountItems += data.getConversations().size();
        }
    }

    @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CHOICE_IMAGE_FROM_GALLERY) {
                Uri selectedUri = data.getData();
                if (selectedUri != null) {
                    startCrop(selectedUri);
                } else {
                    showToast("Tidak dapat mengambil gambar");
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                onSelectImage(UCrop.getOutput(data));
            }
        }
        if (resultCode == UCrop.RESULT_ERROR) {
            handleCropError(data);
        }

    }

    private void startCrop(Uri uri) {
        UCrop.of(uri, Uri.fromFile(new File(CommonUtilities.getTargetImage())))
                .withMaxResultSize(MESSAGE_IMAGE_CROP_WIDTH, MESSAGE_IMAGE_CROP_HEIGHT)
                .start(getContext(), ConversationFragment.this);
    }

    private void onSelectImage(Uri selectedImage) {
        if (selectedImage != null) {
            String filePath = FileUtils.getPath(getContext(), selectedImage);
            fileImage = new File(filePath);
            Logs.d(TAG, "imge size : " + CommonUtilities.size((double) fileImage.length()));
            ivAttachmentImage.setVisibility(View.VISIBLE);
            tvAttachmentName.setVisibility(View.VISIBLE);
            tvAttachmentName.setText(fileImage.getPath());
            Picasso.with(getContext()).load(fileImage).resize(200, 0).into(ivAttachmentImage);
        } else {
            Toast.makeText(getContext(), R.string.error_pick_image, Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Toast.makeText(getContext(), cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getContext(), "Gagal memotong gambar", Toast.LENGTH_SHORT).show();
        }
    }

    public MultipartBody.Part getRequestImage() {
        if (fileImage != null) {
            // create RequestBody instance from file
            ProgressRequestBody requestFile = null;
            try {
                requestFile = new ProgressRequestBody(fileImage, MediaType.parse("multipart/form-data"), new ProgressRequestBody.UploadCallbacks() {
                    @Override public void onProgressUpdate(long uploaded, long lenght, int percentage) {

                    }
                });
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            // MultipartBody.Part is used to send also the actual file name
            if (requestFile != null) {
                return MultipartBody.Part.createFormData("image", fileImage.getName(), requestFile);
            } else {
                return null;
            }
        }
        return null;
    }

    @Nullable @Override public String getTitle() {
        return "Conversation";
    }

    @Override public boolean isSearch() {
        return false;
    }

    @Override public boolean isCart() {
        return false;
    }
}