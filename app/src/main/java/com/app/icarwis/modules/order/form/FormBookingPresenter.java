package com.app.icarwis.modules.order.form;

import android.content.Context;

import com.app.icarwis.base.BasePresenter;
import com.app.icarwis.models.eventbus.EventCoordinateBooking;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class FormBookingPresenter extends BasePresenter<XFormBookingView>
        implements XFormBookingPresenter {

    public FormBookingPresenter(Context context) {
        super(context);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCoordinateUpdateEvent(EventCoordinateBooking event) {
        if (isViewAttached()){
            getView().updateCoordinate(event);
        }
    }

    @Override public void attachView(XFormBookingView view) {
        super.attachView(view);
        EventBus.getDefault().register(this);
    }

    @Override public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        EventBus.getDefault().unregister(this);

    }
}