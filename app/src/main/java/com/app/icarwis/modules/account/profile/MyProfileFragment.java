package com.app.icarwis.modules.account.profile;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.icarwis.R;
import com.app.icarwis.base.BaseFragment;
import com.app.icarwis.models.eventbus.EventUpdateProfile;
import com.app.icarwis.modules.account.balance.MyBalanceFragment;
import com.app.icarwis.modules.account.editprofile.EditAccountFragment;
import com.app.icarwis.modules.content.list.ContentListFragment;
import com.app.icarwis.modules.content.page.StaticPageFragmentBuilder;
import com.app.icarwis.modules.dashboard.DashboardFragment;
import com.app.icarwis.modules.splash.activity.SplashActivity;
import com.app.icarwis.utility.CommonUtilities;
import com.app.icarwis.utility.Preferences;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.app.icarwis.utility.Config.APP_PREFIX;
import static com.app.icarwis.utility.Config.CUSTOMER_AVATAR;
import static com.app.icarwis.utility.Config.CUSTOMER_EMAIL;
import static com.app.icarwis.utility.Config.CUSTOMER_ID;
import static com.app.icarwis.utility.Config.CUSTOMER_NAME;
import static com.app.icarwis.utility.Config.CUSTOMER_PHONE;
import static com.app.icarwis.utility.Config.CUSTOMER_SALDO;
import static com.app.icarwis.utility.Config.ID_PREFIX;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyProfileFragment extends BaseFragment {


    @BindView(R.id.ivImage) ImageView ivImage;
    @BindView(R.id.tvAccountId) TextView tvAccountId;
    @BindView(R.id.tvName) TextView tvName;
    @BindView(R.id.tvPhone) TextView tvPhone;
    @BindView(R.id.tvEmail) TextView tvEmail;
    @BindView(R.id.layEditProfile) LinearLayout layEditProfile;
    @BindView(R.id.layHelp) LinearLayout layHelp;
    @BindView(R.id.layLogout) LinearLayout layLogout;
    @BindView(R.id.tvSaldo) TextView tvSaldo;

    private Preferences prefs;

    public MyProfileFragment() {
        // Required empty public constructor
    }


    @Override protected int getLayoutRes() {
        return R.layout.fragment_my_profile;
    }

    @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prefs = new Preferences(getContext());
        setupProfile();
    }

    private void setupProfile() {
        if (!TextUtils.isEmpty(prefs.getPreferencesString(CUSTOMER_AVATAR))) {
            Picasso.with(getContext()).load(prefs.getPreferencesString(CUSTOMER_AVATAR)).placeholder(R.drawable.person_default).into(ivImage);
        }
        String id = APP_PREFIX + String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)) + ID_PREFIX;
        tvAccountId.setText(id);
        tvName.setText(prefs.getPreferencesString(CUSTOMER_NAME));
        tvPhone.setText(prefs.getPreferencesString(CUSTOMER_PHONE));
        tvEmail.setText(prefs.getPreferencesString(CUSTOMER_EMAIL));
        tvSaldo.setText(CommonUtilities.toRupiahNumberFormat(prefs.getPreferencesInt(CUSTOMER_SALDO)));
    }

    @OnClick({R.id.ivImage, R.id.layEditProfile, R.id.layBalance,R.id.layAboutUs, R.id.layHelp, R.id.layInfo, R.id.layLogout, R.id.layIcarJoin}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivImage:
                break;
            case R.id.layEditProfile:
                mListener.gotoPage(new EditAccountFragment(), false, null);
                break;
            case R.id.layBalance:
                mListener.gotoPage(new MyBalanceFragment(), false, null);
                break;
            case R.id.layAboutUs:
                mListener.gotoPage(new StaticPageFragmentBuilder("Tentang-Kami").build(), false, null);
                break;
            case R.id.layHelp:
                if (((DashboardFragment) getParentFragment()) != null) {
                    ((DashboardFragment) getParentFragment()).setCurrentItem(2);
                }
                break;
            case R.id.layInfo:
                mListener.gotoPage(new ContentListFragment(), false, null);
                break;
            case R.id.layLogout:
                showConfirmDialog(SweetAlertDialog.NORMAL_TYPE, "Konfirmasi", "Apakah anda yakin akan logout?", "Ya", "Tidak", "logout", true);
                break;
            case R.id.layIcarJoin:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://icarindo.com/customer"));
                startActivity(browserIntent);
                break;
        }
    }

    @Override public void onAlertConfirm(String action) {
        switch (action) {
            case "logout":
                prefs.clearAllPreferences();
                Intent intent = new Intent(getActivity(), SplashActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                getActivity().finish();
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onProfileUpdateEvent(EventUpdateProfile event) {
        setupProfile();
    }

    @Override public void onAttach(Context context) {
        super.onAttach(context);
        EventBus.getDefault().register(this);
    }

    @Override public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }
}
