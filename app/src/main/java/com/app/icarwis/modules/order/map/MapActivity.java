package com.app.icarwis.modules.order.map;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.icarwis.R;
import com.app.icarwis.models.eventbus.EventCoordinateBooking;
import com.app.icarwis.utility.Logs;
import com.app.icarwis.utility.MapMyLocation;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, MapMyLocation.LocationInterface, MapMyLocation.OnAddressUpdateListener {
    private static final String TAG = MapActivity.class.getSimpleName();
    public static final String EXTRA_LAT = "extra_lat";
    public static final String EXTRA_LNG = "extra_lng";
    public static final String EXTRA_ADDRESS = "extra_address";
    public static final String EXTRA_TYPE = "extra_type";
    private final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 12317;

    @BindView(R.id.tvAddress) TextView tvAddress;
    @BindView(R.id.layAddress) CardView layAddress;
    @BindView(R.id.mapView) MapView mapView;
    @BindView(R.id.btnSubmit) TextView btnSubmit;
    @BindView(R.id.tvChose) TextView tvChose;
    @BindView(R.id.tvToolbarTitle) TextView tvToolbarTitle;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.appBar) AppBarLayout appBar;
    @BindView(R.id.rlAddress) LinearLayout rlAddress;

    private MapMyLocation mapMyLocation;
    private boolean gotoSettingLocation = false;
    private GoogleMap map;
    private LatLng currentLatLng;
    private String currentAddress = "";
    private String typeBooking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        ButterKnife.bind(this);
        if (getIntent().getExtras() != null) {
            if (!Double.isNaN(getIntent().getExtras().getDouble(EXTRA_LAT, Double.NaN))) {
                Logs.d(TAG, "onCreate ==> lat lang not null => " + getIntent().getExtras().getDouble(EXTRA_LAT));
                currentLatLng = new LatLng(getIntent().getExtras().getDouble(EXTRA_LAT), getIntent().getExtras().getDouble(EXTRA_LNG));
            }
            if (getIntent().getExtras().getString(EXTRA_ADDRESS) != null) {
                currentAddress = getIntent().getExtras().getString(EXTRA_ADDRESS);
            }
        }
        typeBooking = getIntent().getExtras().getString(EXTRA_TYPE, EventCoordinateBooking.TYPE_JEMPUT);
        switch (typeBooking) {
            case EventCoordinateBooking.TYPE_JEMPUT:
                tvChose.setText("Penjemputan");
                btnSubmit.setText("Atur Tempat Penjemputan");
                break;
            case EventCoordinateBooking.TYPE_PULANG:
                tvChose.setText("Kepulangan");
                btnSubmit.setText("Atur Tempat Kepulangan");
                break;
        }
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        mapMyLocation = new MapMyLocation(this, 1);
        mapMyLocation.setOnAddressUpdateListener(this);
    }

    @OnClick({R.id.tvChose, R.id.btnSubmit, R.id.rlAddress}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvChose:
            case R.id.btnSubmit:
                if (currentLatLng != null) {
                    EventBus.getDefault().post(new EventCoordinateBooking(currentLatLng, currentAddress, typeBooking));
                }
                finish();
                break;
            case R.id.rlAddress:
                findPlace();
                break;
        }
    }

    @Override public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
        MapsInitializer.initialize(this);

        mapMyLocation.setDisposableItem(mapMyLocation.getLocationWithoutAddress());
        mapMyLocation.setListener(this);
        mapMyLocation.getMyLocation();
    }

    @Override public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override public void OnLocationUpdates(Location location) {
        if (map != null && mapView != null) {
            if (!map.isMyLocationEnabled()) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                map.setMyLocationEnabled(true);

            }

            // Updates the location and zoom of the MapView
            if (currentLatLng != null && !Double.isNaN(currentLatLng.latitude)) {
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 17));
            } else {
                Logs.d(TAG, "OnLocationUpdates ==> current LatLang null");
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 17));
            }

            map.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                @Override public void onCameraIdle() {
                    new Handler().postDelayed(new Runnable() {
                        @Override public void run() {
                            currentLatLng = map.getCameraPosition().target;
                            mapMyLocation.getAddressByLoc(currentLatLng.latitude, currentLatLng.longitude);
                        }
                    }, 200);


                }
            });

        }
    }

    @Override public void OnLocationLast(Location location) {

    }

    @Override public void gotoLocationSettings() {
        startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 154);
        gotoSettingLocation = true;
    }

    @Override public void OnLocationError(String msg, int code) {

    }

    @Override public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (mapView != null) {
            mapView.onSaveInstanceState(bundle);
        }
    }

    @Override public void onDestroy() {
        super.onDestroy();
        if (mapMyLocation != null) {
            mapMyLocation.unregister();
        }
        if (mapView != null) {
            mapView.onDestroy();
        }
    }

    @Override public void onResume() {
        super.onResume();
        if (mapView != null) {
            mapView.onResume();
        }
        if (gotoSettingLocation && mapMyLocation != null) {
            mapMyLocation.getMyLocation();
            gotoSettingLocation = false;
        }
    }

    @Override public void onPause() {
        super.onPause();
        if (mapView != null) {
            mapView.onPause();
        }
    }

    @Override public void onLowMemory() {
        super.onLowMemory();
        if (mapView != null) {
            mapView.onLowMemory();
        }
    }

    @Override public void onAddressUpdate(Address address) {
        currentAddress = address.getAddressLine(0);
        tvAddress.setText(currentAddress);

    }

    @Override public void onAddressError(Throwable throwable) {
        currentAddress = "";
        tvAddress.setText(currentAddress);
    }

    private void findPlace() {
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            Log.e("find place error === ", "ERROR repairable");
        } catch (GooglePlayServicesNotAvailableException e) {
            Log.e("find place error === ", "ERROR not available");
        }
    }

    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logs.d(TAG, "onActivityResult " + String.valueOf(requestCode));
        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {

                Place place = PlaceAutocomplete.getPlace(this, data);
                currentLatLng = place.getLatLng();

                map.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 17));
            }
        } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
            Status status = PlaceAutocomplete.getStatus(this, data);
            Log.e("PlACE ERROR == ", status.getStatusMessage());
        }
    }
}
