package com.app.icarwis.modules.order.paketwisata.detail;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XPaketWisataDetailPresenter extends MvpPresenter<XPaketWisataDetailView> {

}