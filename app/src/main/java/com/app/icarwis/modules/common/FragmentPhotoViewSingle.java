package com.app.icarwis.modules.common;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.app.icarwis.R;
import com.github.chrisbanes.photoview.PhotoView;
import com.hannesdorfmann.fragmentargs.FragmentArgs;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
@FragmentWithArgs
public class FragmentPhotoViewSingle extends DialogFragment {

    @Arg String path;
    @BindView(R.id.ivImage)
    PhotoView ivImage;
    @BindView(R.id.ivBack) ImageView ivBack;

    private Unbinder unbinder;

    public FragmentPhotoViewSingle() {
        // Required empty public constructor
    }

    @Override public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentArgs.inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_photo_view, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (path != null) {
            if (!path.contains("http")) {
                File ivFile = new File(path);
                Picasso.with(getContext())
                        .load(ivFile)
                        .into(ivImage);
            } else {

                Picasso.with(getContext())
                        .load(path)
                        .into(ivImage);
            }
        } else {
            Toast.makeText(getContext(), "Url tidak valid", Toast.LENGTH_SHORT).show();
        }

        return view;
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.ivBack) public void onViewClicked() {
        getDialog().dismiss();
    }
}
