package com.app.icarwis.modules.booking.detail;

import android.content.Context;
import android.os.Handler;

import com.app.icarwis.app.App;
import com.app.icarwis.base.BaseRxLcePresenter;
import com.app.icarwis.models.transaction.RequestTransaction;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DetailMyBookingPresenter extends BaseRxLcePresenter<XDetailMyBookingView, RequestTransaction>
        implements XDetailMyBookingPresenter {

    public DetailMyBookingPresenter(Context context) {
        super(context);
    }

    @Override public void loadData(boolean pullToRefresh, String bookingCode) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "detail");
        params.put("bookingCode", bookingCode);

        subscribe(App.getService().apiTransaction(params), pullToRefresh);
    }

    @Override public void updateTransaction(final String bookingCode, final String status, final String desc) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "update_status");
        params.put("bookingCode", bookingCode);
        params.put("desc", desc);

        if (isViewAttached()) {
            getView().shoProgressDialog(true);
        }

        Observer<RequestTransaction> observer1 = new Observer<RequestTransaction>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestTransaction data) {
                new Handler().postDelayed(new Runnable() {
                    @Override public void run() {
                        if (isViewAttached()) {
                            getView().shoProgressDialog(false);
                            loadData(false, bookingCode);
                        }
                    }
                }, 200);

            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    updateTransaction(bookingCode, status, desc);
                }
            }

            @Override public void onComplete() {

            }
        };
        App.getService().apiTransaction(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer1);
    }
}