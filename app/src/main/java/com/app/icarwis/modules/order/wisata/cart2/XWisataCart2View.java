package com.app.icarwis.modules.order.wisata.cart2;

import com.app.icarwis.models.wisata.RequestWisata;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XWisataCart2View extends MvpLceView<RequestWisata> {

    void setEmptyCart(String text);

    void showProgressDialog(boolean show);

    void onNextDirection(RequestWisata data);

    void showError(String string);
}