package com.app.icarwis.modules.home;

import android.content.Context;

import com.app.icarwis.app.App;
import com.app.icarwis.base.BaseRxLcePresenter;
import com.app.icarwis.models.berita.RequestContent;

import java.util.HashMap;

public class HomePresenter extends BaseRxLcePresenter<XHomeView, RequestContent>
        implements XHomePresenter {

    public HomePresenter(Context context) {
        super(context);
    }

    @Override public void loadData(boolean pullToRefresh) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "home");

        subscribe(App.getService().apiContentRequest(params), pullToRefresh);
    }
}