package com.app.icarwis.models.transaction;

import com.app.icarwis.models.RequestBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Luthfi Aziz on 29/11/2017.
 */

public class RequestTransaction extends RequestBase {
    @SerializedName("payment_groups")
    @Expose
    private ArrayList<PaymentGroup> paymentGroups = new ArrayList<>();
    @SerializedName("transactions")
    @Expose
    private ArrayList<Transaction> transactions = new ArrayList<>();
    @SerializedName("transaction")
    @Expose
    private Transaction transaction;
    @SerializedName("bookingCode")
    @Expose
    private String bookingCode;

    public ArrayList<Transaction> getTransactions() {
        return transactions;
    }

    public ArrayList<PaymentGroup> getPaymentGroups() {
        return paymentGroups;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public String getBookingCode() {
        return bookingCode;
    }
}
