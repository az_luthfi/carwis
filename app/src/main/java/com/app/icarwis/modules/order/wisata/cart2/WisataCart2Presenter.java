package com.app.icarwis.modules.order.wisata.cart2;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import com.app.icarwis.R;
import com.app.icarwis.app.App;
import com.app.icarwis.base.BaseRxLcePresenter;
import com.app.icarwis.models.wisata.RequestWisata;
import com.app.icarwis.utility.Config;

import java.util.ArrayList;
import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class WisataCart2Presenter extends BaseRxLcePresenter<XWisataCart2View, RequestWisata>
        implements XWisataCart2Presenter {

    public WisataCart2Presenter(Context context) {
        super(context);
    }

    @Override public void loadCart(boolean pullToRefresh, HashMap<String, String> params) {

        ArrayList<String> wisataCarts = prefs.getWisataCartPreferences();
        if (wisataCarts.size() > 0){
            params.put("cwIds", TextUtils.join(",", wisataCarts));
            params.put("customerId", String.valueOf(prefs.getPreferencesInt(Config.CUSTOMER_ID)));
            params.put("act", "wisata-cart");

            subscribe(App.getService().apiWisata(params), pullToRefresh);
        }else{
            getView().setEmptyCart("Anda belum memilih wisata");
        }

    }

    @Override public void hitungDirection(HashMap<String, String> params) {
        ArrayList<String> wisataCarts = prefs.getWisataCartPreferences();
        if (wisataCarts.size() > 0){
            params.put("cwIds", TextUtils.join(",", wisataCarts));
            params.put("customerId", String.valueOf(prefs.getPreferencesInt(Config.CUSTOMER_ID)));
            params.put("act", "duration");

            if (isViewAttached()) {
                getView().showProgressDialog(true);
            }

            Observer<RequestWisata> observer = new Observer<RequestWisata>() {
                @Override public void onSubscribe(Disposable d) {

                }

                @Override public void onNext(RequestWisata data) {
                    if (isViewAttached()) {
                        getView().showProgressDialog(false);
                        getView().onNextDirection(data);
                    }
                }

                @Override public void onError(Throwable e) {
                    if (isViewAttached()) {
                        getView().showProgressDialog(false);
                        getView().showError(context.getString(R.string.error_network_light));
                    }
                }

                @Override public void onComplete() {

                }
            };

            App.getService().apiWisata(params)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(observer);
        }else{
            Toast.makeText(context, "Anda belum memilih wisata", Toast.LENGTH_SHORT).show();
        }
    }


}