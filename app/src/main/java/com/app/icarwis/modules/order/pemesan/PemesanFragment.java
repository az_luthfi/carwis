package com.app.icarwis.modules.order.pemesan;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.app.icarwis.R;
import com.app.icarwis.base.BaseMvpFragment;
import com.app.icarwis.listeners.OnChangeToolbar;
import com.app.icarwis.modules.order.invoice.OrderInvoiceFragment;
import com.app.icarwis.modules.order.invoice.OrderInvoiceFragmentBuilder;
import com.app.icarwis.utility.Validation;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;

@FragmentWithArgs
public class PemesanFragment extends BaseMvpFragment<XPemesanView, PemesanPresenter>
        implements XPemesanView, OnChangeToolbar {
    public static final int TYPE_WISATA = 1;
    public static final int TYPE_CAR_RENTAL = 2;
    public static final int TYPE_PAKET_WISATA = 3;

    @Arg HashMap<String, String> params;
    @Arg int typePesan;
    @Arg(required = false) String jsonCar;
    @Arg(required = false) String jsonPaketWisata;
    @Arg(required = false) String jsonRequestWisata;
    @BindView(R.id.etName) MaterialEditText etName;
    @BindView(R.id.etEmail) MaterialEditText etEmail;
    @BindView(R.id.etPhone) MaterialEditText etPhone;
    @BindView(R.id.bntSubmit) TextView bntSubmit;

    public PemesanFragment() {

    }

    @Override public PemesanPresenter createPresenter() {
        return new PemesanPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_pemesan;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        etName.setText("Nama Pemesan");
//        etPhone.setText("087758402241");
//        etEmail.setText("asdasd@gmail.com");
    }

    @OnClick(R.id.bntSubmit) public void onViewClicked() {
        if (Validation.isEmpty(etName, "Nama wajib diisi")) return;
        if (Validation.isEmpty(etPhone, "No. Telp / HP wajib diisi")) return;
        if (etPhone.length() < 10) {
            etPhone.requestFocus();
            etPhone.setError("No. Telp / HP tidak sesuai");
            return;
        }
        if (Validation.isEmpty(etEmail, "Email wajib diisi")) return;
        if (!Validation.isValidEmailAddress(etEmail.getText().toString())) {
            etEmail.requestFocus();
            etEmail.setError("Email tidak sesuai");
            return;
        }
        OrderInvoiceFragment fragment = null;
        params.put("pemesanName", etName.getText().toString());
        params.put("pemesanPhone", etPhone.getText().toString());
        params.put("pemesanEmail", etEmail.getText().toString());
        switch (typePesan){
            case TYPE_WISATA:
                fragment = new OrderInvoiceFragmentBuilder(params, typePesan)
                        .jsonRequestWisata(jsonRequestWisata)
                        .jsonCar(jsonCar)
                        .build();
                break;
            case TYPE_CAR_RENTAL:
                fragment = new OrderInvoiceFragmentBuilder(params, typePesan)
                        .jsonCar(jsonCar)
                        .build();
                break;
            case TYPE_PAKET_WISATA:
                fragment = new OrderInvoiceFragmentBuilder(params, typePesan)
                        .jsonPaketWisata(jsonPaketWisata)
                        .build();
                break;
        }
        if (fragment != null){
            mListener.gotoPage(fragment, false, null);
        }
    }

    @Override public String getTitle() {
        return "Detail Pemesan";
    }

    @Override public boolean isSearch() {
        return false;
    }

    @Override public boolean isCart() {
        return false;
    }
}