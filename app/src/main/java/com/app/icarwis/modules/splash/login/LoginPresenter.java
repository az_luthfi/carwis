package com.app.icarwis.modules.splash.login;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.app.icarwis.R;
import com.app.icarwis.app.App;
import com.app.icarwis.base.BasePresenter;
import com.app.icarwis.models.customer.RequestCustomer;
import com.app.icarwis.utility.CommonUtilities;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.app.icarwis.utility.Config.CUSTOMER_REG_ID;

public class LoginPresenter extends BasePresenter<XLoginView>
        implements XLoginPresenter {

    private Activity activity;

    public LoginPresenter(Context context, Activity activity) {
        super(context);
        this.activity = activity;
    }

    @Override public void resetPassword(String emailReset) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "forget_pin_before_login");
        params.put("email", emailReset);

        if (isViewAttached()) {
            getView().showProgressDialog(true);
        }

        Observer<RequestCustomer> observer = new Observer<RequestCustomer>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestCustomer requestCustomer) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    if (requestCustomer.getStatus()) {
                        Toast.makeText(context, requestCustomer.getText(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, requestCustomer.getText(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    Toast.makeText(context, context.getString(R.string.error_network_light), Toast.LENGTH_SHORT).show();
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiCustomer(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    @Override public void requestLogin(String email, @Nullable String password, boolean isSocial) {
        HashMap<String, String> params = new HashMap<>();
        params.put("regId", getRegId());
        params.put("email", email);
        params.put("handset", CommonUtilities.getDeviceName());
        params.put("app_version", String.valueOf(CommonUtilities.getAppVersion(context)));
        params.put("email", email);
        if (!isSocial){
            params.put("act", "doLogin");
            params.put("pass", CommonUtilities.secure("doLogin", password));
        }else{
            params.put("act", "isRegister");
        }

        if (isViewAttached()) {
            getView().showProgressDialog(true);
        }

        Observer<RequestCustomer> observer = new Observer<RequestCustomer>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestCustomer requestCustomer) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    if (requestCustomer.getStatus()) {
                        getView().onSuccessAuth(requestCustomer.getCustomer());
                    } else {
                        getView().showError(requestCustomer.getText());
                    }
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().showError(context.getString(R.string.error_network_light));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiCustomer(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    private String getRegId() {
        return prefs.getPreferencesString(CUSTOMER_REG_ID) == null ? "" : prefs.getPreferencesString(CUSTOMER_REG_ID);
    }
}