package com.app.icarwis.modules.booking;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.app.icarwis.R;
import com.app.icarwis.base.BaseFragment;
import com.app.icarwis.modules.booking.list.ListMyBookingFragmentBuilder;
import com.app.icarwis.utility.SmartFragmentStatePagerAdapter;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyBookingFragment extends BaseFragment {


    @BindView(R.id.tabLayout) TabLayout tabLayout;
    @BindView(R.id.viewPager) ViewPager viewPager;

    private MyBookingPagerAdapter pagerAdapter;

    public MyBookingFragment() {
        // Required empty public constructor
    }


    @Override protected int getLayoutRes() {
        return R.layout.fragment_with_tab_layout;
    }

    @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pagerAdapter = new MyBookingPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    public class MyBookingPagerAdapter extends SmartFragmentStatePagerAdapter {
        private String tabTitles[] = new String[]{"Upcoming Trip", "History"};

        public MyBookingPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new ListMyBookingFragmentBuilder("upcoming").build();
                default:
                    return new ListMyBookingFragmentBuilder("history").build();
            }
        }

        @Override public int getCount() {
            return tabTitles.length;
        }

        @Override public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }
    }


}
