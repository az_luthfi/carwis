package com.app.icarwis.modules.content.detail;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XDetailContentPresenter extends MvpPresenter<XDetailContentView> {

}