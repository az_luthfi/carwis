package com.app.icarwis.modules.account.editprofile;


import com.app.icarwis.models.customer.RequestCustomer;
import com.app.icarwis.models.datalist.SimpleList;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import java.util.ArrayList;

public interface XEditAccountView extends MvpLceView<RequestCustomer> {

    void showDialog(ArrayList<? extends SimpleList> data, String title);

    void imageFromGallery();

    void showLoadingUpdate(boolean show);

    void gotoBack();
}