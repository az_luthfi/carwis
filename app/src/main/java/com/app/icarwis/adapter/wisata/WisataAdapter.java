package com.app.icarwis.adapter.wisata;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.app.icarwis.base.BaseRecyclerViewLoadingAdapter;
import com.app.icarwis.holder.ItemLoadingHolder;
import com.app.icarwis.holder.wisata.ItemWisataHolder;
import com.app.icarwis.models.wisata.Wisata;

/**
 * Created by Luthfi Aziz on 18/11/2017.
 */

public class WisataAdapter extends BaseRecyclerViewLoadingAdapter<Wisata> {
    public static final int TYPE_CHOOSE = 1;
    public static final int TYPE_CART = 2;
    private static final String TAG = WisataAdapter.class.getSimpleName();
    private Listener listener;
    private boolean isBusy = false;
    private int type;

    public WisataAdapter(ItemLoadingHolder.ItemViewLoadingListener loadingListener, Listener listener, int type) {
        super(loadingListener);
        this.listener = listener;
        this.type = type;
    }

    @Override protected int getItemViewTypeChild(int position) {
        return 1;
    }

    @Override protected RecyclerView.ViewHolder onCreateViewHolderChild(ViewGroup parent, int viewType, LayoutInflater inflater) {
        return new ItemWisataHolder(inflater, parent, type);
    }

    @Override protected void onBindViewHolderChild(RecyclerView.ViewHolder holder, int position, Wisata model) {
        if (holder instanceof ItemWisataHolder) {
            ((ItemWisataHolder) holder).bindView(model, listener);
        }
    }

//    public void updateItemChecked(Wisata wisata, int position) {
//        isBusy = true;
//        int lastChecked = -1;
//        for (int i = 0; i < items.size(); i++) {
//            if (items.get(i).isChecked()) {
//                lastChecked = i;
//            } else {
//                break;
//            }
//        }
//        lastChecked += 1;
//        wisata.setChecked(true);
//        if (position == lastChecked) {
//            notifyItemChanged(position, wisata);
//        } else {
//            removeItem(position);
//            items.add(lastChecked, wisata);
//            notifyItemInserted(lastChecked);
//        }
//        listener.scrollToPosition(lastChecked);
//        new android.os.Handler().postDelayed(new Runnable() {
//            @Override public void run() {
//                isBusy = false;
//            }
//        }, 200);
//    }
//
//    public void updateItemUnChecked(int position, Wisata wisata) {
//        isBusy = true;
//        items.get(position).setChecked(false);
//        ArrayList<Wisata> wisataArrayList = new ArrayList<>();
//        for (Wisata wisata1 : items) {
//            if (wisata1.isChecked()) {
//                wisataArrayList.add(wisataArrayList.size(), wisata1);
//            }
//        }
//        Comparator<Wisata> comparator = new Comparator<Wisata>() {
//            //
//            @Override
//            public int compare(Wisata object1, Wisata object2) {
//                return Double.compare(object1.getDistance(), object2.getDistance());
//            }
//        };
//        Collections.sort(items, comparator);
//        for (int i = 0; i < items.size(); i++) {
//            if (items.get(i).isChecked()) {
//                items.remove(i);
//                i -= 1;
//            }
//        }
//        for (int b = 0; b < wisataArrayList.size(); b++) {
//            items.add(b, wisataArrayList.get(b));
//        }
//        notifyDataSetChanged();
//        new android.os.Handler().postDelayed(new Runnable() {
//            @Override public void run() {
//                isBusy = false;
//            }
//        }, 200);
//
//    }

    public interface Listener {
        boolean onCheck(int position, Wisata wisata, boolean checked);

        void scrollToPosition(int position);

        void onDetail(int position);
    }

//    @Override public void clear() {
//        for (int i = 0; i < getItemCount(); i++) {
//            if (items.get(i).isChecked()){
//                continue;
//            }
//            items.remove(i);
//            i-=1;
//        }
//        notifyDataSetChanged();
//    }
//
//    @Override public void setItems(ArrayList<Wisata> data) {
//        OUTER : for (int i = 0; i < data.size(); i++) {
//            for (int j = 0; j < items.size(); j++) {
//                if (!items.get(j).isChecked()){
//                    break;
//                }
//                if (data.get(i).getCwId().equals(items.get(j).getCwId())){
//                    continue OUTER;
//                }
//            }
//            items.add(data.get(i));
//        }
//        notifyDataSetChanged();
//    }
//
//    public boolean isBusy() {
//        return isBusy;
//    }
}
