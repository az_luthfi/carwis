package com.app.icarwis.adapter.content;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.app.icarwis.base.BaseRecyclerViewLoadingAdapter;
import com.app.icarwis.holder.ItemLoadingHolder;
import com.app.icarwis.holder.content.ItemContentListHolder;
import com.app.icarwis.models.berita.Content;


/**
 * Created by j3p0n on 4/27/2017.
 */
public class ContentListAdapter extends BaseRecyclerViewLoadingAdapter<Content> {

    public ContentListAdapter(ItemLoadingHolder.ItemViewLoadingListener loadingListener) {
        super(loadingListener);
    }

    @Override protected int getItemViewTypeChild(int position) {
        return 1;
    }

    @Override protected RecyclerView.ViewHolder onCreateViewHolderChild(ViewGroup parent, int viewType, LayoutInflater inflater) {
        return new ItemContentListHolder(inflater, parent);
    }

    @Override protected void onBindViewHolderChild(RecyclerView.ViewHolder holder, int position, Content model) {
        if (holder instanceof  ItemContentListHolder){
            ((ItemContentListHolder) holder).bindView(model);
        }
    }
}