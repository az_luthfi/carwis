package com.app.icarwis.modules.order.wisata.detailwisata;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XDetailWisataPresenter extends MvpPresenter<XDetailWisataView> {

}