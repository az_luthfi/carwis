package com.app.icarwis.modules.order.wisata.car;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.icarwis.R;
import com.app.icarwis.adapter.car.CarAdapter;
import com.app.icarwis.base.BaseLceRefreshFragment;
import com.app.icarwis.listeners.OnChangeToolbar;
import com.app.icarwis.models.car.RequestCar;
import com.app.icarwis.models.wisata.RequestWisata;
import com.app.icarwis.modules.order.pemesan.PemesanFragment;
import com.app.icarwis.modules.order.pemesan.PemesanFragmentBuilder;
import com.app.icarwis.utility.listview.DividerItemDecoration;
import com.app.icarwis.utility.listview.ItemClickSupport;
import com.google.gson.Gson;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import java.util.HashMap;

import butterknife.BindView;

@FragmentWithArgs
public class CarListFragment extends BaseLceRefreshFragment<SwipeRefreshLayout, RequestCar, XCarListView, CarListPresenter>
        implements XCarListView, OnChangeToolbar {

    @Arg HashMap<String, String> params;
    @Arg String jsonRequestWisata;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.emptyView) TextView emptyView;
    @BindView(R.id.contentView) SwipeRefreshLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.container) FrameLayout container;

    private CarAdapter adapter;
    private RequestWisata requestWisata;

    public CarListFragment() {

    }

    @Override public CarListPresenter createPresenter() {
        return new CarListPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_with_recyclerview_refresh;
    }

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWisata = new Gson().fromJson(jsonRequestWisata, RequestWisata.class);
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new CarAdapter(null);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.LIST_VERTICAL));
        recyclerView.setAdapter(adapter);

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                PemesanFragment fragment = new PemesanFragmentBuilder(params, PemesanFragment.TYPE_WISATA)
                        .jsonRequestWisata(jsonRequestWisata)
                        .jsonCar(new Gson().toJson(adapter.getItem(position)))
                        .build();
               mListener.gotoPage(fragment, false, null);
            }
        });

        loadData(false);
    }

    @Override public void setData(RequestCar data) {
        if (adapter.getItemCount() > 0) {
            adapter.clear();
        }
        if (data.getStatus()) {
            recyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            adapter.setItems(data.getCars());
        } else {
            recyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
            emptyView.setText(data.getText());
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadData(pullToRefresh, params);
    }

    @Override public String getTitle() {
        return "Pilih Mobil";
    }

    @Override public boolean isSearch() {
        return false;
    }

    @Override public boolean isCart() {
        return false;
    }
}