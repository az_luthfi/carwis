package com.app.icarwis.modules.order.wisata.wisata;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import java.util.HashMap;

public interface XWisataPresenter extends MvpPresenter<XWisataView> {

    void loadData(boolean pullToRefresh, HashMap<String, String> params, int page, String querySearch, String idProvince, String idCity);

    void hitungDirection(HashMap<String, String> params);
}