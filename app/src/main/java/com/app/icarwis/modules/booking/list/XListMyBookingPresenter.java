package com.app.icarwis.modules.booking.list;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XListMyBookingPresenter extends MvpPresenter<XListMyBookingView> {

    void loadData(boolean pullToRefresh, int page, String status);
}