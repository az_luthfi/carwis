package com.app.icarwis.modules.splash.register;

import com.app.icarwis.models.berita.RequestContent;
import com.app.icarwis.models.customer.Customer;
import com.app.icarwis.models.datalist.SimpleList;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.ArrayList;

public interface XRegisterView extends MvpView {

    void showAccountsDialog();

    void showDialog(ArrayList<? extends SimpleList> data, String title);

    void showProgressDialog(boolean show);

    void showError(String text);

    void onSuccessRegister(Customer customer);

    void shoDialogTermAndCon(RequestContent data);
}