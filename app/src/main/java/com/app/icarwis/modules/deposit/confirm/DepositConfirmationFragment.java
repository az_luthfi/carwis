package com.app.icarwis.modules.deposit.confirm;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.icarwis.R;
import com.app.icarwis.base.BaseMvpFragment;
import com.app.icarwis.listeners.OnChangeToolbar;
import com.app.icarwis.models.deposit.Deposit;
import com.app.icarwis.modules.deposit.add.DepositFragment;
import com.app.icarwis.utility.CommonUtilities;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import butterknife.BindView;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.app.icarwis.utility.Config.CUSTOMER_SALDO;


@FragmentWithArgs
public class DepositConfirmationFragment extends BaseMvpFragment<XDepositConfirmationView, DepositConfirmationPresenter>
        implements XDepositConfirmationView, OnChangeToolbar {

    @Arg String depositJson;
    @BindView(R.id.ivProductImage) ImageView ivProductImage;
    @BindView(R.id.tvNominalDeposit) TextView tvNominalDeposit;
    @BindView(R.id.tvBankName) TextView tvBankName;
    @BindView(R.id.tvBankAccountName) TextView tvBankAccountName;
    @BindView(R.id.tvBankAccountNumber) TextView tvBankAccountNumber;
    @BindView(R.id.etPesan) AppCompatEditText etPesan;
    @BindView(R.id.tvSaldo) TextView tvSaldo;
    @BindView(R.id.buttonConfirmDeposit) Button buttonConfirmDeposit;
    @BindView(R.id.buttonCancelDeposit) Button buttonCancelDeposit;
    private Deposit deposit;

    public DepositConfirmationFragment() {

    }

    @Override public DepositConfirmationPresenter createPresenter() {
        return new DepositConfirmationPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_confirmation_deposit;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvSaldo.setText(CommonUtilities.toRupiahFormat(presenter.getPrefs().getPreferencesInt(CUSTOMER_SALDO)));
        deposit = CommonUtilities.getObject(depositJson, Deposit.class);
        if (deposit != null) {
            tvNominalDeposit.setText(CommonUtilities.toRupiahFormat(CommonUtilities.toPlain(deposit.getDepositAmount())));
            tvBankName.setText(deposit.getDepositBankTo());
            tvBankAccountName.setText(deposit.getDepositBankToAcc());
            tvBankAccountNumber.setText(deposit.getDepositBankToNumber());
        }
    }

    @OnClick({R.id.buttonCancelDeposit, R.id.buttonConfirmDeposit}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonCancelDeposit:
                showConfirmDialog(SweetAlertDialog.WARNING_TYPE, "Peringatan", "Apakah Anda yakin ingin membatalkan deposit?", "Ya, Batalkan", "Tidak", "cancelDeposit", false);
                break;
            case R.id.buttonConfirmDeposit:
                presenter.confirmDeposit(deposit.getDepositId(), etPesan.getText().toString());
                break;
        }
    }

    @Override public void onAlertConfirm(String action) {
        switch (action) {
            case "cancelDeposit":
                presenter.cancelDeposit(deposit.getDepositId());
                break;
            case "back":
                mListener.back();
                mListener.gotoPage(new DepositFragment(), false, "Tambah Deposit");
                break;
            case "close":

                break;
        }
    }

    @Override public void showProgressDialog(@Nullable String msg, boolean show) {
        if (show) {
            showLoading(msg, false);
        } else {
            hideLoading();
        }
    }

    @Override public void onError(String contextString) {
        showSnackBar(contextString);
    }

    @Override public void showConfirmDialogView(int warningType, String peringatan, String s, String s1, String tidak, String cancelDeposit) {
        showConfirmDialog(warningType, peringatan, s, s1, tidak, cancelDeposit, true);
    }

    @Nullable @Override public String getTitle() {
        return "Konfirmasi Deposit";
    }

    @Override public boolean isSearch() {
        return false;
    }

    @Override public boolean isCart() {
        return false;
    }

}