package com.app.icarwis.modules.order.carrental.carparent;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XCarParentPresenter extends MvpPresenter<XCarParentView> {

    void loadData();
}