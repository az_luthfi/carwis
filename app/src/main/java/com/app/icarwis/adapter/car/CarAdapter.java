package com.app.icarwis.adapter.car;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.app.icarwis.base.BaseRecyclerViewLoadingAdapter;
import com.app.icarwis.holder.ItemLoadingHolder;
import com.app.icarwis.holder.car.ItemCarHolder;
import com.app.icarwis.models.car.Car;

/**
 * Created by Luthfi Aziz on 19/11/2017.
 */
public class CarAdapter extends BaseRecyclerViewLoadingAdapter<Car> {


    public CarAdapter(ItemLoadingHolder.ItemViewLoadingListener loadingListener) {
        super(loadingListener);
    }

    @Override protected int getItemViewTypeChild(int position) {
        return 1;
    }

    @Override protected RecyclerView.ViewHolder onCreateViewHolderChild(ViewGroup parent, int viewType, LayoutInflater inflater) {
        return new ItemCarHolder(inflater, parent);
    }

    @Override protected void onBindViewHolderChild(RecyclerView.ViewHolder holder, int position, Car model) {
        if (holder instanceof ItemCarHolder){
            ((ItemCarHolder) holder).bindView(model);
        }
    }
}