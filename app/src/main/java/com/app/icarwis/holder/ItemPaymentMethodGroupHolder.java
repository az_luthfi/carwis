package com.app.icarwis.holder;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.app.icarwis.R;
import com.app.icarwis.adapter.PaymentMethodGroupAdapter;
import com.app.icarwis.models.transaction.PaymentGroup;
import com.app.icarwis.models.transaction.PaymentMethod;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemPaymentMethodGroupHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.cbPaymentMethodGroup) RadioButton cbPaymentMethodGroup;
    @BindView(R.id.tvPaymentMethodGroup) TextView tvPaymentMethodGroup;
    @BindView(R.id.tvPaymentMethodName) TextView tvPaymentMethodName;
    @BindView(R.id.layArrowRight) FrameLayout layArrowRight;
    @BindView(R.id.layContainer) LinearLayout layContainer;

    private AlertDialog.Builder builder;
    private ArrayList<String> options;

    public ItemPaymentMethodGroupHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_payment_method_group, parent, false));
    }

    public ItemPaymentMethodGroupHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
        builder = new AlertDialog.Builder(itemView.getContext());
    }

    public void bindView(PaymentGroup model, final PaymentMethodGroupAdapter.Listener listener) {
        tvPaymentMethodGroup.setText(model.getPaymentMethodGroup());
        if (model.isSelected()) {
            cbPaymentMethodGroup.setChecked(true);
        } else {
            cbPaymentMethodGroup.setChecked(false);
        }
        if (model.getPositionPayment() > -1) {
            tvPaymentMethodName.setVisibility(View.VISIBLE);
            tvPaymentMethodName.setText(model.getPaymentMethods().get(model.getPositionPayment()).getPaymentMethodName());
        } else {
            tvPaymentMethodName.setVisibility(View.GONE);
        }
        if (model.getPaymentMethods().size() == 1 && model.getPaymentMethods().get(0).getPaymentMethodGroup().equals(model.getPaymentMethodGroup())) {
            layArrowRight.setVisibility(View.GONE);
            options = null;
        } else {
            layArrowRight.setVisibility(View.VISIBLE);
            options = new ArrayList<>();
            for (PaymentMethod paymentMethod : model.getPaymentMethods()) {
                options.add(paymentMethod.getPaymentMethodName());
            }
            builder.setTitle("Pilih Metode Pembayaran");
            builder.setItems(options.toArray(new CharSequence[options.size()]), new DialogInterface.OnClickListener() {
                @Override public void onClick(DialogInterface dialogInterface, int i) {
                    listener.onSelected(getAdapterPosition(), i);
                }
            });
        }

        layContainer.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                if (layArrowRight.getVisibility() == View.GONE) {
                    listener.onSelected(getAdapterPosition(), -1);
                } else {
                    builder.show();
                }
            }
        });
    }
}
